
------------------------------------------------------------------
-- Variables
------------------------------------------------------------------

local require = GLOBAL.require
local Ingredient = GLOBAL.Ingredient

local PlayablePets = GLOBAL.PlayablePets
local SETTING = GLOBAL.PP_SETTINGS
local MOBTYPE = GLOBAL.PP_MOBTYPES

local STRINGS = require("ppnr_strings")
GLOBAL.PPNR_FORGE = require("ppnr_forge")

local TUNING = require("ppnr_tuning")
------------------------------------------------------------------
-- Configuration Data
------------------------------------------------------------------

PlayablePets.Init(env.modname)

GLOBAL.ANTLION_MODE = GetModConfigData("Antlion")
GLOBAL.MOB_EWECUS = GetModConfigData("Ewecus")
----------------------------------------------------------------------
PrefabFiles = {
	"nrmonster_wpn",
	--------
	"deerp_fx",
	"shadowchannelerp",
	"fossil_spike2_p",
	"sporecloudp",
	"sporebombp",
	"klaus_fire_meteorp",
	"ppnrbuffs", --for reforged use only
	--"sandspike",
}

GLOBAL.PPNR_MobCharacters = {
	antlionp        = { fancyname = "Antlion",           gender = "FEMALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, mimic_prefab = {"antlion"}},
	beequeenp        = { fancyname = "Bee Queen",           gender = "FEMALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, mimic_prefab = {"beequeen"} },
	klausp        = { fancyname = "Klaus",           gender = "MALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, mimic_prefab = {"klaus"} },
	toadstoolp        = { fancyname = "Toadstool",           gender = "MALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = false, mimic_prefab = {"toadstool"} },
	toadstool_darkp        = { fancyname = "Misery Toadstool",           gender = "MALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, mimic_prefab = {"toadstool_dark"} },
	stalkerp        = { fancyname = "Reanimated Skeleton",           gender = "NEUTRAL",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true, mimic_prefab = {"stalker"} },
	stalker_forestp        = { fancyname = "Reanimated Surface Skeleton",           gender = "NEUTRAL",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true, mimic_prefab = {"stalker_forest"} },
	stalker_atriump        = { fancyname = "Ancient Fuelweaver",           gender = "NEUTRAL",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true, mimic_prefab = {"stalker_atrium"} },
	stalker_minionp        = { fancyname = "Woven Shadow",           gender = "NEUTRAL",   mobtype = {}, skins = {}, mimic_prefab = {"stalker_minion"} },
	babycatp        = { fancyname = "Kittycoon",           gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"critter_kitten"} },
	babyhoundp        = { fancyname = "Vargling",           gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"critter_puppy"} },
	babydragonp        = { fancyname = "Broodling",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"critter_dragonling"} },
	babyglommerp        = { fancyname = "GlomGlom",           gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"critter_gromling"} },
	babysheepp        = { fancyname = "Ewelet",           gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"critter_lamb"} },
	sheepp        = { fancyname = "Ewecus",           gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"spat"} },
	gibletp        = { fancyname = "Giblet",           gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"critter_perdling"} },
	beeguardp        = { fancyname = "Grumple Bee",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"beeguard"} }, 
	bird2p        = { fancyname = "Canary",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"canary"} }, 
	deerp        = { fancyname = "No-Eyed Deer",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"deer"} }, 
	deer_redp        = { fancyname = "RedGemmed Deer",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"deer_red"} }, 
	deer_bluep        = { fancyname = "BlueGemmed Deer",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"deer_blue"} }, 
	shadow_bishopp        = { fancyname = "Shadow Bishop",           gender = "NEUTRAL",   mobtype = {MOBTYPE.GIANT}, skins = {}, mimic_prefab = {"shadow_bishop"} },
	shadow_bishop2p        = { fancyname = "Shadow Bishop (lv 3)",           gender = "NEUTRAL",   mobtype = {MOBTYPE.GIANT}, skins = {}},
	shadow_knightp        = { fancyname = "Shadow Knight",           gender = "NEUTRAL",   mobtype = {MOBTYPE.GIANT}, skins = {}, mimic_prefab = {"shadow_knight"} },
	shadow_knight2p        = { fancyname = "Shadow Knight (lv 3)",           gender = "NEUTRAL",   mobtype = {MOBTYPE.GIANT}, skins = {} },
	shadow_rookp        = { fancyname = "Shadow Rook",           gender = "NEUTRAL",   mobtype = {MOBTYPE.GIANT}, skins = {}, mimic_prefab = {"shadow_rook"} },
	shadow_rook2p        = { fancyname = "Shadow Rook (lv 3)",           gender = "NEUTRAL",   mobtype = {MOBTYPE.GIANT}, skins = {} },
	stagehandp        = { fancyname = "Stagehand",           gender = "NEUTRAL",   mobtype = {}, skins = {}, mimic_prefab = {"stagehand"} },
}

-- Necessary to ensure a specific order when adding mobs to the character select screen. This table is iterated and used to index the one above
PPNR_Character_Order = {
	"stagehandp",
	"babyhoundp",
	"babycatp",
	"babyglommerp",
	"babysheepp",
	"sheepp",
	"babydragonp",
	"gibletp",
	"bird2p",
	"deerp",
	"deer_redp",
	"deer_bluep",
	"shadow_bishopp",
	"shadow_bishop2p",
	"shadow_knightp",
	"shadow_knight2p",
	"shadow_rookp",
	"shadow_rook2p",
	"beeguardp",
	"beequeenp",
	"klausp",
	"antlionp",
	"toadstoolp",
	"toadstool_darkp",
	"stalker_minionp",
	"stalker_forestp",
	"stalkerp",
	"stalker_atriump",
}



Assets = {
	Asset("ANIM", "anim/ghost_monster_build.zip"),
}

local require = GLOBAL.require
local STRINGS = GLOBAL.STRINGS
--Character Select Screen Stuff--

------------------------------------------------------------------
-- Component Overrides
------------------------------------------------------------------

-------------------------------------------------------
--Wardrobe stuff--

--Skin Puppet Stuff
local MobPuppets = require("ppnr_puppets")
local MobSkins = require("ppnr_skins")
PlayablePets.RegisterPuppetsAndSkins(PPNR_Character_Order, MobPuppets, MobSkins)
------------------------------------------------------------------
-- Commands
------------------------------------------------------------------

---------------------------------------------------------
--Custom Recipes
local Ingredient = GLOBAL.Ingredient
--local pigrecipe = AddRecipe("pighouse_player", {Ingredient("boards", 4),Ingredient("cutstone", 2)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "pighouse_placer", nil, nil, 1, "pigmanplayer", "images/inventoryimages/houseplayer.xml", "houseplayer.tex")

--pigrecipe.sortkey = -7.0
-----------Mob Home Recipes-------------------------------------

--local spiderhomerecipe = AddRecipe("spidernest_p", {Ingredient("silk", 10),Ingredient("spidergland", 3), Ingredient("papyrus", 2)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "spidereggsack_placer", nil, nil, 1 ,nil, "images/inventoryimages/spiderhome.xml", "spiderhome.tex" )



--checks to make sure the player attacks. Might not be necessary.

ACTIONS = GLOBAL.ACTIONS
ActionHandler = GLOBAL.ActionHandler
----------------------------------------------------------
---------------------------------------------------------
--PostInits
local TheNet = GLOBAL.TheNet

local function FriendlyBombsProjectileInit(prefab) --make it to where it doesn't hurt Toadstool players.
	local function OnProjectileHit(prefab)
        local x, y, z = prefab.Transform:GetWorldPosition()
        prefab:Remove()
        local bomb = SpawnPrefab(bombname)
		if prefab.isplayerbomb ~= nil then
			bomb.isplayerbomb = true
		end
        bomb.Transform:SetPosition(x, y, z)
        bomb.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/spore_land")
        local toadstool = prefab.components.entitytracker:GetEntity("toadstool")
        if toadstool ~= nil then
            bomb.components.entitytracker:TrackEntity("toadstool", toadstool)
        end
    end
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"toadstool", "notarget", "INLIMBO", "playerghost"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "notarget", "toadstool", "INLIMBO", "playerghost"}
	else	
		return {"player", "companion", "notarget", "toadstool", "INLIMBO", "playerghost"}
	end
end

local function FriendlyBombsInit(prefab) --make it to where it doesn't hurt Toadstool players.

local function Explode(prefab)
	print(prefab.isplayerbomb)
    prefab._growtask = nil

    prefab.AnimState:PlayAnimation("explode")
    prefab.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/spore_explode")
    prefab:DoTaskInTime(prefab.AnimState:GetCurrentAnimationLength(), prefab.Remove)
    prefab.persists = false

    prefab._explode:push()
    FadeOut(prefab)

    --Dedicated server does not need to spawn the local fx
    if not TheNet:IsDedicated() then
        CreateGroundFX(prefab)
    end

    local x, y, z = prefab.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, TUNING.TOADSTOOL_MUSHROOMBOMB_RADIUS, { "_health", "_combat" }, { "INLIMBO", "epic", "toadstool" })
	local ents2 = TheSim:FindEntities(x, y, z, TUNING.TOADSTOOL_MUSHROOMBOMB_RADIUS, { "_health", "_combat" }, GetExcludeTags(prefab))
    if #ents > 0 then
        local toadstool = prefab.components.entitytracker:GetEntity("toadstool")
		local toadstoolp = prefab.components.entitytracker:GetEntity("toadstoolp")
        local damage =
            toadstool ~= nil and
            toadstool.components.combat ~= nil and
            toadstool.components.combat.defaultdamage or
            TUNING.TOADSTOOL_DAMAGE	
		if prefab.setreturn then --this is not likely to mess with the other... right?
			local damage = prefab.setreturn or 200
			--print("Custom Mushroom Dmg Values Detected")
		end	
		
		if prefab.isplayerbomb == nil or TheNet:GetPVPEnabled() then
        for i, v in ipairs(ents) do
            if v:IsValid() and not v:IsInLimbo() and not v.HasTag("toadstool") and
                v.components.combat ~= nil and not (v.components.health ~= nil and v.components.health:IsDead()) then
                v.components.combat:GetAttacked(prefab, TheNet:GetServerGameMode() == "lavaarena" and PPNR_FORGE.TOADSTOOL.BOMB_DAMAGE or damage, nil, "explosive")
            end
        end
		
		else
		
		for i, v in ipairs(ents2) do
            if v:IsValid() and not v:IsInLimbo() and not v.HasTag("toadstool") and
                v.components.combat ~= nil and not (v.components.health ~= nil and v.components.health:IsDead()) then
                v.components.combat:GetAttacked(toadstoolp, TheNet:GetServerGameMode() == "lavaarena" and PPNR_FORGE.TOADSTOOL.BOMB_DAMAGE or damage, nil, "explosive")
            end
        end
		end
    end
end
end

local function FriendlyWaspInit(prefab)
	local function OnNearBy(prefab, target)
	if prefab.components.childspawner ~= nil and not target:HasTag("bee") then
        prefab.components.childspawner:ReleaseAllChildren(target, "killerbee")
    end

	end
	
	if prefab.components.playerprox then
	prefab.components.playerprox:SetOnPlayerNear(OnNearBy)
	end
end

local function FriendlyBeeGuardInit(prefab)
	local function CheckFocusTarget(prefab)
    if prefab._focustarget ~= nil and (
            not prefab._focustarget:IsValid() or
            prefab._focustarget.components.health:IsDead() or
            prefab._focustarget:HasTag("playerghost")-- or prefab._focustarget:HasTag("bee")
        ) then
        prefab._focustarget = nil
    end
    return prefab._focustarget
end

local function Retarget2Fn(prefab)
    local focustarget = CheckFocusTarget(prefab)
    if focustarget ~= nil then
        return focustarget, not prefab.components.combat:TargetIs(focustarget)
    end
    local player, distsq = prefab:GetNearestPlayer()
	if player ~= nil and not player:HasTag("bee") then
		return distsq ~= nil and distsq < 225 and player or nil
		else
		return nil
	end	
end

local function KeepTarget2Fn(prefab, target)
    local focustarget = CheckFocusTarget(prefab)
    return (focustarget ~= nil and
            prefab.components.combat:TargetIs(focustarget))
        or (prefab.components.combat:CanTarget(target) and
            prefab:IsNear(target, 40) and not target:HasTag("bee"))
end


	--prefab:AddComponent("combat")
	if prefab.components.combat then
	prefab.components.combat:SetRetargetFunction(2, Retarget2Fn)
    prefab.components.combat:SetKeepTargetFunction(KeepTarget2Fn)
	end
end

local function FriendlyBeesInit(prefab)
local function KillerRetarget2(prefab)
    return GLOBAL.FindEntity(prefab, SpringCombatMod(8),
        function(guy)
            return prefab.components.combat:CanTarget(guy) and not guy:HasTag("bee")
        end,
        { "_combat", "_health" },
        { "insect", "INLIMBO"},
        { "character", "animal", "monster" })
end

local function SpringBeeRetarget2(prefab)
    return GLOBAL.TheWorld.state.isspring and
        GLOBAL.FindEntity(prefab, 4,
            function(guy)
				return prefab.components.combat:CanTarget(guy) and not guy:HasTag("bee")
            end,
            { "_combat", "_health" },
            { "insect", "INLIMBO"},
            { "character", "animal", "monster" })
        or nil
	end
	if prefab.components.combat then
	if prefab == "killerbee" then
		prefab.components.combat:SetRetargetFunction(2, KillerRetarget2)
	else	
		prefab.components.combat:SetRetargetFunction(2, SpringBeeRetarget2)
	end	
	end
end


local function FriendlySporeCloudInit(inst)
    --inst:AddComponent("combat")
   -- inst.components.combat:SetDefaultDamage(TheNet:GetServerGameMode() == "lavaarena" and PPNR_FORGE.TOADSTOOL.SPORECLOUD_DAMAGE or TUNING.TOADSTOOL_SPORECLOUD_DAMAGE)
	if TheNet:GetServerGameMode() == "lavaarena" and inst.components.aura then
		inst.components.aura.radius = TUNING.TOADSTOOL_SPORECLOUD_RADIUS
		inst.components.aura.tickperiod = TUNING.TOADSTOOL_SPORECLOUD_TICK
		inst.components.aura.auraexcludetags = {"player", "companion", "notarget", "toadstool", "INLIMBO", "playerghost", "sporeimmune"}
		inst.components.aura:Enable(true)
	end
end

AddPrefabPostInit("sporecloud", FriendlySporeCloudInit)
AddPrefabPostInit("mushroombomb", FriendlyBombsInit)
AddPrefabPostInit("mushroombomb_projectile", FriendlyBombsProjectileInit)
AddPrefabPostInit("wasphive", FriendlyWaspInit)
AddPrefabPostInit("killerbee", FriendlyBeesInit)
AddPrefabPostInit("bee", FriendlyBeesInit)
AddPrefabPostInit("beeguard", FriendlyBeeGuardInit)

---------------------------------------------------------
------------------------------------------------------------------
-- Asset Population
------------------------------------------------------------------

local assetPaths = { "bigportraits/", "images/map_icons/", "images/avatars/avatar_", "images/avatars/avatar_ghost_" }
local assetTypes = { {"IMAGE", "tex"}, {"ATLAS", "xml"} }

-- Iterate through the player mob table and do the following:
-- 1. Populate the PrefabFiles table with the mob prefab names and their skin prefabs (if applicable)
-- 2. Add an atlas and image for the mob's following assets:
-- 2.1 Character select screen portraits
-- 2.2 Character map icons
-- 2.3 ??? FIXME
-- 2.4 ??? FIXME
--for prefab, mob in pairs(GLOBAL.PP_MobCharacters) do
for _, prefab in ipairs(PPNR_Character_Order) do
	local mob = GLOBAL.PPNR_MobCharacters[prefab]
	if PlayablePets.MobEnabled(mob, env.modname) then
		table.insert(PrefabFiles, prefab)
	end
	
	-- Add custom skin prefabs, if available
	-- Example: "dragonplayer_formal"
	for _, skin in ipairs(mob.skins) do
			table.insert(PrefabFiles, prefab.."_"..skin)
	end
	
	for _, path in ipairs(assetPaths) do
		for _, assetType in ipairs(assetTypes) do
			--print("Adding asset: "..assetType[1], path..prefab.."."..assetType[2])
			table.insert( Assets, Asset( assetType[1], path..prefab.."."..assetType[2] ) )
		end
	end
end

------------------------------------------------------------------
-- Mob Character Instantiation
------------------------------------------------------------------

-- Adds a mod character based on an individual mob
-- prefab is the prefab name (e.g. clockwork1player)
-- mob.fancyname is the mob's ingame name (e.g. Knight)
-- mob.gender is fairly self-explanatory
--for prefab, mob in pairs(GLOBAL.PP_MobCharacters) do
for _, prefab in ipairs(PPNR_Character_Order) do
	local mob = GLOBAL.PPNR_MobCharacters[prefab]
	PlayablePets.SetGlobalData(prefab, mob)
	if PlayablePets.MobEnabled(mob, env.modname) then
		AddMinimapAtlas("images/map_icons/"..prefab..".xml")
		AddModCharacter(prefab, mob.gender)
		--print("AddModCharacter mode is "..GLOBAL.MODCHARACTERMODES[prefab].build)
	end
end

