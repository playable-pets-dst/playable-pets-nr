local DEFAULT_SCALE = 0.20
local Puppets = {
	--NR
	stagehandp = {bank = "stagehand", build = "stagehand", anim = "idle_loop_01", scale = DEFAULT_SCALE, shinybuild = "stagehand" },
	sheepp = {bank = "spat", build = "spat_build", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "spat" },
	toadstoolp = {bank = "toadstool", build = "toadstool_build", anim = "idle", scale = 0.08, shinybuild = "toadstool", sixfaced = true, uiscale = 0.06 },
	toadstool_darkp = {bank = "toadstool", build = "toadstool_dark_build", anim = "idle", scale = 0.08, shinybuild = "toadstool_dark", sixfaced = true, uiscale = 0.06 },
	klausp = {bank = "klaus", build = "klaus_build", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "klaus", sixfaced = true, uiscale = 0.125 },
	bird2p = {bank = "canary", build = "canary_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "canary" },
	antlionp = {bank = "antlion", build = "antlion_build", anim = "idle", scale = 0.1, shinybuild = "antlion" },
	babyhoundp = {bank = "pupington", build = "pupington_build", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "pupington" },
	babycatp = {bank = "kittington", build = "kittington_build", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "kittington", sixfaced = true },
	babysheepp = {bank = "sheepington", build = "sheepington_build", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "sheepington", sixfaced = true },
	babydragonp = {bank = "dragonling", build = "dragonling_build", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "dragonling", sixfaced = true },
	babyglommerp = {bank = "glomling", build = "glomling_build", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "glomling", sixfaced = true },
	gibletp = {bank = "perdling", build = "perdling_build", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "perdling" },
	stalkerp = {bank = "stalker", build = "stalker_shadow_build", anim = "idle", scale = 0.1, shinybuild = "stalker_cave", override_build = "stalker_cave_build", uiscale = 0.08 },
	stalker_atriump = {bank = "stalker", build = "stalker_shadow_build", anim = "idle", scale = 0.1, shinybuild = "stalker_atrium", override_build = "stalker_atrium_build", uiscale = 0.08 },
	stalker_forestp = {bank = "stalker_forest", build = "stalker_shadow_build", anim = "idle", scale = 0.1, shinybuild = "stalker_forest", override_build = "stalker_forest_build", uiscale = 0.08 },
	shadow_knightp = {bank = "shadow_knight", build = "shadow_knight", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "shadow_knight" },
	shadow_bishopp = {bank = "shadow_bishop", build = "shadow_bishop", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "shadow_bishop", y_offset = -20 },
	shadow_rookp = {bank = "shadow_rook", build = "shadow_rook", anim = "idle_loop", scale = DEFAULT_SCALE - 0.12, shinybuild = "shadow_rook" },
	shadow_knight2p = {bank = "shadow_knight", build = "shadow_knight", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "shadow_knight", override_build = "shadow_knight_upg_build", symbols_to_override = {"arm", "ear", "face", "head", "leg_low", "neck", "spring"}, symbols_to_override_with = {"arm2", "ear2", "face2", "head2", "leg_low2", "neck2", "spring2"}},
	shadow_bishop2p = {bank = "shadow_bishop", build = "shadow_bishop", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "shadow_bishop", override_build = "shadow_bishop_upg_build", symbols_to_override = {"body_mid", "body_upper", "head", "sharp_feather_a", "sharp_feather_b", "wing" }, symbols_to_override_with = {"body_mid2", "body_upper2", "head2", "sharp_feather_a2", "sharp_feather_b2", "wing2"}, y_offset = -10},
	shadow_rook2p = {bank = "shadow_rook", build = "shadow_rook", anim = "idle_loop", scale = DEFAULT_SCALE -0.12, shinybuild = "shadow_rook", override_build = "shadow_rook_upg_build", symbols_to_override = {"base", "big_horn", "bottom_head", "small_horn_lft", "small_horn_rgt", "top_head" }, symbols_to_override_with = {"base2", "big_horn2", "bottom_head2", "small_horn_lft2", "small_horn_rgt2", "top_head2" }},
	deerp = {bank = "deer", build = "deer_build", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "deer", override_build = "deer_build", sixfaced = true, hide = {"CHAIN", "swap_antler"}, symbols_to_override = {"swap_neck_collar"}, symbols_to_override_with = {"swap_neck"} },
	deer_redp = {bank = "deer", build = "deer_build", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "deer", override_build = "deer_build", sixfaced = true },
	deer_bluep = {bank = "deer", build = "deer_build", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "deer", override_build = "deer_build", sixfaced = true, symbols_to_override = {"swap_antler_red"}, symbols_to_override_with = {"swap_antler_blue"}},
	beequeenp = {bank = "bee_queen", build = "bee_queen_build", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "bee_queen", sixfaced = true, uiscale = 0.12, y_offset = -20 },
	beeguardp = {bank = "bee_guard", build = "bee_guard_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "bee_guard", sixfaced = true, uiscale = 0.27, y_offset = -10 },
	stalker_minionp = {bank = "stalker_minion_2", build = "stalker_minion_2", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "stalker_minion_2", sixfaced = true },
}

return Puppets