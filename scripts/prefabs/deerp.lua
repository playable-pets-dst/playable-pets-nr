local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "deerp"

local assets = 
{
	Asset("ANIM", "anim/deer_shiny_build_01.zip"),
	Asset("ANIM", "anim/deer_ruins.zip"),
	Asset("ANIM", "anim/deer_moon.zip"),
	Asset("ANIM", "anim/deer_shadow.zip"),
}

local prefabs = {
}

local start_inv = {
}

local start_inv2 = 
{

}



if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 800,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 150,
	runspeed = TUNING.DEER_RUN_SPEED,
	walkspeed = TUNING.DEER_WALK_SPEED,
	damage = 60,
	hit_range = TUNING.DEER_ATTACK_RANGE,
	range = TUNING.DEER_ATTACK_RANGE,
	bank = "deer",
	build = "deer_build",
	scale = 1,
	stategraph = "SGdeerp",
	minimap = "deerp.tex",
	
}

--Loot that drops when you die, duh.
SetSharedLootTable('deerp',
-----Prefab---------------------Chance------------
{
    {'meat',			1.00},
	{'meat',			1.00},
})

local FORGE_STATS = PPNR_FORGE[string.upper(prefabname)] --DEER
--==============================================
--					Mob Functions
--==============================================

--------------------------------------------------------------------------

local function DoNothing()
end

local function DoChainIdleSound(inst, volume)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/chain_idle", nil, volume)
end

local function DoBellIdleSound(inst, volume)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/bell_idle", nil, volume)
end

local function DoChainSound(inst, volume)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/chain", nil, volume)
end

local function DoBellSound(inst, volume)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/bell", nil, volume)
end

local function SetupSounds(inst)
    if inst.gem == nil then
        inst.DoChainSound = DoNothing
        inst.DoChainIdleSound = DoNothing
        inst.DoBellSound = DoNothing
        inst.DoBellIdleSound = DoNothing
    elseif IsSpecialEventActive(SPECIAL_EVENTS.WINTERS_FEAST) then
        inst.DoChainSound = DoChainSound
        inst.DoChainIdleSound = DoChainIdleSound
        inst.DoBellSound = DoBellSound
        inst.DoBellIdleSound = DoBellIdleSound
    else
        inst.DoChainSound = DoChainSound
        inst.DoChainIdleSound = DoChainIdleSound
        inst.DoBellSound = DoNothing
        inst.DoBellIdleSound = DoNothing
    end
end
--==============================================
--				Custom Common Functions
--==============================================	

local function GrowAntler(inst, iswinter)
	if TheWorld.state.iswinter and inst.hasantler == false then
	inst.AnimState:Show("swap_antler")
	inst.antlertype = math.random(3)
	if inst.isshiny ~= 0 then
	inst.AnimState:OverrideSymbol("swap_antler_red", "deer_shiny_build_0"..inst.isshiny, "swap_antler"..tostring(inst.antlertype))
	else
	inst.AnimState:OverrideSymbol("swap_antler_red", "deer_build", "swap_antler"..tostring(inst.antlertype))
	end
	--make antlers random
	--inst.AnimState:PlayAnimation("growantler_pst")
	inst.hasantler = true
	elseif inst.hasantler == true then
		inst.AnimState:Show("swap_antler")
		inst.AnimState:OverrideSymbol("swap_antler_red", "deer_build", "swap_antler"..tostring(inst.antlertype))
	end
end

----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end

local function SetSkinDefault(inst, data)
	if data then
		inst.AnimState:SetBuild(data.build)
		inst.AnimState:OverrideSymbol("swap_antler", data.build, "swap_antler")
		inst.AnimState:OverrideSymbol("swap_neck_collar", data.build, "swap_neck")
		--inst.AnimState:OverrideSymbol("swap_neck", data.build, "swap_neck")
		--inst.AnimState:OverrideSymbol("swap_antler", data.build, "swap_antler")
	else
		inst.AnimState:SetBuild(mob.build)
		inst.AnimState:OverrideSymbol("swap_antler", mob.build, "swap_antler")
		inst.AnimState:OverrideSymbol("swap_neck_collar", mob.build, "swap_neck")
		--inst.AnimState:OverrideSymbol("swap_antler", mob.build, "swap_antler")
	end	
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("deer")
	inst:AddTag("deergemresistance")
	inst:AddTag("deer_blue")
    inst:AddTag("animal")
	----------------------------------
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	inst.components.combat:SetHurtSound("dontstarve/creatures/together/deer/hit")
	----------------------------------
	--Variables		
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = false

	inst.hasantler = true
	inst.mobsleep = true
	inst.ghostbuild = "ghost_monster_build"
	inst.hasantler = false
	inst.antlertype = 1
	
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	inst.AnimState:Hide("swap_antler")
	
	local body_symbol = "deer_torso"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)

	 if inst.gem ~= nil then
        if inst.gem ~= "red" then
            inst.AnimState:OverrideSymbol("swap_antler_red", "deer_build", "swap_antler_"..inst.gem)
        end
        if IsSpecialEventActive(SPECIAL_EVENTS.WINTERS_FEAST) and inst.isshiny ~= 0 then
			inst.AnimState:OverrideSymbol("deer_hair", "deer_shiny_build_0"..inst.isshiny, "deer_hair_winter")
            inst.AnimState:OverrideSymbol("swap_neck_collar", "deer_shiny_build_0"..inst.isshiny, "swap_neck_collar_winter")
            inst.AnimState:OverrideSymbol("klaus_deer_chain", "deer_shiny_build_0"..inst.isshiny, "klaus_deer_chain_winter")
            inst.AnimState:OverrideSymbol("deer_chest", "deer_shiny_build_0"..inst.isshiny, "deer_chest_winter")
		elseif IsSpecialEventActive(SPECIAL_EVENTS.WINTERS_FEAST) and inst.isshiny == 0 then
            inst.AnimState:OverrideSymbol("deer_hair", "deer_build", "deer_hair_winter")
            inst.AnimState:OverrideSymbol("swap_neck_collar", "deer_build", "swap_neck_collar_winter")
            inst.AnimState:OverrideSymbol("klaus_deer_chain", "deer_build", "klaus_deer_chain_winter")
            inst.AnimState:OverrideSymbol("deer_chest", "deer_build", "deer_chest_winter")
        end
        inst:AddTag("deergemresistance")
        --inst:SetPrefabNameOverride("deer_gemmed")
    else
        inst.AnimState:Hide("swap_antler")
        inst.AnimState:Hide("CHAIN")
        inst.AnimState:OverrideSymbol("swap_neck_collar", "deer_build", "swap_neck")
    end
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Gem Related--
	 if inst.gem ~= "red" then
        MakeMediumBurnableCharacter(inst, "deer_torso")
        inst.components.burnable:SetBurnTime(TUNING.DEER_ICE_BURN_PANIC_TIME)
    end
    if inst.gem ~= "blue" then
        MakeMediumFreezableCharacter(inst, "deer_torso")
        inst.components.freezable:SetResistance(1)
        inst.components.freezable:SetDefaultWearOffTime(TUNING.DEER_FIRE_FREEZE_WEAR_OFF_TIME)
        --NOTE: no diminishing returns
    end
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.ROUGHAGE, FOODTYPE.VEGGIE }, { FOODTYPE.ROUGHAGE, FOODTYPE.VEGGIE}) 
    inst.components.eater:SetAbsorptionModifiers(1,2,2) --This might multiply food stats.
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 100, .5)
    inst.DynamicShadow:SetSize(1.75, .75)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:WatchWorldState("iswinter", GrowAntler) 
	if inst.hasantler == false then
		inst:WatchWorldState( "isday", GrowAntler)
	end
	inst:DoTaskInTime(3, GrowAntler)--try growing antlers on spawn.
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	SetupSounds(inst)
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)