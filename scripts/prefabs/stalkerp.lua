local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "stalkerp"

local assets = 
{	
	Asset("ANIM", "anim/stalker_basic.zip"),
    Asset("ANIM", "anim/stalker_action.zip"),
    Asset("ANIM", "anim/stalker_shadow_build.zip"),
    Asset("ANIM", "anim/stalker_cave_build.zip"),
}



local prefabs = 
{	
	"shadowheart",
    "fossil_piece",
    "fossilspike",
    "nightmarefuel",
}
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 4000,
	hunger = 300,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 150,
	runspeed = TUNING.STALKER_SPEED + 1,
	walkspeed = TUNING.STALKER_SPEED + 1,
	damage = 100*2,
	range = 4,
	hit_range = 6,
	bank = "stalker",
	build = "stalker_shadow_build",
	build2 = "stalker_cave_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGstalkerp",
	minimap = "stalkerp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('stalkerp',
{
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
})

local FORGE_STATS = PPNR_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================
local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return  {"playerghost", "shadowcreature", "shadow", "shadowminion", "INLIMBO", "bird", "stalkerminion", "notarget"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"playerghost", "shadowcreature", "shadow", "shadowminion", "INLIMBO", "bird", "stalkerminion", "notarget", "player", "companion"}
	else	
		return {"playerghost", "shadowcreature", "shadow", "shadowminion", "INLIMBO", "bird", "stalkerminion", "notarget"}
	end
end

local function SetSkinDefault(inst, data)
	if data then
		inst.AnimState:SetBuild(data.build2 or mob.build)
		inst.AnimState:AddOverrideBuild(data.build)
	else
		inst.AnimState:SetBuild(mob.build)
		inst.AnimState:AddOverrideBuild(mob.build2)
	end	
end

local function OnDoneTalking(inst)
    if inst.talktask ~= nil then
        inst.talktask:Cancel()
        inst.talktask = nil
    end
    inst.SoundEmitter:KillSound("talk")
end

local function OnTalk(inst)
    OnDoneTalking(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/stalker/talk_LP", "talk")
    inst.talktask = inst:DoTaskInTime(1.5 + math.random() * .5, OnDoneTalking)
end

local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function OnDestroyOther(inst, other)
	if other ~= nil and other:HasTag("wall") then
		local isname = inst:GetDisplayName()
		local selfpos = inst:GetPosition()
		print("LOGGED: "..isname.." destroyed a wall at "..selfpos.x..","..selfpos.y..","..selfpos.z.." as "..inst.prefab)
	end	
    if other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.DIG and
        other.components.workable.action ~= ACTIONS.NET and other.prefab ~= "fossil_stalker" and
        not inst.recentlycharged[other] then
        SpawnPrefab("collapse_small").Transform:SetPosition(other.Transform:GetWorldPosition())
        other.components.workable:Destroy(inst)
        if other:IsValid() and other.components.workable ~= nil and other.components.workable:CanBeWorked() then
            inst.recentlycharged[other] = true
            inst:DoTaskInTime(3, ClearRecentlyCharged, other)
        end
    end
end

local function OnCollide(inst, other)
    if other ~= nil and (other:HasTag("boulder") or other:HasTag("tree")) and
        other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.DIG and
        other.components.workable.action ~= ACTIONS.NET and
        not inst.recentlycharged[other] then
        inst:DoTaskInTime(2 * FRAMES, OnDestroyOther, other)
    end
end

local SNARE_OVERLAP_MIN = 1
local SNARE_OVERLAP_MAX = 3
local function NoSnareOverlap(x, z, r)
    return #TheSim:FindEntities(x, 0, z, r or SNARE_OVERLAP_MIN, { "fossilspike" }) <= 0
end

--Hard limit target list size since casting does multiple passes it
local SNARE_MAX_TARGETS = 100
local SNARE_TAGS = { "_combat", "locomotor"}
local SNARE_YES_TAGS = {"monster", "hostile", "player", "character", "epic"}
local SNARE_NO_TAGS = { "flying", "ghost", "playerghost", "shadowcreature", "shadow", "shadowminion", "INLIMBO", "bird", "stalker", "stalkerminion" }
local function FindSnareTargets(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local targets = {}
    local priorityindex = 1
    local priorityindex2 = 1
    local ents = TheSim:FindEntities(x, y, z, 30, SNARE_TAGS, GetExcludeTagsp(inst), SNARE_YES_TAGS)
    for i, v in ipairs(ents) do
        if not (v.components.health ~= nil and v.components.health:IsDead()) then
            if v:HasTag("player") then
                table.insert(targets, priorityindex, v)
                priorityindex = priorityindex + 1
                priorityindex2 = priorityindex2 + 1
            elseif v.components.combat:TargetIs(inst) then
                table.insert(targets, priorityindex2, v)
                priorityindex2 = priorityindex2 + 1
            else
                table.insert(targets, v)
            end
            if #targets >= SNARE_MAX_TARGETS then
                return targets
            end
        end
    end
    return #targets > 0 and targets or nil
end

local function ChangeToCustomObstacle(inst)
    inst:RemoveEventCallback("animover", ChangeToCustomObstacle)
    local x, y, z = inst.Transform:GetWorldPosition()
    inst.Physics:Stop()
    --inst.Physics:SetMass(99999)
    --inst.Physics:SetCapsule(0.2, 2)
	MakeObstaclePhysics(inst, 1.5)
	--MakeGiantCharacterPhysics(inst, 1000, .75)
	inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
    --inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.ITEMS)
	inst.Physics:CollidesWith(COLLISION.GIANTS)
	inst.Physics:CollidesWith(COLLISION.WORLD)
	--inst.Physics:CollidesWith(COLLISION.PLAYERS)
    inst.Physics:CollidesWith(COLLISION.CHARACTERS)
    inst.Physics:Teleport(x, 0, z)
end

local function ChangeToObstacle(inst) --added to prevent crash when removing fossilspike's on listenforevent.
    inst:RemoveEventCallback("animover", ChangeToObstacle)
    local x, y, z = inst.Transform:GetWorldPosition()
    inst.Physics:Stop()
    inst.Physics:SetMass(0) 
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.ITEMS)
    inst.Physics:CollidesWith(COLLISION.CHARACTERS)
    inst.Physics:Teleport(x, 0, z)
end

local function SpawnSnare(inst, x, z, r, num, target)
    local vars = { 1, 2, 3, 4, 5, 6, 7 }
    local used = {}
    local queued = {}
    local count = 0
    local dtheta = PI * 2 / num
    local thetaoffset = math.random() * PI * 2
    local delaytoggle = 0
    local map = TheWorld.Map
    for theta = math.random() * dtheta, PI * 2, dtheta do
        local x1 = x + r * math.cos(theta)
        local z1 = z + r * math.sin(theta)
        if map:IsPassableAtPoint(x1, 0, z1) then
            local spike = SpawnPrefab("fossilspike")
			--spike:DoTaskInTime(1, function(inst) inst.Physics:CollidesWith(COLLISION.GIANTS) end)
			spike:RemoveEventCallback("animover", ChangeToObstacle)
			spike:ListenForEvent("animover", ChangeToCustomObstacle)
			spike:DoTaskInTime(2, ChangeToCustomObstacle)
            spike.Transform:SetPosition(x1, 0, z1)

            local delay = delaytoggle == 0 and 0 or .2 + delaytoggle * math.random() * .2
            delaytoggle = delaytoggle == 1 and -1 or 1

            local duration = GetRandomWithVariance(TUNING.STALKER_SNARE_TIME, TUNING.STALKER_SNARE_TIME_VARIANCE)

            local variation = table.remove(vars, math.random(#vars))
            table.insert(used, variation)
            if #used > 3 then
                table.insert(queued, table.remove(used, 1))
            end
            if #vars <= 0 then
                local swap = vars
                vars = queued
                queued = vars
            end

            spike:RestartSpike(delay, duration, variation)
            count = count + 1
        end
    end
    if count <= 0 then
        return false
    elseif target:IsValid() then
        target:PushEvent("snared", { attacker = inst })
    end
    return true
end

local function SpawnSnares(inst, targets)
    local count = 0
    local nextpass = {}
	--local x, y, z = inst.Transform:GetWorldPosition()
	--Note for future use: local ents = TheSim:FindEntities(cx, 0, cy, 8, musthavetags, canthavetags, musthaveoneoftags)
    --local ents = TheSim:FindEntities(x, y, z, 10, SNARE_TAGS, SNARE_NO_TAGS, SNARE_YES_TAGS)
    for i, v in ipairs(targets) do
        if v:IsValid() and v:IsNear(inst, 30) and v.prefab ~= "stalkerp" then
            local x, y, z = v.Transform:GetWorldPosition()
            local islarge = v:HasTag("largecreature") or v:HasTag("epic")
            local r = (v.Physics ~= nil and v.Physics:GetRadius() or 0) + (islarge and 1.5 or 1.5)
            local num = islarge and 12 or 6
            if NoSnareOverlap(x, z, r + SNARE_OVERLAP_MAX) then
                if SpawnSnare(inst, x, z, r, num, v) then
                    count = count + 1
                    if count >= TUNING.STALKER_MAX_SNARES then
                        return
                    end
                end
            else
                table.insert(nextpass, { x = x, z = z, r = r, n = num, inst = v })
            end
        end
    end
    if #nextpass <= 0 then
        return
    end
    for range = SNARE_OVERLAP_MAX - 1, SNARE_OVERLAP_MIN, -1 do
        local i = 1
        while i <= #nextpass do
            local v = nextpass[i]
            if NoSnareOverlap(v.x, v.z, v.r + range) then
                if SpawnSnare(inst, v.x, v.z, v.r, v.n, v.inst) then
                    count = count + 1
                    if count >= TUNING.STALKER_MAX_SNARES or #nextpass <= 1 then
                        return
                    end
                end
                table.remove(nextpass, i)
            else
                i = i + 1
            end
        end
    end
end


local wilton_sayings =
{
    --"..grooaan..",
    "..(crunch)..",
   -- "..Y-yy..",
    "..(gurgle)..",
    "Huurgh...",
    "..gruaagh..",
   -- "..W-wwh..",
}

local function test_talk(inst)
    return wilton_sayings[math.random(#wilton_sayings)]
end
 
--==============================================
--				Custom Common Functions
--==============================================	
local function FireProjectile(inst, target)
    if target then
		local pos = inst:GetPosition()
		local spit = SpawnPrefab("spat_bomb")
		spit.Transform:SetPosition(pos.x, 0, pos.z)
		inst:DoTaskInTime(0, function(inst) spit.components.projectile.owner = inst end)
		spit.components.projectile.owner = inst
		spit.components.projectile:Throw(inst, target)
    end
end

----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.components.combat.areahitrange = nil
	inst.components.combat.areahitcheck = nil
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("epic")
	inst:AddTag("stalker")
	inst:AddTag("monster")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)	
	
	inst.components.talker.fontsize = 40
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.colour = Vector3(238 / 255, 69 / 255, 105 / 255)
    inst.components.talker.offset = Vector3(0, -700, 0)
    inst.components.talker.symbol = "fossil_chest"
	inst.components.talker.mod_str_fn = test_talk
	
	inst:WatchWorldState("issummer", function() PlayablePets.SetSandstormImmunity(inst) end) --makes immune to Sandstorm visual issues.
	
	PlayablePets.SetSandstormImmunity(inst)	
end

local master_postinit = function(inst)  
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0, 9999) --fire, acid, poison
	inst.components.combat:SetAreaDamage(4, TUNING.STALKER_AOE_SCALE)
	inst.components.combat:SetHurtSound("dontstarve/creatures/together/stalker/hit")
	----------------------------------
	--Variables		
	inst.mobsleep = true --doesn't sleep, uses taunt state instead.
	inst.taunt = true
	--inst.taunt2 = true
	
	inst.hit_recovery = 2
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	inst.mobplayer = true
	inst.poisonimmune = true
	inst.ghostbuild = "ghost_monster_build"
	
	local body_symbol = "fossil_chest"
	inst.poisonsymbol = body_symbol
	
	inst.AnimState:AddOverrideBuild("stalker_cave_build")
	--MakeLargeBurnableCharacter(inst, body_symbol)
    --MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst:AddComponent("epicscare")
    inst.components.epicscare:SetRange(TUNING.STALKER_EPICSCARE_RANGE)
	
	--inst.SetEngaged = SetEngaged
    inst.FindSnareTargets = FindSnareTargets
    inst.SpawnSnares = SpawnSnares
	
	inst.recentlycharged = {}
    inst.Physics:SetCollisionCallback(OnCollide)
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	
	inst.components.talker.fontsize = 40
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.colour = Vector3(238 / 255, 69 / 255, 105 / 255)
    inst.components.talker.offset = Vector3(0, -700, 0)
    inst.components.talker.symbol = "fossil_chest"
	inst.components.talker.mod_str_fn = test_talk
    --inst.components.talker:MakeChatter()
	
	--inst.components.talker.ontalk = OnTalk
	inst:ListenForEvent("ontalk", OnTalk)
    inst:ListenForEvent("donetalking", OnDoneTalking)
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1.5) --This might multiply food stats.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1000, .75)
    inst.DynamicShadow:SetSize(4, 2)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	inst.components.talker:StopIgnoringAll()
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) inst.components.talker:StopIgnoringAll() end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) inst.components.talker:StopIgnoringAll() end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
