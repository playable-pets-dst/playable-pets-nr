local MakePlayerCharacter = require "prefabs/player_common"

local prefabname = "gibletp"
local assets = 
{
	Asset("ANIM", "anim/perdling_shiny_build_01.zip"),
	Asset("ANIM", "anim/perd_shiny_build_01.zip"),
	Asset("ANIM", "anim/waddler.zip"),
	
}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = 60,
	hunger = 75,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 100,
	runspeed = TUNING.CRITTER_WALK_SPEED,
	walkspeed = TUNING.CRITTER_WALK_SPEED,
	damage = 12,
	attackperiod = 0,
	hit_range = 2,
	range = 1,
	bank = "perdling", 
	build = "perdling_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGgibletp",
	minimap = "gibletp.tex",
	
}

local mob_adult = 
{
	health = 100 * 1.2, --health bonus for growing
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 200,
	runspeed = TUNING.PERD_RUN_SPEED,
	walkspeed = TUNING.PERD_WALK_SPEED,
	damage = 10 * 1.2, --damage bonus for growing
	range = 2,
	hit_range = 3,
	attackperiod = 0,
	bank = "perd", 
	build = "perd",
	scale = 1,
	shiny = "perd",
	--build2 = "alternate build here",
	stategraph = "SGbigperdp",
	minimap = "gibletp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('babygibletp',
-----Prefab---------------------Chance------------
{
    {'drumstick',			1.00},
})

SetSharedLootTable('bigperdp',
-----Prefab---------------------Chance------------
{
    {'drumstick',			1.00},
	{'drumstick',			1.00},
})

-------------------------------------------------------------------
--Catcoon functions--

------------------------------------------------------


-------------------------------------------------------
--Growth Code--

local function SetAdult(inst)
	inst.sg:GoToState("idle")

	PlayablePets.SetCommonStats(inst, mob_adult, nil, true) --mob table, ishuman, ignorepvpmultiplier
	inst.components.ppskin_manager:LoadSkin(mob_adult, true)
	PlayablePets.SetCommonWeatherResistances(inst, 60) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	
	MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(1.5, .75)
    inst.Transform:SetFourFaced()
	
	inst.taunt = false
	inst.shouldwalk = false
	inst.isadult = true --added as a failsafe
	
	inst.components.lootdropper:SetChanceLootTable('bigperdp')
	inst.components.eater:SetAbsorptionModifiers(1,1,1)
end

local function OnGrowUp(inst)
	inst.Transform:SetPosition(inst.Transform:GetWorldPosition())
	inst.isadult = true 
	inst.sg:GoToState("growup")
end

local function applyupgrades(inst)

    local health_percent = inst.components.health:GetPercent()
	local hunger_percent = inst.components.hunger:GetPercent()
	--local sanity_percent = inst.components.sanity:GetPercent()
	
	if inst.isadult == false then
	inst.components.health.maxhealth = mob.health
	inst.components.hunger.max = mob.hunger
	
	else
		inst.components.health.maxhealth = mob_adult.health --give bonus hp for grown up pups
		inst.components.hunger.max = mob_adult.hunger	
	end
    inst.components.health:SetPercent(health_percent)
end

local ADULT_REQ = 45

local function OnEat(inst, food)
	local age = inst.components.age:GetAgeInDays()
    if food and food.components.edible then
		if food.components.edible.foodtype == FOODTYPE.VEGGIE and MOBGROW~= "Disable" and inst.birdfood < 999 then
			--food helps you grow!
			if food.prefab == "dragonpie" then
				inst.birdfood = inst.birdfood + 5
			else
				inst.birdfood = inst.birdfood + 1
			end
			if inst.birdfood >= ADULT_REQ and inst.isadult == false then
				--inst.sg:GoToState("transform")
				OnGrowUp(inst)
				applyupgrades(inst)
			end
		end
    end
end

local function ondeath(inst)
   inst.birdfood = 0
   inst.isadult = false
   applyupgrades(inst)
end
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.birdfood = data.birdfood or 0
		inst.isadult = data.isadult or false
		
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.birdfood = inst.birdfood > 0 and inst.birdfood or nil
	data.isadult = inst.isadult or false
	
end

local function OnPreload(inst, data)
    if data ~= nil and data.isadult ~= nil then
        inst.isadult = data.isadult
        applyupgrades(inst)
        --re-set these from the save data, because of load-order clipping issues
        if data.health and data.health.health then inst.components.health:SetCurrentHealth(data.health.health) end
        if data.hunger and data.hunger.hunger then inst.components.hunger.current = data.hunger.hunger end
        --if data.sanity and data.sanity.current then inst.components.sanity.current = data.sanity.current end
        inst.components.health:DoDelta(0)
        inst.components.hunger:DoDelta(0)
        inst.components.sanity:DoDelta(0)
		if inst.isadult then
			SetAdult(inst)
		end
    end
end

local function GetMobTable(inst)
	if inst.birdfood < ADULT_REQ then
		return mob
	elseif inst.birdfood >= ADULT_REQ then
		return mob_adult
	end
end

local function SetSkinDefault(inst, data)
	if not inst:HasTag("playerghost") then
		if data then
			if inst.isadult ~= nil and inst.isadult == true then
				inst.AnimState:SetBuild(data.adult_build)
			else	
				inst.AnimState:SetBuild(data.build)			
			end
		else
			inst.AnimState:SetBuild(inst.isadult and mob_adult.build or mob.build)
		end	
	end
end

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end

local master_postinit = function(inst)  
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('gibletp')
	----------------------------------
	--Tags--
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	inst.mobplayer = true
	
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	inst.ghostbuild = "ghost_monster_build"
	----------------------------------
	--Growth--
	inst.birdfood = 0
	
	inst.isadult = false
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	inst.userfunctions =
    {
        SetAdult = SetAdult,
    }
	
	---------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,2,1) --This might multiply food stats.
    inst.components.eater:SetOnEatFn(OnEat) --Runs a function when the player eats something.
	---------------------------------
	--Physics and Scale--
	MakeCharacterPhysics(inst, 50, 0.5)
    inst.DynamicShadow:SetSize(1.5, .75)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("death", ondeath)
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    inst.OnPreLoad = OnPreload
  	------------------------------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst)) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
    inst:ListenForEvent("respawnfromghost", function() 
		inst:DoTaskInTime(5, function(inst) PlayablePets.SetCommonStats(inst, GetMobTable(inst), nil, true) end)
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst)) end)
    end)
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
