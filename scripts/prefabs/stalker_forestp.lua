local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "stalker_forestp"

local assets = 
{
	Asset("ANIM", "anim/stalker_forest_shiny_build_01.zip"),
	
	Asset("ANIM", "anim/stalker_forest.zip"),
    Asset("ANIM", "anim/stalker_shadow_build.zip"),
    Asset("ANIM", "anim/stalker_forest_build.zip"),
}



local prefabs = 
{	
	"shadowheart",
    "fossil_piece",
    "nightmarefuel",
    "stalker_bulb",
    "stalker_berry",
    "stalker_fern",
    "damp_trail",
}
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 4000,
	hunger = 225,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 150,
	runspeed = TUNING.STALKER_SPEED,
	walkspeed = TUNING.STALKER_SPEED,
	damage = 200,
	range = 4,
	bank = "stalker_forest",
	build = "stalker_shadow_build",
	build2 = "stalker_forest_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGstalker_forestp",
	minimap = "stalker_forestp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('stalkerp',
{
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    --{"nightmarefuel",       1.00},
    --{"nightmarefuel",       1.00},
   -- {"nightmarefuel",       0.50},
    --{"nightmarefuel",       0.50},
})

local FORGE_STATS = PPNR_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================
local function SetSkinDefault(inst, data)
	if data then
		inst.AnimState:SetBuild(data.build2 or mob.build)
		inst.AnimState:AddOverrideBuild(data.build)
	else
		inst.AnimState:SetBuild(mob.build)
		inst.AnimState:AddOverrideBuild(mob.build2)
	end	
end

local function OnDoneTalking(inst)
    if inst.talktask ~= nil then
        inst.talktask:Cancel()
        inst.talktask = nil
    end
    inst.SoundEmitter:KillSound("talk")
end

local function OnTalk(inst)
    OnDoneTalking(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/stalker/talk_LP", "talk")
    inst.talktask = inst:DoTaskInTime(1.5 + math.random() * .5, OnDoneTalking)
end

local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function OnDestroyOther(inst, other)
	if other ~= nil and other:HasTag("wall") then
		local isname = inst:GetDisplayName()
		local selfpos = inst:GetPosition()
		print("LOGGED: "..isname.." destroyed a wall at "..selfpos.x..","..selfpos.y..","..selfpos.z.." as "..inst.prefab)
	end	
    if other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.DIG and
        other.components.workable.action ~= ACTIONS.NET and other.prefab ~= "fossil_stalker" and
        not inst.recentlycharged[other] then
        SpawnPrefab("collapse_small").Transform:SetPosition(other.Transform:GetWorldPosition())
        other.components.workable:Destroy(inst)
        if other:IsValid() and other.components.workable ~= nil and other.components.workable:CanBeWorked() then
            inst.recentlycharged[other] = true
            inst:DoTaskInTime(3, ClearRecentlyCharged, other)
        end
    end
end

local function OnCollide(inst, other)
    if other ~= nil and (other:HasTag("boulder") or other:HasTag("tree")) and
        other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.DIG and
        other.components.workable.action ~= ACTIONS.NET and
        not inst.recentlycharged[other] then
        inst:DoTaskInTime(2 * FRAMES, OnDestroyOther, other)
    end
end

local MAX_TRAIL_VARIATIONS = 7
local MAX_RECENT_TRAILS = 4
local TRAIL_MIN_SCALE = 1
local TRAIL_MAX_SCALE = 1.6

local function PickTrail(inst)
    local rand = table.remove(inst.availabletrails, math.random(#inst.availabletrails))
    table.insert(inst.usedtrails, rand)
    if #inst.usedtrails > MAX_RECENT_TRAILS then
        table.insert(inst.availabletrails, table.remove(inst.usedtrails, 1))
    end
    return rand
end

local function RefreshTrail(inst, fx)
    if fx:IsValid() then
        fx:Refresh()
    else
        inst._trailtask:Cancel()
        inst._trailtask = nil
    end
end

local function DoTrail(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    if inst.sg:HasStateTag("moving") then
        local theta = -inst.Transform:GetRotation() * DEGREES
        x = x + math.cos(theta)
        z = z + math.sin(theta)
    end
    local fx = SpawnPrefab("damp_trail")
    fx.Transform:SetPosition(x, 0, z)
    fx:SetVariation(PickTrail(inst), GetRandomMinMax(TRAIL_MIN_SCALE, TRAIL_MAX_SCALE), TUNING.STALKER_BLOOM_DECAY)
    if inst._trailtask ~= nil then
        inst._trailtask:Cancel()
    end
    inst._trailtask = inst:DoPeriodicTask(TUNING.STALKER_BLOOM_DECAY * .5, RefreshTrail, nil, fx)
end

local BLOOM_CHOICES =
{
    ["stalker_bulb"] = .1,
    ["stalker_bulb_double"] = .1,
    ["stalker_berry"] = 0.5,
    ["stalker_fern"] = 2,
}

local function DoPlantBloom(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local map = TheWorld.Map
    local offset = FindValidPositionByFan(
        math.random() * 2 * PI,
        math.random() * 3,
        8,
        function(offset)
            local x1 = x + offset.x
            local z1 = z + offset.z
            return map:IsPassableAtPoint(x1, 0, z1)
                and map:IsDeployPointClear(Vector3(x1, 0, z1), nil, 3)
                and #TheSim:FindEntities(x1, 0, z1, 6, { "stalkerbloom" }) < 2
        end
    )

    if offset ~= nil and inst._blooming ~= false then
        SpawnPrefab(weighted_random_choice(BLOOM_CHOICES)).Transform:SetPosition(x + offset.x, 0, z + offset.z)
    end
end

local function OnStartBlooming(inst)
    DoTrail(inst)
    inst._bloomtask = inst:DoPeriodicTask(3 * FRAMES, DoPlantBloom, 2 * FRAMES)
end

local function _StartBlooming(inst)
    if inst._bloomtask == nil then
        inst._bloomtask = inst:DoTaskInTime(0, OnStartBlooming)
    end
end

local function StartBlooming(inst)
    if not inst._blooming then
        inst._blooming = true
        _StartBlooming(inst)
    end
end

local function StopBlooming(inst)
    if inst._blooming then
        inst._blooming = false
    end
end

local function OnDecay(inst)
    if not inst.components.health:IsDead() then
        --No chance fuel drops if decayed due to daylight
        inst.components.lootdropper:SetLoot(nil)
        inst.components.health:Kill()
    end
end

local function OnIsNight(inst, isnight)
    if isnight then
        if inst._decaytask ~= nil then
            inst._decaytask:Cancel()
            inst._decaytask = nil
        end
    elseif inst._decaytask == nil then
        inst._decaytask = inst:DoTaskInTime(2 + math.random(), OnDecay)
    end
end

local wilton_sayings =
{
    --"..grooaan..",
    "..(crunch)..",
   -- "..Y-yy..",
    "..(gurgle)..",
    "Huurgh...",
    "..gruaagh..",
   -- "..W-wwh..",
}
 
--==============================================
--				Custom Common Functions
--==============================================	
local function FireProjectile(inst, target)
    if target then
		local pos = inst:GetPosition()
		local spit = SpawnPrefab("spat_bomb")
		spit.Transform:SetPosition(pos.x, 0, pos.z)
		inst:DoTaskInTime(0, function(inst) spit.components.projectile.owner = inst end)
		spit.components.projectile.owner = inst
		spit.components.projectile:Throw(inst, target)
    end
end

----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.healmult = 3
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "darts"})
	end)
	
	inst:AddComponent("buffable")
	inst.components.buffable:AddBuff("heal_dealt", {{name = "heal_dealt", val = 1.25, type = "mult"}})
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 2
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("epic")
	inst:AddTag("stalker")
	inst:AddTag("monster")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)	
	
	--[[
	inst.components.talker.fontsize = 40
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.colour = Vector3(238 / 255, 69 / 255, 105 / 255)
    inst.components.talker.offset = Vector3(0, -700, 0)
    inst.components.talker.symbol = "fossil_chest"
	inst.components.talker.mod_str_fn = test_talk]]
	
	inst:WatchWorldState("issummer", function() PlayablePets.SetSandstormImmunity(inst) end) --makes immune to Sandstorm visual issues.
	
	PlayablePets.SetSandstormImmunity(inst)	
end

local master_postinit = function(inst)  
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0, 9999) --fire, acid, poison
	inst.components.combat:SetAreaDamage(4, TUNING.STALKER_AOE_SCALE)
	inst.components.combat:SetHurtSound("dontstarve/creatures/together/stalker/hit")
	----------------------------------
	--Variables		
	inst.mobsleep = true --doesn't sleep, uses taunt state instead.
	inst.altattack = true
	
	inst.hit_recovery = 2
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	inst.mobplayer = true
	inst.poisonimmune = true
	inst.ghostbuild = "ghost_monster_build"
	
	inst.AnimState:AddOverrideBuild("stalker_forest_build") 
	
	inst.usedtrails = {}
    inst.availabletrails = {}
    for i = 1, MAX_TRAIL_VARIATIONS do
        table.insert(inst.availabletrails, i)
    end
	
	inst._blooming = false
    inst.DoTrail = DoTrail
    inst.StartBlooming = StartBlooming
    inst.StopBlooming = StopBlooming
	if not PP_FORGE_ENABLED then
		StartBlooming(inst)
	end
	
	local body_symbol = "fossil_chest"
	inst.poisonsymbol = body_symbol
	--MakeLargeBurnableCharacter(inst, body_symbol)
    --MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.recentlycharged = {}
    inst.Physics:SetCollisionCallback(OnCollide)
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, "stalkerp") --inst, prefaboverride
	
	--[[
	inst.components.talker.fontsize = 40
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.colour = Vector3(238 / 255, 69 / 255, 105 / 255)
    inst.components.talker.offset = Vector3(0, -700, 0)
    inst.components.talker.symbol = "fossil_chest"
	inst.components.talker.mod_str_fn = test_talk
    --inst.components.talker:MakeChatter()
	
	--inst.components.talker.ontalk = OnTalk
	inst:ListenForEvent("ontalk", OnTalk)
    inst:ListenForEvent("donetalking", OnDoneTalking)]]
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1.5) --This might multiply food stats.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1000, .75)
    inst.DynamicShadow:SetSize(4, 2)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
