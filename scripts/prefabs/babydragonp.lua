local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------

---------------------------


local assets = 
{
	Asset("ANIM", "anim/dragonling_shiny_build_01.zip"),
	Asset("ANIM", "anim/dragonfly_shiny_build_01.zip"),
	Asset("ANIM", "anim/dragonfly_shiny_build_02.zip"),
	Asset("ANIM", "anim/dragonfly_fire_shiny_build_02.zip"),
	Asset("ANIM", "anim/dragonfly_fire_shiny_build_01.zip"),
	
}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = BOSS_STATS and 750 or 250,
	hunger = 150,
	hungerrate = 0.12,
	sanity = 100,
	runspeed = TUNING.CRITTER_WALK_SPEED,
	walkspeed = TUNING.CRITTER_WALK_SPEED,
	damage = 20,
	range = 2,
	hit_range = 3,
	bank = "dragonling", 
	build = "dragonling_build",
	shiny = "dragonling",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGbabydragonp",
	minimap = "babydragonp.tex",
	
}

local mob_adult = 
{
	health = BOSS_STATS and TUNING.DRAGONFLY_HEALTH or 5000 * 1.2,
	hunger = 500,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 200,
	runspeed = TUNING.DRAGONFLY_SPEED,
	walkspeed = TUNING.DRAGONFLY_SPEED,
	damage = 75*2,
	damage2 = 90*2,
	attackperiod = TUNING.DRAGONFLY_ATTACK_PERIOD,
	range = TUNING.DRAGONFLY_ATTACK_RANGE,
	hit_range = 6,
	bank = "dragonfly",
	build = "dragonfly_build",
	shiny = "dragonfly",
	scale = 1.3,
	--build2 = "alternate build here",
	stategraph = "SGdragonp",
	minimap = "babydragonp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('babydragonp',
-----Prefab---------------------Chance------------
{
    {'meat',            1.00},
    {'charcoal',        0.50},
    
   
})

SetSharedLootTable('dragonp', 
-----Prefab---------------------Chance------------
{
	{'dragon_scales',    1.00},
	{'lavae_egg',    	 0.33},
    {'meat',             1.00},
    {'meat',             1.00},
    {'meat',             1.00},
    {'meat',             1.00},
    {'meat',             1.00},
    {'meat',             1.00},
})



-------------------------------------------------------------------
--Dragonfly functions--
local function TransformNormal(inst)
    inst.enraged = false
	inst.components.ppskin_manager:LoadSkin(mob_adult, true)
    --Set normal stats
	inst.Light:Enable(true)
	inst.Light:SetRadius(0)
	inst.Light:SetFalloff(2)
	inst.Light:SetIntensity(4)
	inst.Light:SetColour(245/255,40/255,0/255)
    inst.components.locomotor.walkspeed = mob_adult.walkspeed
    inst.components.combat:SetDefaultDamage(mob_adult.damage)


    inst.components.propagator:StopSpreading()
end

local function TransformFire(inst)
    inst.enraged = true
	inst.components.ppskin_manager:LoadSkin(mob_adult, true)
    inst.can_ground_pound = true
    --Set fire stats
    inst.components.locomotor.walkspeed = TUNING.DRAGONFLY_FIRE_SPEED
    inst.components.combat:SetDefaultDamage(mob_adult.damage2)
	
	inst.Light:Enable(true)
	inst.Light:SetRadius(5)
	inst.Light:SetFalloff(0.9)
	inst.Light:SetIntensity(0.6)
	if inst.components.ppskin_manager:GetSkin() then
		--TODO just make a table for this
		inst.Light:SetColour(0/255,157/255,255/255)
	else
		inst.Light:SetColour(180/255,195/255,150/255)
	end
	if MOBFIRE== "Disable" then 
		inst.components.propagator:StartSpreading()
	end
end
------------------------------------------------------

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("dragonfly") and not dude.components.health:IsDead() end, 10)
    end
end

-------------------------------------------------------
--Growth Code--

local function SetAdult(inst)
	inst.sg:GoToState("idle")	
	
	inst:RemoveTag("lavae")
	inst:AddTag("flying")
	inst:AddTag("epic")
	
	inst:AddComponent("groundpounder")
	inst.components.groundpounder.numRings = 2
	inst.components.groundpounder.damageRings = 0
	if MOBFIRE== "Disable" then 
		inst.components.groundpounder.burner = true
	end  
	inst.components.combat:SetHurtSound("dontstarve_DLC001/creatures/dragonfly/hurt")
	
    inst.components.groundpounder.groundpoundfx = "firesplash_fx"
    inst.components.groundpounder.groundpounddamagemult = 1.25
    inst.components.groundpounder.groundpoundringfx = "firering_fx"
	inst.components.groundpounder.noTags = {"lavae"}
	
	PlayablePets.SetCommonStats(inst, mob_adult) --mob table, ishuman, ignorepvpmultiplier
	inst.components.ppskin_manager:LoadSkin(mob_adult, true)
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0) --fire, acid, poison, freeze (flat value, not a multiplier)
	
	MakeFlyingGiantCharacterPhysics(inst, 500, 1.4)
	inst.DynamicShadow:SetSize(6, 3.5)
    inst.components.combat.hiteffectsymbol = "dragonfly_body"
	
	PlayablePets.SetAmphibious(inst, nil, nil, true, nil, true) --have to run it again due to physic changes
	
	local body_symbol = "dragonfly_body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
    
    inst.Transform:SetSixFaced()
	
	inst.taunt = false
	inst.mobsleep = true
	inst.isdragonrage = false
	inst.taunt2 = true
	inst.isadult = true --added as a failsafe
	
	inst.components.lootdropper:SetChanceLootTable('dragonp')

	inst.components.eater:SetAbsorptionModifiers(1,2,2)
	
	inst.TransformFire = TransformFire
    inst.TransformNormal = TransformNormal
	inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/dragonfly/fly", "flying")
	inst.Transform:SetScale(1.3,1.3,1.3)
    
end

local function OnGrowUp(inst)
	inst.Transform:SetPosition(inst.Transform:GetWorldPosition())
	inst.isadult = true 
	inst.sg:GoToState("growup")
end

local function applyupgrades(inst)

    local health_percent = inst.components.health:GetPercent()
	local hunger_percent = inst.components.hunger:GetPercent()
	--local sanity_percent = inst.components.sanity:GetPercent()
	
	if inst.isadult == false then
		inst.components.health.maxhealth = mob.health
		inst.components.hunger.max = mob.hunger
	else
		inst.components.health.maxhealth = mob_adult.health --give bonus hp for grown up pups
		inst.components.hunger.max = mob_adult.hunger	
	end
    inst.components.health:SetPercent(health_percent)
	inst.components.hunger:SetPercent(hunger_percent)
end

local ADULT_REQ = 150

local function OnEat(inst, food)
	local age = inst.components.age:GetAgeInDays()
	--local dragon_config = MOB_DRAGONFLY or nil
    if food and food.components.edible then
		if food.components.edible.foodtype == FOODTYPE.MEAT and MOBGROW== "Enable" then
			--food helps you grow!
				inst.dragonfood = inst.dragonfood + 1
			if inst.dragonfood >= ADULT_REQ and inst.isadult == false then
				OnGrowUp(inst)
				applyupgrades(inst)
			end
		end
    end
end

local function ondeath(inst)
	inst.dragonfood = 0
	inst.isadult = false
	applyupgrades(inst)
	local body_symbol = "head"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	inst.components.combat:SetHurtSound(nil)
end
-------------------------------------------------------
----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.dragonfood = data.dragonfood or 0
		inst.isadult = data.isadult or false
		
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.dragonfood = inst.dragonfood > 0 and inst.dragonfood or nil
	data.isadult = inst.isadult or false
	
end

local function OnPreload(inst, data)
    if data ~= nil and data.isadult ~= nil then
        inst.isadult = data.isadult
        applyupgrades(inst)
        --re-set these from the save data, because of load-order clipping issues
        if data.health and data.health.health then inst.components.health:SetCurrentHealth(data.health.health) end
        if data.hunger and data.hunger.hunger then inst.components.hunger.current = data.hunger.hunger end
        --if data.sanity and data.sanity.current then inst.components.sanity.current = data.sanity.current end
        inst.components.health:DoDelta(0)
        inst.components.hunger:DoDelta(0)
        inst.components.sanity:DoDelta(0)
		if inst.isadult then
			SetAdult(inst)
		end
    end
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
	inst:WatchWorldState( "issummer", function() PlayablePets.SetSandstormImmunity(inst) end) --makes immune to Sandstorm visual issues.
	
	PlayablePets.SetSandstormImmunity(inst)
	
end

local function SetSkinDefault(inst, data)
	if not inst:HasTag("playerghost") then
		if data then
			if inst.isadult ~= nil and inst.isadult == true then
				inst.AnimState:SetBuild(inst.enraged and data.rage_build or data.adult_build)
			else	
				inst.AnimState:SetBuild(data.build)
			end
		else
			if inst.isadult ~= nil and inst.isadult == true then
				inst.AnimState:SetBuild(inst.enraged and "dragonfly_fire_build" or mob_adult.build)
			else	
				inst.AnimState:SetBuild(mob.build)
			end
		end	
	end
end

local function GetMobTable(inst)
	if inst.dragonfood < ADULT_REQ then
		return mob
	elseif inst.dragonfood >= ADULT_REQ then
		return mob_adult
	end
end


local master_postinit = function(inst)  
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, nil, nil, 9999) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('babydragonp')
	----------------------------------
	--Tags--
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	inst.mobplayer = true
	inst:AddTag("lavae")
	
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	local body_symbol = "head"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Growth--
	inst.dragonfood = 0
	inst.isadult = false
	
	inst.userfunctions =
    {
        SetAdult = SetAdult,
    }
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--PvP Mode Stuff--
	inst.mobteleported = false
	----------------------------------
	--Eater--
	
	--Kinds of diets I know of: MEAT, ROUGHAGE(grass and stuff), GEARS, OMNI(no idea it prevents eating entirely, just disable this if you want both meat and veggies).
	inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.BURNT }, { FOODTYPE.MEAT, FOODTYPE.BURNT }) 
    inst.components.eater:SetAbsorptionModifiers(1,3,3) --This might multiply food stats.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
    inst.components.eater:SetOnEatFn(OnEat) --Runs a function when the player eats something.
	---------------------------------
	--Physics and Scale--
	MakeGhostPhysics(inst, 1, .5) --Special physics for flying characters(?)
    inst.DynamicShadow:SetSize(2, .75)
    inst.Transform:SetSixFaced()
	PlayablePets.SetAmphibious(inst, nil, nil, true, nil, true)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", onattacked)
	inst:ListenForEvent("death", ondeath)
	---------------------------------
	--Functions that saves and loads data.
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    inst.OnPreLoad = OnPreload
  	------------------------------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst), true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
    inst:ListenForEvent("respawnfromghost", function() 
		inst:DoTaskInTime(5, function(inst) PlayablePets.SetCommonStats(inst, GetMobTable(inst), nil, true) end)
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst), true, true) 
			inst.components.lootdropper:SetChanceLootTable('babydragonp')
			inst.DynamicShadow:SetSize(2, .75)
		end)
    end)
	
    return inst	
end

return MakePlayerCharacter("babydragonp", prefabs, assets, common_postinit, master_postinit, start_inv)
