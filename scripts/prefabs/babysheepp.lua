local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--Make it grow up into a hound.
---------------------------


local assets = 
{
	Asset("ANIM", "anim/sheepington_shiny_build_01.zip"),
}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = 175,
	hunger = 100,
	hungerrate = 0.12,
	sanity = 100,
	runspeed = TUNING.CRITTER_WALK_SPEED,
	walkspeed = TUNING.CRITTER_WALK_SPEED,
	damage = 15,
	range = 1,
	bank = "sheepington", 
	build = "sheepington_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGbabysheepp",
	minimap = "babysheepp.tex",
	
}

local mob_adult = 
{
	health = 600 * 1.2,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 150,
	runspeed = 6,
	walkspeed = 2,
	damage = 40*2,
	attackperiod = 2,
	range = 12,
	hit_range = 40,
	bank = "spat",
	build = "spat_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGsheepp",
	minimap = "sheepp.tex",
	
}------------------
--Loot that drops when you die, duh.
SetSharedLootTable('babysheepp',
-----Prefab---------------------Chance------------
{
    {'meat',            1.00},
    {'steelwool',       1.00},
    {'phlegm',          0.50},
    
   
})

SetSharedLootTable('bigsheepp',
-----Prefab---------------------Chance------------
{
    {'meat',            1.00},
    {'meat',            1.00},
    {'meat',            1.00},
    {'meat',            1.00},
    {'steelwool',       1.00},
    {'steelwool',       1.00},
    {'steelwool',       0.50},
    {'phlegm',          1.00},
    {'phlegm',          0.50},
})

-------------------------------------------------------------------
--Ewecus functions--
local sounds =
{
    walk = "dontstarve/creatures/spat/walk",
    grunt = "dontstarve/creatures/spat/grunt",
    yell = "dontstarve/creatures/spat/yell",
    hit = "dontstarve/creatures/spat/hurt",
    death = "dontstarve/creatures/spat/death",
    curious = "dontstarve/creatures/spat/curious",
    sleep = "dontstarve/creatures/spat/sleep",
    angry = "dontstarve/creatures/spat/angry",
    spit = "dontstarve/creatures/spat/spit",
    spit_hit = "dontstarve/creatures/spat/spit_hit",
}
------------------------------------------------------

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("spat") and not dude.components.health:IsDead() end, 10)
    end
end

local function onhitother2(inst, other)
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("spat") and not dude.components.health:IsDead() end, 10)
end
--------------------------------------------------------
--Ammo--
local function OnProjectileHit(inst, attacker, other)
	inst.SoundEmitter:PlaySound(sounds.spit_hit)
    local x, y, z = inst.Transform:GetWorldPosition()
    SpawnPrefab("spat_splat_fx").Transform:SetPosition(x, 0, z)
	print(other.prefab)
    if other ~= nil and other:IsValid() then
        if other ~= nil then
            other.components.combat:GetAttacked(inst.owner, 30)
        end
        if other.components.pinnable ~= nil then
            other.components.pinnable:Stick()
        end
    end
    inst:Remove()
end


local function FireProjectile(inst, target)
    if target then
		local pos = inst:GetPosition()
		local targetpos = target:GetPosition()
		--I am simply too lazy to make my own projectile, so I'll just mess with the normal one.
		local spit = SpawnPrefab("spat_bomb")
		spit.Transform:SetPosition(pos.x, 0, pos.z)
		spit:RemoveComponent("complexprojectile")
		spit:AddComponent("projectile")
		spit.components.projectile:SetSpeed(30)
		spit.components.projectile:SetHoming(false)
		spit.components.projectile:SetHitDist(1)
		spit.components.projectile:SetOnHitFn(OnProjectileHit)
		spit.components.projectile:SetOnMissFn(inst.Remove)
		spit.Physics:SetCollisionCallback(nil)
		--inst.components.projectile:SetOnThrownFn(OnThrown)
		spit.components.projectile.owner = inst
		spit.components.projectile:Throw(inst, target)
    end
end
-------------------------------------------------------
local function SetSkinDefault(inst, data)
	if not inst:HasTag("playerghost") then
		if data then
			if inst.isadult ~= nil and inst.isadult == true then
				inst.AnimState:SetBuild(data.adult_build)
			else	
				inst.AnimState:SetBuild(data.build)			
			end
		else
			inst.AnimState:SetBuild(inst.isadult and mob_adult.build or mob.build)
		end	
	end
end
-------------------------------------------------------
--Growth Code--

local function SetAdult(inst)
	inst.sg:GoToState("idle")	

	MakeCharacterPhysics(inst, 100, .5)
    inst.DynamicShadow:SetSize(6, 2)
    inst.Transform:SetSixFaced()
	
	inst.isadult = true --added as a failsafe
	inst._ismonster = true
	inst.altattack = true
	inst.FireProjectile = FireProjectile
	inst.components.lootdropper:SetChanceLootTable('bigsheepp')
	
	PlayablePets.SetCommonStats(inst, mob_adult) --mob table, ishuman, ignorepvpmultiplier
	inst.components.ppskin_manager:LoadSkin(mob_adult, true)
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 2, 0.5, 0.25) --fire, acid, poison, freeze (flat value, not a multiplier)

	inst.components.eater:SetAbsorptionModifiers(2,1,1)

	inst:AddComponent("shedder")
	inst.components.shedder.shedItemPrefab = "steelwool"
	inst.components.shedder.shedHeight = 0.1
	inst.components.shedder:StartShedding(1200) --Note: 480 is 1 day. 480 x n = n amount of days.
end

local function OnGrowUp(inst)
	inst.Transform:SetPosition(inst.Transform:GetWorldPosition())
	inst.isadult = true 
	inst.sg:GoToState("growup")
end

local function applyupgrades(inst)

    local health_percent = inst.components.health:GetPercent()
	local hunger_percent = inst.components.hunger:GetPercent()
	--local sanity_percent = inst.components.sanity:GetPercent()
	
	if inst.isadult == false then
		inst.components.health.maxhealth = mob.health
		inst.components.hunger.max = mob.hunger	
	else
		inst.components.health.maxhealth = mob_adult.health--give bonus hp for grown up pups
		inst.components.hunger.max = mob_adult.hunger	
	end
    inst.components.health:SetPercent(health_percent)
end

local function OnEat(inst, food)
	local age = inst.components.age:GetAgeInDays()
    if food and food.components.edible then
		if MOBGROW~= "Disable" and MOB_EWECUS == "Enable" then
			--food helps you grow!
			inst.sheepfood = inst.sheepfood + 1
			if inst.sheepfood >= 130 and inst.isadult == false then
				OnGrowUp(inst)
				applyupgrades(inst)
			end
		end
    end
end

local function ondeath(inst)
	inst.sheepfood = 0
	inst.isadult = false
	inst.monster_user = nil
	inst.FireProjectile = nil
	applyupgrades(inst)
end
-------------------------------------------------------
----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.sheepfood = data.sheepfood or 0
		inst.isadult = data.isadult or false
		
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.sheepfood = inst.sheepfood > 0 and inst.sheepfood or nil
	data.isadult = inst.isadult or false
	
end

local function OnPreload(inst, data)
    if data ~= nil and data.isadult ~= nil then
        inst.isadult = data.isadult
        applyupgrades(inst)
        --re-set these from the save data, because of load-order clipping issues
        if data.health and data.health.health then inst.components.health:SetCurrentHealth(data.health.health) end
        if data.hunger and data.hunger.hunger then inst.components.hunger.current = data.hunger.hunger end
        --if data.sanity and data.sanity.current then inst.components.sanity.current = data.sanity.current end
        inst.components.health:DoDelta(0)
        inst.components.hunger:DoDelta(0)
        inst.components.sanity:DoDelta(0)
		if inst.isadult then
			SetAdult(inst)
		end
    end
end

local common_postinit = function(inst) 

	inst.MiniMapEntity:SetIcon(mob.minimap)
	
	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	
end

local function GetMobTable(inst)
	if inst.isadult then
		return mob_adult
	else
		return mob
	end
end

local master_postinit = function(inst)  
	------------------------------------------
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, nil, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, nil, nil, nil, 9999) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('babysheepp')
	----------------------------------
	--Tags--
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.	
	inst:AddTag("spat")
    inst:AddTag("animal")
    inst:AddTag("largecreature")
	
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	inst.sheeprun = true
	inst.ghostbuild = "ghost_monster_build"
	inst.mobplayer = true
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	----------------------------------
	--Growth--
	inst.sheepfood = 0
	inst.isadult = false
	inst.sounds = sounds
	
	inst.userfunctions =
    {
        SetAdult = SetAdult,
    }
	----------------------------------
	--PvP Mode Stuff--
	inst.mobteleported = false
	----------------------------------
	--Eater--
	
	--Kinds of diets I know of: MEAT, ROUGHAGE(grass and stuff), GEARS, OMNI(no idea it prevents eating entirely, just disable this if you want both meat and veggies).
	inst.components.eater:SetDiet({ FOODTYPE.ROUGHAGE, FOODTYPE.VEGGIE }, { FOODTYPE.ROUGHAGE, FOODTYPE.VEGGIE}) 
    inst.components.eater:SetAbsorptionModifiers(4,3,3) --This might multiply food stats.
    inst.components.eater:SetOnEatFn(OnEat) --Runs a function when the player eats something.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 1, .5)
    inst.DynamicShadow:SetSize(2, .75)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("death", ondeath)
	---------------------------------
	--Functions that saves and loads data.
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    inst.OnPreLoad = OnPreload
  	------------------------------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst)) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
    inst:ListenForEvent("respawnfromghost", function() 
		inst:DoTaskInTime(5, function(inst) PlayablePets.SetCommonStats(inst, GetMobTable(inst), nil, true) end)
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst)) end)
    end)
	
    return inst
end

return MakePlayerCharacter("babysheepp", prefabs, assets, common_postinit, master_postinit, start_inv)
