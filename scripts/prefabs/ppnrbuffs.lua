--WARNING, these buffs are primarily made for extra perks in the forged forge mod. Using any of these in base game will most likely crash you!
local assets = {

}

local prefabs = {

}

local TARGETING_RANGE = 50
local MAX_RANGE = 1200
local DURATION = 15

local function CanTarget(target, mob)
    -- this checks for notarget tag, isvalid, isvisible, is not dead etc
    return mob.components.combat:CanTarget(target) --and not (mob:HasTag("companion") and target:HasTag("LA_mob"))
end

local function GetLeader(mob)
    return mob.components.follower and mob.components.follower.leader
end

local function GetTime(target)
	return target:HasTag("epic") and DURATION/3 or DURATION
end

-- Returns the closest target to the given mob
-- exclude_target will only return the exclude target if all other targets are dead
local function FindClosestTarget(mob, targets, exclude_target)
    local closest = {target = false, distsq = MAX_RANGE, exclude_target = false, exclude_target_distsq = MAX_RANGE}
    local mob_pos = mob:GetPosition()
    local function CompareClosest(target, dist_index, target_index)
        local curdistsq = distsq(mob_pos, target:GetPosition())
        if curdistsq < closest[dist_index] then
            closest[dist_index] = curdistsq
            closest[target_index] = target
        end
    end
    for _,target in pairs(targets or AllPlayers) do
        if CanTarget(target, mob) then
            if target == exclude_target then
                CompareClosest(target, "exclude_target_distsq", "exclude_target")
            else
                CompareClosest(target, "distsq", "target")
            end
        end
    end
    return closest.target or closest.exclude_target or nil, math.sqrt(closest.distsq or closest.exclude_target_distsq)
end

local function MindControlRetarget(inst)
	local pos = inst:GetPosition()
	local ents = TheSim:FindEntities(pos.x, 0, pos.z, 30, {"LA_mob"}, {"player", "companion", "notarget"})
	inst.components.combat:EngageTarget(FindClosestTarget(inst, ents))
end

local function OnAttached(inst, target)
	inst.components.timer:StartTimer(inst.prefab.."buffover", GetTime(target))
	
    inst.entity:SetParent(target.entity)
    inst:ListenForEvent("death", function()
        inst.components.debuff:Stop()
    end, target)
	
	local fx = SpawnPrefab("sanity_raise")
	--fx.AnimState:SetMultColour(1, 1, 1, 1)
	fx.entity:SetParent(target.entity)
	
	local _d = 0.25
	target:DoTaskInTime(1, function(target)
		SpawnPrefab("pp_mindcontrol_aoe").Transform:SetPosition(target:GetPosition():Get())
		target.SoundEmitter:PlaySound("dontstarve/creatures/together/stalker/death3/orchhits")
		target.AnimState:SetMultColour(_d, _d, _d, 1)
	
		if target.components.combat.target then
			target.components.combat.target = nil
		end
		target:AddTag("companion")
		target:RemoveTag("LA_mob")
	
		target.components.combat.targetfn_old = target.components.combat.targetfn
		target.components.combat:SetRetargetFunction(2, MindControlRetarget)
	end)
	
end

local function OnTimerDone(inst, data)
    if data.name == inst.prefab.."buffover" then
        inst.components.debuff:Stop()
    end
end

local function OnExtended(inst, target)
    inst.components.timer:StopTimer(inst.prefab.."buffover")
    inst.components.timer:StartTimer(inst.prefab.."buffover", GetTime(target))
end

local function OnDetached(inst, target)
	target:AddTag("LA_mob")
	target:RemoveTag("companion")
	
	local fx = SpawnPrefab("sanity_lower")
	fx.Transform:SetPosition(target:GetPosition():Get())	
	--fx.AnimState:SetMultColour(1, 1, 1, 1)
	target:DoTaskInTime(0.5, function(inst)
		SpawnPrefab("pp_mindcontrol_aoe").Transform:SetPosition(target:GetPosition():Get())
		
		target.SoundEmitter:PlaySound("dontstarve/creatures/together/stalker/death3/orchhits")
	
		if target.components.combat.target then
			target.components.combat.target = nil
		end
	
		target.components.combat:SetRetargetFunction(2, target.components.combat.targetfn_old)
		target.components.combat.targetfn_old = nil
	
		target.AnimState:SetMultColour(1, 1, 1, 1)
	end)
	
	inst:Remove()
end

local function debufffn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
		return inst
	end
	
	inst:AddComponent("timer")
	
	inst:ListenForEvent("timerdone", OnTimerDone)
		
	inst:AddComponent("debuff")
	inst.components.debuff:SetAttachedFn(OnAttached)
	inst.components.debuff:SetDetachedFn(OnDetached)
	inst.components.debuff:SetExtendedFn(OnExtended)
	
	
	
	return inst
end

local function ShieldOverride()
	return "idle"..math.random(1, 4)
end

local function FxOverride()
	return "shad"..math.random(1, 3)
end

local function MakeFx(name, bank, build, anim, scale, animloop, isgrounded, animoverride, colour)
	local function fn()
		local inst = CreateEntity()
		local trans = inst.entity:AddTransform()
		inst.entity:AddNetwork()
		inst.entity:AddAnimState()
		inst.entity:AddSoundEmitter()
		
		inst.AnimState:SetBank(bank)
		inst.AnimState:SetBuild(build)
		inst.AnimState:PlayAnimation(animoverride and animoverride() or anim, animloop and animloop or false)
		
		if scale then
			inst.AnimState:SetScale(scale, scale)
		end
		
		if colour then
			inst.AnimState:SetMultColour(colour[1], colour[2], colour[3], colour[4])
		end
		
		if isgrounded then
			inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
		end
		
		inst:AddTag("FX")
		
		inst.entity:SetPristine()
		
		if not TheWorld.ismastersim then
            return inst
        end

        inst.persists = false
		inst:ListenForEvent("animover", inst.Remove)
        --inst:DoTaskInTime(2, inst.Remove)
		
		return inst	
	end
	return Prefab(name, fn, assets, prefabs)
end

return Prefab("pp_mindcontrol", debufffn, assets, prefabs),
	MakeFx("pp_mindcontrol_aoe", "stalker_shield", "stalker_shield", "idle1", nil, nil, nil, ShieldOverride),
	MakeFx("pp_mindcontrol_aoe2", "bearger_ring_fx", "bearger_ring_fx", "idle", 1, nil, true, nil, {0, 0, 0, 1}),
	MakeFx("pp_mindcontrol_fx", "cane_shadow_fx", "cane_shadow_fx", "shad1", 2, nil, nil, FxOverride, {0, 0, 0, 1})