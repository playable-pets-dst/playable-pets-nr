local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------


local assets = 
{
	Asset("ANIM", "anim/bee_guard_shiny_build_01.zip"),
	Asset("ANIM", "anim/bee_guard_puffy_shiny_build_01.zip"),
}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 200,
	hunger = 100,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --I think .25 is defualt.
	sanity = 100,
	runspeed = TUNING.BEEGUARD_SPEED,
	runspeed2 = TUNING.BEEGUARD_DASH_SPEED,
	walkspeed = TUNING.BEEGUARD_SPEED,
	damage = 30*2,
	damage_poofy = 50*2,
	range = 2,
	hit_range = 3,
	bank = "bee_guard",
	build = "bee_guard_build",
	build2 = "bee_guard_puffy_build",
	scale = 1.4,
	shiny = "bee_guard",
	stategraph = "SGbeeguardp",
	minimap = "beeguardp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('beeguardp',
-----Prefab---------------------Chance------------
{
    --{'stinger',             1.00},  
   
})

local workersounds =
{
    takeoff = "dontstarve/bee/bee_takeoff",
	attack = "dontstarve/bee/killerbee_attack",
    buzz = "dontstarve/bee/bee_fly_LP",
    hit = "dontstarve/creatures/together/bee_queen/beeguard/hurt",
    death = "dontstarve/creatures/together/bee_queen/beeguard/death",
}

local normalsounds =
{
	takeoff = "dontstarve/bee/bee_takeoff",
    attack = "dontstarve/bee/killerbee_attack",
    buzz = "dontstarve/bee/bee_fly_LP",
    hit = "dontstarve/creatures/together/bee_queen/beeguard/hurt",
    death = "dontstarve/creatures/together/bee_queen/beeguard/death",
}

local poofysounds =
{
	takeoff = "dontstarve/bee/bee_takeoff",
    attack = "dontstarve/bee/killerbee_attack",
    buzz = "dontstarve/bee/killerbee_fly_LP",
    hit = "dontstarve/creatures/together/bee_queen/beeguard/hurt",
    death = "dontstarve/creatures/together/bee_queen/beeguard/death",
}

local function MakeNormal(inst)
	inst.is_poofy = false 
	inst.components.combat:SetDefaultDamage(mob.damage)
	inst.components.locomotor.runspeed = mob.runspeed
	inst.components.ppskin_manager:LoadSkin(mob, true)
	inst.sounds = normalsounds
	if inst.SoundEmitter:PlayingSound("buzz") then
          inst.SoundEmitter:KillSound("buzz")
          inst.SoundEmitter:PlaySound(inst.sounds.buzz, "buzz")
    end
	inst.sg:GoToState("idle") --knock them out of run states.
end

local function MakePoofy(inst)
	inst.is_poofy = true
	inst.components.combat:SetDefaultDamage(mob.damage_poofy)
	inst.components.locomotor.runspeed = mob.runspeed2
	inst.components.ppskin_manager:LoadSkin(mob, true)
	inst.sounds = poofysounds
	if inst.SoundEmitter:PlayingSound("buzz") then
          inst.SoundEmitter:KillSound("buzz")
          inst.SoundEmitter:PlaySound(inst.sounds.buzz, "buzz")
    end
	SpawnPrefab("bee_poof_small").Transform:SetPosition(inst.Transform:GetWorldPosition())
end

local function onattacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, 30, function(dude) return dude:HasTag("bee") and not dude.components.health:IsDead() end, 30)
end

local function onhitother2(inst, other)
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("bee") and not dude.components.health:IsDead() end, 30)
end

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	
end

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end

local function SetSkinDefault(inst, data)
	if data then
		if inst.is_poofy then
			inst.AnimState:SetBuild(data.puffy_build)
		else	
			inst.AnimState:SetBuild(data.build)			
		end
	else
		inst.AnimState:SetBuild(inst.is_poofy and mob.build2 or mob.build)
	end	
end

local master_postinit = function(inst)  
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 3, 3) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:AddChanceLoot("stinger", 0.5)
	----------------------------------
	--Tags--
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	inst:AddTag("bee")
    inst:AddTag("flying")
	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.is_poofy = false
	
	inst.MakeNormal = MakeNormal
	inst.MakePoofy = MakePoofy
	inst.mobplayer = true
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	inst.sounds = normalsounds
	inst.SoundEmitter:PlaySound(inst.sounds.buzz, "buzz")
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,2,2) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	inst.DynamicShadow:SetSize(1.2, .75)
    MakeFlyingCharacterPhysics(inst, 1.5, .75)
	PlayablePets.SetAmphibious(inst, nil, nil, true, nil, true)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip)
	inst:ListenForEvent("attacked", onattacked)
	inst.components.combat.onhitotherfn = onhitother2
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
   ------------------------------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true) 
			inst.SoundEmitter:PlaySound(inst.sounds.buzz, "buzz")
		end)
    end)
	
    return inst	
end
return MakePlayerCharacter("beeguardp", prefabs, assets, common_postinit, master_postinit, start_inv)
