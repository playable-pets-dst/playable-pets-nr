local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "deer_redp"

local assets = 
{
	Asset("ANIM", "anim/deer_shiny_build_01.zip"),
    Asset("ANIM", "anim/deer_shadow.zip"),
}

local prefabs = {
}

local start_inv = {
}

local start_inv2 = 
{

}



if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 800,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 150,
	runspeed = TUNING.DEER_RUN_SPEED,
	walkspeed = TUNING.DEER_WALK_SPEED,
	damage = 60,
	hit_range = TUNING.DEER_ATTACK_RANGE,
	range = TUNING.DEER_ATTACK_RANGE + 20,
	bank = "deer",
	build = "deer_build",
	scale = 1,
	stategraph = "SGdeerp",
	minimap = "deer_redp.tex",
	
}

--Loot that drops when you die, duh.
SetSharedLootTable('deerp',
-----Prefab---------------------Chance------------
{
    {'meat',			1.00},
	{'meat',			0.50},
})

local FORGE_STATS = PPNR_FORGE[string.upper(prefabname)] --DEER
--==============================================
--					Mob Functions
--==============================================
local SPELL_MAX_TARGETS = 20
local function FindCastTargets(inst, target)
    if inst.components.combat.target ~= nil then
        --Single target for deer without keeper
        return target.components.health ~= nil
            and not (target.components.health:IsDead() or
                    target:HasTag("playerghost") or
                    target:HasTag("deergemresistance"))
            and target:IsNear(inst, TUNING.DEER_GEMMED_CAST_RANGE)
            and NoSpellOverlap(target.Transform:GetWorldPosition())
            and { target }
            or nil
    end
    --Multi-target when receiving commands from keeper
    local x, y, z = inst.Transform:GetWorldPosition()
    local targets = {}
    local priorityindex = 1
    for i, v in ipairs(TheSim:FindEntities(x, y, z, TUNING.DEER_GEMMED_CAST_RANGE, { "_combat", "_health" }, { "INLIMBO", "playerghost", "deergemresistance" })) do
        if not v.components.health:IsDead() then
            if v:HasTag("player") then
                table.insert(targets, priorityindex, v)
                if #targets >= SPELL_MAX_TARGETS then
                    return targets
                end
                priorityindex = priorityindex + 1
            elseif v.components.combat.target ~= nil and v.components.combat.target:HasTag("deergemresistance") then
                table.insert(targets, v)
                if #targets >= SPELL_MAX_TARGETS then
                    return targets
                end
            end
        end
    end
    return #targets > 0 and targets or nil
end

local function SpawnSpell(inst, target)
	local tar = inst.components.combat.target or nil
	local spell = SpawnPrefab(inst.castfx)
	if tar ~= nil then
		local x, y, z = tar.Transform:GetWorldPosition() 
		spell.Transform:SetPosition(x, 0, z)
	else
		local x, y, z = inst.Transform:GetWorldPosition() 
		spell.Transform:SetPosition(x, 0, z)
	end	
    spell:DoTaskInTime(inst.castduration, spell.KillFX)
    return spell
end

local function SpawnSpells(inst, target)
    local spells = {}
    local nextpass = {}
	
    for i, v in ipairs(inst.components.combat.target) do
        if v and v:IsNear(inst, TUNING.DEER_GEMMED_CAST_MAX_RANGE) then
            local x, y, z = v.Transform:GetWorldPosition()
            if NoSpellOverlap(x, 0, z, SPELL_OVERLAP_MAX) then
                table.insert(spells, SpawnSpell(inst, x, z))
                if #spells >= TUNING.DEER_GEMMED_MAX_SPELLS then
                    return spells
                end
            else
                table.insert(nextpass, { x = x, z = z })
            end
        end
    end
    if #nextpass <= 0 then
        return spells
    end
    for range = SPELL_OVERLAP_MAX - 1, SPELL_OVERLAP_MIN, -1 do
        local i = 1
        while i <= #nextpass do
            local v = nextpass[i]
            if NoSpellOverlap(v.x, 0, v.z, range) then
                table.insert(spells, SpawnSpell(inst, v.x, v.z))
                if #spells >= TUNING.DEER_GEMMED_MAX_SPELLS or #nextpass <= 1 then
                    return spells
                end
                table.remove(nextpass, i)
            else
                i = i + 1
            end
        end
    end
    return #spells > 0 and spells or nil
end

local function DoCast(inst, targets)
    local spells = targets ~= nil and SpawnSpells(inst, targets) or nil
   -- inst.components.timer:StopTimer("deercast_cd")
    --inst.components.timer:StartTimer("deercast_cd", spells ~= nil and inst.castcd or TUNING.DEER_GEMMED_FIRST_CAST_CD)
    return spells
end

local function OnNewTarget(inst, data)
    if data.target ~= nil then
        inst:SetEngaged(true)
    end
end

local function IsDeadKeeper(keeper)
    return (keeper.IsUnchained == nil or keeper:IsUnchained())
        and keeper.components.health ~= nil
        and keeper.components.health:IsDead()
end

local function GemmedRetargetFn(inst)
    local keeper = inst.components.entitytracker:GetEntity("keeper")
    return keeper ~= nil
        and not IsDeadKeeper(keeper)
        and keeper.components.combat ~= nil
        and keeper.components.combat.target
        or nil
end

local function GemmedOnAttacked(inst, data)
    local keeper = inst.components.entitytracker:GetEntity("keeper")
    if keeper == nil or not IsDeadKeeper(keeper) then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 12, ShareTargetFn, 3)
    end
end

local function SetEngaged(inst, engaged)
    --NOTE: inst.engaged is nil at instantiation, and engaged must not be nil
    if inst.engaged ~= engaged then
        inst.engaged = engaged
        inst.components.timer:StopTimer("deercast_cd")
        if engaged then
            inst.components.timer:StartTimer("deercast_cd", TUNING.DEER_GEMMED_FIRST_CAST_CD)
            inst:RemoveEventCallback("newcombattarget", OnNewTarget)
        else
            inst:ListenForEvent("newcombattarget", OnNewTarget)
        end
    end
end

local function OnGotCommander(inst, data)
    local keeper = inst.components.entitytracker:GetEntity("keeper")
    if keeper ~= data.commander then
        inst.components.entitytracker:ForgetEntity("keeper")
        inst.components.entitytracker:TrackEntity("keeper", data.commander)

        inst.components.knownlocations:RememberLocation("keeperoffset", inst:GetPosition() - data.commander:GetPosition(), false)
    end
end

local function OnLostCommander(inst, data)
    local keeper = inst.components.entitytracker:GetEntity("keeper")
    if keeper == data.commander then
        inst.components.entitytracker:ForgetEntity("keeper")
        inst.components.knownlocations:ForgetLocation("keeperoffset")
    end
end

local function GemmedOnLoadPostPass(inst)
    local keeper = inst.components.entitytracker:GetEntity("keeper")
    if keeper ~= nil then
        if keeper.components.commander ~= nil then
            keeper.components.commander:AddSoldier(inst)
        end
    end
end

local function OnUpdateOffset(inst, offset)
    inst.components.knownlocations:RememberLocation("keeperoffset", offset)
end

--------------------------------------------------------------------------

local function DoNothing()
end

local function DoChainIdleSound(inst, volume)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/chain_idle", nil, volume)
end

local function DoBellIdleSound(inst, volume)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/bell_idle", nil, volume)
end

local function DoChainSound(inst, volume)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/chain", nil, volume)
end

local function DoBellSound(inst, volume)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/bell", nil, volume)
end

local function SetupSounds(inst)
    if inst.gem == nil then
        inst.DoChainSound = DoNothing
        inst.DoChainIdleSound = DoNothing
        inst.DoBellSound = DoNothing
        inst.DoBellIdleSound = DoNothing
    elseif IsSpecialEventActive(SPECIAL_EVENTS.WINTERS_FEAST) then
        inst.DoChainSound = DoChainSound
        inst.DoChainIdleSound = DoChainIdleSound
        inst.DoBellSound = DoBellSound
        inst.DoBellIdleSound = DoBellIdleSound
    else
        inst.DoChainSound = DoChainSound
        inst.DoChainIdleSound = DoChainIdleSound
        inst.DoBellSound = DoNothing
        inst.DoBellIdleSound = DoNothing
    end
end
--==============================================
--				Custom Common Functions
--==============================================	

----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end

local function SetSkinDefault(inst, data)
	if data then
		inst.AnimState:SetBuild(data.build)
		inst.AnimState:OverrideSymbol("swap_antler_red", data.build, "swap_antler_red")
        inst.AnimState:OverrideSymbol("swap_neck_collar", data.build, "swap_neck_collar")
	else
		inst.AnimState:SetBuild(mob.build)
		if IsSpecialEventActive(SPECIAL_EVENTS.WINTERS_FEAST) then
			inst.AnimState:OverrideSymbol("deer_hair", mob.build, "deer_hair_winter")
			inst.AnimState:OverrideSymbol("swap_neck_collar", mob.build, "swap_neck_collar_winter")
			inst.AnimState:OverrideSymbol("klaus_deer_chain", mob.build, "klaus_deer_chain_winter")
			inst.AnimState:OverrideSymbol("deer_chest", mob.build, "deer_chest_winter")
		end
		inst.AnimState:OverrideSymbol("swap_antler_red", mob.build, "swap_antler_red")
	end	
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("deer")
	inst:AddTag("deergemresistance")
	inst:AddTag("deer_blue")
    inst:AddTag("animal")
    ----------------------------------
    inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 1, 1) --fire, acid, poison
	inst.components.combat:SetHurtSound("dontstarve/creatures/together/deer/hit")
	----------------------------------
	--Variables		
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = false
	inst.canmagic = true --allows to cast spells.
	inst.hasantler = true
	inst.mobsleep = true
	inst.ghostbuild = "ghost_monster_build"
	inst.gem = "red"
	inst.antlertype = 1
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	local body_symbol = "deer_torso"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.SpawnSpell = SpawnSpell
	 if inst.gem ~= nil then
        if inst.gem ~= "red" then
            inst.AnimState:OverrideSymbol("swap_antler_red", "deer_build", "swap_antler_"..inst.gem)
        end
        if IsSpecialEventActive(SPECIAL_EVENTS.WINTERS_FEAST) and inst.isshiny ~= 0 then
			inst.AnimState:OverrideSymbol("deer_hair", "deer_shiny_build_0"..inst.isshiny, "deer_hair_winter")
            inst.AnimState:OverrideSymbol("swap_neck_collar", "deer_shiny_build_0"..inst.isshiny, "swap_neck_collar_winter")
            inst.AnimState:OverrideSymbol("klaus_deer_chain", "deer_shiny_build_0"..inst.isshiny, "klaus_deer_chain_winter")
            inst.AnimState:OverrideSymbol("deer_chest", "deer_shiny_build_0"..inst.isshiny, "deer_chest_winter")
		elseif IsSpecialEventActive(SPECIAL_EVENTS.WINTERS_FEAST) and inst.isshiny == 0 then
            inst.AnimState:OverrideSymbol("deer_hair", "deer_build", "deer_hair_winter")
            inst.AnimState:OverrideSymbol("swap_neck_collar", "deer_build", "swap_neck_collar_winter")
            inst.AnimState:OverrideSymbol("klaus_deer_chain", "deer_build", "klaus_deer_chain_winter")
            inst.AnimState:OverrideSymbol("deer_chest", "deer_build", "deer_chest_winter")
        end
        inst:AddTag("deergemresistance")
        --inst:SetPrefabNameOverride("deer_gemmed")
    else
        inst.AnimState:Hide("swap_antler")
        inst.AnimState:Hide("CHAIN")
        inst.AnimState:OverrideSymbol("swap_neck_collar", "deer_build", "swap_neck")
    end
	----------------------------------
	--Gem Related--
	 if inst.gem ~= "red" then
        MakeMediumBurnableCharacter(inst, "deer_torso")
        inst.components.burnable:SetBurnTime(TUNING.DEER_ICE_BURN_PANIC_TIME)
    end
    if inst.gem ~= "blue" then
        MakeMediumFreezableCharacter(inst, "deer_torso")
        inst.components.freezable:SetResistance(1)
        inst.components.freezable:SetDefaultWearOffTime(TUNING.DEER_FIRE_FREEZE_WEAR_OFF_TIME)
        --NOTE: no diminishing returns
    end
	
	
    if inst.gem ~= nil then

        if inst.gem == "red" then
            inst.castfx = "deer_fire_circle"
            inst.castduration = 4
            inst.castcd = TUNING.DEER_FIRE_CAST_CD
        else
            inst.castfx = "deer_ice_circlep"
            inst.castduration = 6
            inst.castcd = TUNING.DEER_ICE_CAST_CD
        end

        --inst.SetEngaged = SetEngaged
        inst.FindCastTargets = FindCastTargets
        inst.DoCast = DoCast
        --inst.OnLoadPostPass = GemmedOnLoadPostPass
    end
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.ROUGHAGE, FOODTYPE.VEGGIE }, { FOODTYPE.ROUGHAGE, FOODTYPE.VEGGIE}) 
    inst.components.eater:SetAbsorptionModifiers(1,2,2) --This might multiply food stats.
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 100, .5)
    inst.DynamicShadow:SetSize(1.75, .75)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	SetupSounds(inst)
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)