local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local MOB_HEALTH = BOSS_STATS and {1000, 4000, 10000} or {250, 750, 5750}
local MOB_HUNGER = {150, 200, 250}
local MOB_DAMAGE = BOSS_STATS and {45 * 2, 100 * 2, 165 * 2} or {50 * 2, 80 * 2, 100 * 2}
local MOB_ATTACKPERIOD = BOSS_STATS and {6, 5.5, 5} or {4, 4, 4}
local MOB_RANGE = {4, 6, 8} --BOSS_STATS and {4, 6, 8}  or {4, 6, 8}
local MOB_HIT_RANGE = {4, 5, 6}

--kills
local LVL2_REQ = 40
local LVL3_REQ = 175

local prefabname = "shadow_rookp"

local assets = 
{
	
}



local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = MOB_HEALTH[1], --unused, used table instead.
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 300,
	runspeed = 7,
	walkspeed = 7,
	damage = MOB_DAMAGE[1],
	attackperiod = MOB_ATTACKPERIOD[1],
	range = MOB_RANGE[1],
	hit_range = MOB_HIT_RANGE[1],
	bank = "shadow_rook",
	build = "shadow_rook", --this or shadow_rook_upg_build
	shiny = "shadow_rook",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGshadow_rookp",
	minimap = "shadow_rookp.tex",
}
-----------------------
--Loot that drops when you die, duh.
local function lootsetfn(lootdropper) --very neat stuff here.
    local loot = {}

    if lootdropper.inst.level >= 2 then
        for i = 1, math.random(2, 3) do
            table.insert(loot, "nightmarefuel")
        end

        if lootdropper.inst.level >= 3 then
            table.insert(loot, "shadowheart") 
            table.insert(loot, "nightmarefuel")
            --TODO: replace with shadow equipment drops
            table.insert(loot, "armor_sanity")
            table.insert(loot, "nightsword")
        end
    end

    lootdropper:SetLoot(loot)
end

local sounds =
{
    attack = "dontstarve/sanity/rook/attack_1",
    attack_grunt = "dontstarve/sanity/rook/attack_grunt",
    death = "dontstarve/sanity/rook/die",
    idle = "dontstarve/sanity/rook/idle",
    taunt = "dontstarve/sanity/rook/taunt",
    appear = "dontstarve/sanity/creature1/appear",
    disappear = "dontstarve/sanity/rook/dissappear",
    levelup = "dontstarve/sanity/creature1/taunt",
    hit = "dontstarve/sanity/rook/hit_response",
	teleport = "dontstarve/sanity/rook/teleport",
}

local FORGE_STATS = PPNR_FORGE[string.upper(prefabname)]
--==============================================
--					Mob Functions
--==============================================
----Level Up----


local MAX_LEVEL = 3

local function IsMaxLevel(inst)
    return inst.level == MAX_LEVEL
end

local function commonlevelup(inst, overridelevel)
    if inst.components.health:IsDead() then
        return
    end
    local level = overridelevel
    --if level ~= inst.level then
        inst.level = level

        local scale = TUNING.SHADOW_ROOK.LEVELUP_SCALE[level]

        local x, y, z = inst.Transform:GetWorldPosition()
        inst.Transform:SetScale(scale, scale, scale)
        --inst.Physics:SetCapsule(PHYS_RADIUS["shadow_rook"] * scale, 1)
        inst.Physics:Teleport(x, y, z)

        inst.AnimState:SetMultColour(1, 1, 1, 0.5 + (0.12*(level-1)))

        inst.components.health:SetMaxHealth(MOB_HEALTH[level])
		inst.components.hunger:SetMax(MOB_HUNGER[level])
        
        if level > 1 then
            inst:AddTag("epic")
        else
            inst:RemoveTag("epic")
        end

        return level, scale
   -- end
end

local function levelup(inst, overridelevel)
    local level, scale = commonlevelup(inst, overridelevel)
    if level ~= nil then
        inst.components.combat:SetDefaultDamage(MOB_DAMAGE[level])
        inst.components.combat:SetRange(MOB_RANGE[level], MOB_HIT_RANGE[level])
        inst.components.combat:SetAttackPeriod(MOB_ATTACKPERIOD[level])

        if level > 1 then
            local suffix = tostring(level - 1)
            inst.AnimState:OverrideSymbol("base",           "shadow_rook_upg_build", "base"..suffix)
            inst.AnimState:OverrideSymbol("big_horn",       "shadow_rook_upg_build", "big_horn"..suffix)
            inst.AnimState:OverrideSymbol("bottom_head",    "shadow_rook_upg_build", "bottom_head"..suffix)
            inst.AnimState:OverrideSymbol("small_horn_lft", "shadow_rook_upg_build", "small_horn_lft"..suffix)
            inst.AnimState:OverrideSymbol("small_horn_rgt", "shadow_rook_upg_build", "small_horn_rgt"..suffix)
            inst.AnimState:OverrideSymbol("top_head",       "shadow_rook_upg_build", "top_head"..suffix)
        else
            inst.AnimState:ClearAllOverrideSymbols()
        end
    end
end


local function ForceLevelUp(inst, level)

end

local function OnKill(inst, data) 
	if data.victim and not data.victim:HasTag("insect") 
	and not data.victim:HasTag("smallcreature") and not data.victim:HasTag("bird")
	and not data.victim:HasTag("soulless") then
		inst.components.health:DoDelta((MOB_HEALTH[inst.level] * 0.0125), false)
		if inst.killcount ~= nil and inst.killcount <= 255 and MOBGROW== "Enable" then --set a maximum value to prevent overflows/rollbacks, also keep under 255.
			inst.killcount = inst.killcount + 1
			if inst.killcount >= LVL2_REQ and inst.killcount < LVL3_REQ and inst.level < 2 then
				inst.sg:GoToState("levelup")
			elseif inst.killcount >= LVL3_REQ and inst.level < 3 then
				inst.sg:GoToState("levelup")
			end	
		end	
	end
end
--==============================================
--				Custom Common Functions
--==============================================	
local function OnDeath(inst)
	inst.killcount = 0
	inst.level = 1
end

----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end
--==============================================
--					Loading/Saving
--==============================================
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.killcount = data.killcount or 0
		inst.level = data.level or 1
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.killcount = inst.killcount or 0
	if inst.level > 1 then
        data.level = inst.level
    end
    
    if next(inst.levelupsource) ~= nil then      -- dont write if the table is empty
		data.levelupsource = inst.levelupsource
    end
end

local function OnPreload(inst, data)
	inst.levelupsource = {}
	
    if data ~= nil then
		inst.levelupsource = data.levelupsource or {}
		
		if data.level ~= nil then
			inst:LevelUp(data.level)
		end
    end
end
--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("monster")
    inst:AddTag("flying")
    inst:AddTag("shadowchesspiece")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
	inst:WatchWorldState("issummer", function() PlayablePets.SetSandstormImmunity(inst) end) --makes immune to Sandstorm visual issues.
	
	PlayablePets.SetSandstormImmunity(inst)
	
end

local master_postinit = function(inst)  
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0, 9999) --fire, acid, poison
	inst.components.health:StartRegen(10, 25)
	----------------------------------
	--Variables		
	inst.poisonimmune = true
	inst.debuffimmune = true
	inst.acidimmune = true
	inst.inkimmune = true
	inst.taunt = true
	inst.killcount = 0
	inst.ghostbuild = "ghost_monster_build"
	
	--local body_symbol = "swap_fire"
	--inst.poisonsymbol = body_symbol
	--MakeLargeBurnableCharacter(inst, body_symbol)
    --MakeLargeFreezableCharacter(inst)
	--inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.level = 1
   	inst.levelupsource = {}
    inst.sounds = sounds
	inst.LevelUp = levelup
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	inst:AddComponent("lootdropper")
	inst.components.lootdropper:SetLootSetupFn(lootsetfn)
	
	inst:AddComponent("epicscare")
    inst.components.epicscare:SetRange(TUNING.SHADOW_CHESSPIECE_EPICSCARE_RANGE)	
	----------------------------------
	--Eater--
	inst.components.eater:SetAbsorptionModifiers(2,2,2) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 10, .5)
	RemovePhysicsColliders(inst)
	PlayablePets.SetAmphibious(inst, nil, nil, true, nil, true)
	inst.Physics:CollidesWith(COLLISION.SANITY)
	--MakeGhostPhysics(inst, 1, .5) --Special physics for flying characters(?)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
    inst.DynamicShadow:SetSize(1.5, .5)
	inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("killed", OnKill)
	inst:ListenForEvent("death", OnDeath)
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) levelup(inst, inst.level) end)
	inst:DoTaskInTime(3, function(inst) 
		PlayablePets.SetSkin(inst, mob) 
		inst.AnimState:SetMultColour(1, 1, 1, .5)
		inst.AnimState:SetFinalOffset(1)
	end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) levelup(inst, inst.level) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) levelup(inst, inst.level) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	inst.OnPreload = OnPreload
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
