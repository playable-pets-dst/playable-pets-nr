local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "klausp"

local assets = 
{
	Asset("ANIM", "anim/klaus_shiny_build_01.zip"),
	Asset("ANIM", "anim/klaus_shiny_build_03.zip"),
}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = BOSS_STATS and TUNING.KLAUS_HEALTH or 5000,
	hunger = 350,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 150,
	runspeed = TUNING.KLAUS_SPEED,
	walkspeed = TUNING.KLAUS_SPEED,
	damage = (TUNING.KLAUS_DAMAGE * 1.2),
	attackperiod = TUNING.KLAUS_ATTACK_PERIOD,
	range = 20,
	hit_range = TUNING.KLAUS_HIT_RANGE * 1.4,
	bank = "klaus",
	build = "klaus_build",
	shiny = "klaus",
	scale = 1.68, --we will start off enraged
	--build2 = "alternate build here",
	stategraph = "SGklausp",
	minimap = "klausp.tex",
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('klausp',
-----Prefab---------------------Chance------------
{
	{'monstermeat',				1.00},
	{'monstermeat',				1.00},
	{'monstermeat',				1.00},
	{'monstermeat',				1.00},
	{'monstermeat',				1.00},
	{'monstermeat',				1.00},
})

local FORGE_STATS = PPNR_FORGE[string.upper(prefabname)] --KLAUS
--==============================================
--					Mob Functions
--==============================================
local function SetSkinDefault(inst, data)
	if data and data.build then
		local build = data.build
		if IsSpecialEventActive(SPECIAL_EVENTS.WINTERS_FEAST) then
			inst.AnimState:OverrideSymbol("swap_chain", build, "swap_chain_winter")
			inst.AnimState:OverrideSymbol("swap_chain_link", build, "swap_chain_link_winter")
			inst.AnimState:OverrideSymbol("swap_chain_lock", build, "swap_chain_lock_winter")
			inst.AnimState:OverrideSymbol("swap_klaus_antler", build, "swap_klaus_antler_winter")
		end
		inst.AnimState:SetBuild(build)
	else
		inst.AnimState:SetBuild(mob.build)
	end
end

local PHASE2_HEALTH = .5

local function DoNothing()
end

local function DoFoleySounds(inst, volume)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/chain_foley", nil, volume)
end

local function IsUnchained(inst)
    return inst._unchained
end

local function Unchain(inst)
    if not inst._unchained then
        inst.AnimState:Hide("swap_chain")
        inst.AnimState:Hide("swap_chain_lock")
        --inst.components.sanityaura.aura = inst.enraged and -TUNING.SANITYAURA_HUGE or -TUNING.SANITYAURA_LARGE
        inst.DoFoleySounds = DoNothing
        inst._unchained = true
		inst.components.combat:SetRange(30, TUNING.KLAUS_HIT_RANGE * 1.4)
    end
end

local function Enrage(inst)
    if not inst.enraged then
        inst.enraged = true
        inst.nohelpers = nil --redundant when enraged
        inst.Physics:Stop()
        inst.Physics:Teleport(inst.Transform:GetWorldPosition())
        SetPhysicalScale(inst, TUNING.KLAUS_ENRAGE_SCALE)
        SetStatScale(inst, TUNING.KLAUS_ENRAGE_SCALE)
        --inst.components.sanityaura.aura = inst:IsUnchained() and -TUNING.SANITYAURA_HUGE or -TUNING.SANITYAURA_LARGE
    end
end

--

local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function OnDestroyOther(inst, other)
    if other:IsValid() and (other:HasTag("tree") or other:HasTag("boulder")) and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.DIG and
        other.components.workable.action ~= ACTIONS.NET and
        not inst.recentlycharged[other] then
        SpawnPrefab("collapse_small").Transform:SetPosition(other.Transform:GetWorldPosition())
        --if other.components.lootdropper ~= nil and (other:HasTag("tree") or other:HasTag("boulder")) then
            --other.components.lootdropper:SetLoot({})
        --end
        other.components.workable:Destroy(inst)
		if other:IsValid() and not (other:HasTag("tree") or other:HasTag("boulder")) then
		print("LOGGED: Klaus player destoyed "..other.prefab) --useful for admins to indentify greifing, find out how to print names instead.
		end
        if other:IsValid() and other.components.workable ~= nil and other.components.workable:CanBeWorked() then
            inst.recentlycharged[other] = true
			print("LOGGED: Klaus player attempted destruction on "..other.prefab)
            inst:DoTaskInTime(3, ClearRecentlyCharged, other)
        end
    end
end

local function OnCollide(inst, other)
    if other ~= nil and (other:HasTag("tree") or other:HasTag("boulder")) and
        other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.DIG and
        other.components.workable.action ~= ACTIONS.NET and
        not inst.recentlycharged[other] then
        inst:DoTaskInTime(2 * FRAMES, OnDestroyOther, other)
    end
end
--==============================================
--				Custom Common Functions
--==============================================	

----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.unchained = inst:IsUnchained() or nil
    data.enraged = inst.enraged or nil	
end

local function OnPreload(inst, data)
    if data ~= nil then
        if data.unchained then
            Unchain(inst)
        end
    end
end

local function EnterPhase2Trigger(inst)
    if not (inst:IsUnchained() or inst.components.health:IsDead()) then
        inst:PushEvent("transition")
    end
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("monster")
    inst:AddTag("largecreature")
	inst:AddTag("epic")
    inst:AddTag("deergemresistance")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local master_postinit = function(inst)  
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	----------------------------------
	--Variables		
	inst.altattack2 = true
	inst.enraged = true
	inst.taunt = true
	inst.mobsleep = true
	inst.taunt2 = true
	inst.canchomp = true
	inst.magicattack = "fire"
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	inst.ghostbuild = "ghost_monster_build"
	
	inst.hit_recovery = TUNING.KLAUS_HIT_RECOVERY * 1.68
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetCommonStatResistances(inst, 1, 1, 1, 4) --fire, acid, poison
	inst.components.freezable.diminishingreturns = true
	
	inst.DoFoleySounds = DoFoleySounds
	inst.IsUnchained = IsUnchained
    inst.Unchain = Unchain
    inst.Enrage = Enrage
	inst._unchained = false--net_bool(inst.GUID, "klaus._unchained", "musicdirty") --delete this if unused
	
	if IsSpecialEventActive(SPECIAL_EVENTS.WINTERS_FEAST) and inst.isshiny == 0 then
        inst.AnimState:OverrideSymbol("swap_chain", "klaus_build", "swap_chain_winter")
        inst.AnimState:OverrideSymbol("swap_chain_link", "klaus_build", "swap_chain_link_winter")
        inst.AnimState:OverrideSymbol("swap_chain_lock", "klaus_build", "swap_chain_lock_winter")
        inst.AnimState:OverrideSymbol("swap_klaus_antler", "klaus_build", "swap_klaus_antler_winter")
	elseif IsSpecialEventActive(SPECIAL_EVENTS.WINTERS_FEAST) and inst.isshiny ~= 0 then
		inst.AnimState:OverrideSymbol("swap_chain", "klaus_shiny_build_0"..inst.isshiny, "swap_chain_winter")
        inst.AnimState:OverrideSymbol("swap_chain_link", "klaus_shiny_build_0"..inst.isshiny, "swap_chain_link_winter")
        inst.AnimState:OverrideSymbol("swap_chain_lock", "klaus_shiny_build_0"..inst.isshiny, "swap_chain_lock_winter")
        inst.AnimState:OverrideSymbol("swap_klaus_antler", "klaus_shiny_build_0"..inst.isshiny, "swap_klaus_antler_winter")
    end
	
	inst.recentlycharged = {}
    inst.Physics:SetCollisionCallback(OnCollide)
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	--TODO make global sanityaura function
	--inst:AddComponent("sanityaura")
	--inst.components.sanityaura.aura = -TUNING.SANITYAURA_LARGE
	
	inst:AddComponent("epicscare")
    inst.components.epicscare:SetRange(TUNING.KLAUS_EPICSCARE_RANGE)
	
	inst:AddComponent("healthtrigger")
    inst.components.healthtrigger:AddTrigger(0.5, EnterPhase2Trigger) --I don't think this gets used for anything atm, could later tho.
	----------------------------------
	--Eater--
	inst.components.eater:SetAbsorptionModifiers(2,2,2) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 100, 1.2)
    inst.DynamicShadow:SetSize(3.5 * 1.6, 1.5 * 1.6)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	inst.OnPreload = OnPreload
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
