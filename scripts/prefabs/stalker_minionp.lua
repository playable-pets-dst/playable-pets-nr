local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "stalker_minionp"

local assets = 
{
	Asset("ANIM", "anim/stalker_minion_shiny_build_01.zip"),
	Asset("ANIM", "anim/stalker_minion_2_shiny_build_01.zip"),
}



local prefabs = 
{	

}
	
local projectile_assets =
{
    Asset("ANIM", "anim/spat_bomb.zip"),
}

local projectile_prefabs =
{
    "spat_splat_fx",
    "spat_splash_fx_full",
    "spat_splash_fx_med",
    "spat_splash_fx_low",
    "spat_splash_fx_melted",
}	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 20,
	hunger = 50,
	hungerrate = TUNING.WILSON_HUNGER_RATE/5,
	sanity = 50,
	runspeed = 3,
	walkspeed = 3,
	damage = 20,
	range = 6,
	bank = "stalker_minion",
	build = "stalker_minion",
	bank2 = "stalker_minion_2",
	build2 = "stalker_minion_2",
	shiny = "stalker_minion",
	scale = 1,
	stategraph = "SGstalker_minionp",
	minimap = "stalker_minionp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('stalker_minionp',
{
    {"nightmarefuel",       0.50},
})


local FORGE_STATS = PPNR_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================
local function nostalkerordebrisdmg(inst, amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb)
    return afflicter ~= nil and (afflicter:HasTag("stalker") or afflicter:HasTag("quakedebris"))
end
--==============================================
--				Custom Common Functions
--==============================================	
local function SetAlt(inst)
	if inst.isalt then
		inst.AnimState:SetBank(mob.bank2)
		inst.movestarttime = 16 * FRAMES
		inst.components.locomotor.walkspeed = 1.5
	else
		inst.AnimState:SetBank(mob.bank)	
		inst.movestoptime = 6 * FRAMES
		inst.components.locomotor.walkspeed = mob.walkspeed
	end
end

local function SetSkinDefault(inst, data)
	if data then
		if data.alt then
			inst.isalt = true
		else
			inst.isalt = nil
		end
		inst.AnimState:SetBuild(data.build)
		SetAlt(inst)
	else
		inst.isalt = nil
		inst.AnimState:SetBank(mob.bank) --for some reason SetAlt wasn't updating the mob correctly...
		inst.movestoptime = 6 * FRAMES
		inst.AnimState:SetBuild(mob.build)
	end
end
----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("monster")
    inst:AddTag("stalkerminion")
    inst:AddTag("fossil")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)	
	
	inst:WatchWorldState("issummer", function() PlayablePets.SetSandstormImmunity(inst) end) --makes immune to Sandstorm visual issues.
	
	PlayablePets.SetSandstormImmunity(inst)	
end

local master_postinit = function(inst)  
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0, 9999) --fire, acid, poison
	inst.components.health:StartRegen(5, 5)
	----------------------------------
	--Variables		
	inst.stalker_minion = true
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	inst.mobplayer = true
	inst.poisonimmune = true
	inst.mindcontrolimmune = true
	inst.ghostbuild = "ghost_monster_build"
	
	inst.AnimState:OverrideSymbol("fx_flames", "stalker_shadow_build", "fx_flames")
    inst.AnimState:OverrideSymbol("shield_minion", "stalker_shadow_build", "shield_minion")
    inst.AnimState:OverrideSymbol("fx_dark_minion", "stalker_shadow_build", "fx_dark_minion")
	
	inst.movestoptime = 6 * FRAMES
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.components.health.redirect = nostalkerordebrisdmg
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.ROUGHAGE, FOODTYPE.VEGGIE }, { FOODTYPE.ROUGHAGE, FOODTYPE.VEGGIE}) 
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1.5) --This might multiply food stats.
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 50, .3)
    inst.DynamicShadow:SetSize(1.5, 1)
    inst.Transform:SetSixFaced()
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) SetAlt(inst) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) SetAlt(inst) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
