local MakePlayerCharacter = require "prefabs/player_common"
local easing = require("easing")
---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "toadstoolp"

local assets = 
{
	Asset("ANIM", "anim/toadstool_basic.zip"),
    Asset("ANIM", "anim/toadstool_actions.zip"),
    Asset("ANIM", "anim/toadstool_build.zip"),
    Asset("ANIM", "anim/toadstool_upg_build.zip"),
	---
	Asset("ANIM", "anim/toadstool_shiny_build_01.zip"),
	Asset("ANIM", "anim/toadstool_upg_shiny_build_01.zip"),
	
	Asset("ANIM", "anim/toadstool_dark_shiny_build_01.zip"),
	Asset("ANIM", "anim/toadstool_dark_upg_shiny_build_01.zip"),
}



local prefabs = 
{	
	"mushroomsprout",
    "mushroombomb_projectile",
    "mushroom_light_blueprint",
}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = BOSS_STATS and TUNING.TOADSTOOL_HEALTH or 8500,
	hunger = 300,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 100,
	runspeed = 3,
	walkspeed = 3,
	damage = 200,
	range = 6,
	hit_range = 7,
	bank = "toadstool",
	build = "toadstool_build",
	build2 = "toadstool_upg_build",
	shiny = "toadstool",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGtoadstoolp",
	minimap = "toadstoolp.tex",
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('toadstoolp',
{
    {"froglegs",      1.00},
    {"meat",          1.00},
    {"meat",          1.00},
    {"meat",          1.00},
    {"meat",          0.50},
    {"meat",          0.25},

    {"shroom_skin",   1.00},
    {"shroom_skin",   1.00},

    {"red_cap",       1.00},
    {"red_cap",       0.33},
    {"red_cap",       0.33},

    {"blue_cap",      1.00},
    {"blue_cap",      0.33},
    {"blue_cap",      0.33},

    {"green_cap",     1.00},
    {"green_cap",     0.33},
    {"green_cap",     0.33},
})

local sounds =
{
    walk = "dontstarve/creatures/spat/walk",
    grunt = "dontstarve/creatures/spat/grunt",
    yell = "dontstarve/creatures/spat/yell",
    hit = "dontstarve/creatures/spat/hurt",
    death = "dontstarve/creatures/spat/death",
    curious = "dontstarve/creatures/spat/curious",
    sleep = "dontstarve/creatures/spat/sleep",
    angry = "dontstarve/creatures/spat/angry",
    spit = "dontstarve/creatures/spat/spit",
    spit_hit = "dontstarve/creatures/spat/spit_hit",
}

local FORGE_STATS = PPNR_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================
local function SetSkinDefault(inst, data)
	if data then
		inst.AnimState:SetBuild(data.build)
		if inst.level > 0 then
			inst.AnimState:OverrideSymbol("toad_mushroom", data.build2, "toad_mushroom"..tostring(inst.level))
		end
	else
		inst.AnimState:SetBuild(mob.build)
		if inst.level > 0 then
			inst.AnimState:OverrideSymbol("toad_mushroom", mob.build2, "toad_mushroom"..tostring(inst.level))
		end
	end	
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return  {"notarget", "wall", "toadstool", "corpse"}
	elseif PP_FORGE_ENABLED then
		return {"player", "companion", "INLIMBO", "notarget", "toadstool", "corpse"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "wall", "battlestandard", "toadstool", "corpse"}
	end
end

local function SetLevel(inst, level)
    inst.level = level
    if level < 1 then
        inst.AnimState:ClearOverrideSymbol("toad_mushroom")
        inst.components.locomotor.runspeed = 3
		inst.components.locomotor.fasteronroad = false
        inst.components.combat:SetDefaultDamage(PP_FORGE_ENABLED and PPNR_FORGE.TOADSTOOLP.DAMAGE or 200)
        inst.components.combat:SetAttackPeriod(3)
        inst.mushroombomb_variance = 0
        inst.mushroombomb_maxchain = 1
    else
		inst.components.ppskin_manager:LoadSkin(mob, true)
		inst.components.locomotor.fasteronroad = false
        if level == 1 then
            inst.components.locomotor.runspeed = 3.5
            inst.components.combat:SetDefaultDamage(PP_FORGE_ENABLED and PPNR_FORGE.TOADSTOOLP.DAMAGE or 250)
            inst.components.combat:SetAttackPeriod(2.5)
            inst.mushroombomb_variance = 1
            inst.mushroombomb_maxchain = 2
        elseif level == 2 then
            inst.components.locomotor.runspeed = 4
            inst.components.combat:SetDefaultDamage(PP_FORGE_ENABLED and PPNR_FORGE.TOADSTOOLP.DAMAGE or 300)
            inst.components.combat:SetAttackPeriod(1.75)
            inst.mushroombomb_variance = 2
            inst.mushroombomb_maxchain = 3
        else
            inst.components.locomotor.runspeed = 4.5
            inst.components.combat:SetDefaultDamage(PP_FORGE_ENABLED and PPNR_FORGE.TOADSTOOLP.DAMAGE or 400)
            inst.components.combat:SetAttackPeriod(1.40)
            inst.mushroombomb_variance = 3
            inst.mushroombomb_maxchain = 5
        end
    end
    inst:PushEvent("toadstoollevel", level)
end

local function SetPhaseLevel(inst, phase)
    inst.sporebomb_targets = TUNING.TOADSTOOL_SPOREBOMB_TARGETS_PHASE[phase]
    inst.sporebomb_cd = TUNING.TOADSTOOL_SPOREBOMB_CD_PHASE[phase]
    inst.mushroombomb_count = TUNING.TOADSTOOL_MUSHROOMBOMB_COUNT_PHASE[phase]
    if phase > 2 then
        inst.components.timer:ResumeTimer("pound_cd")
    else
        inst.components.timer:StopTimer("pound_cd")
        inst.components.timer:StartTimer("pound_cd", TUNING.TOADSTOOL_ABILITY_INTRO_CD, true)
    end
end

local function DropShroomSkin(inst)
    local pt = inst:GetPosition()
    pt.y = 4
   -- inst.components.lootdropper:SpawnLootPrefab("shroom_skin", pt)
end

local PHASE2_HEALTH = .7
local PHASE3_HEALTH = .4
local PHASE4_HEALTH = .15

local function OnHealthUpdate(inst)
	local bosshealth = inst.components.health:GetPercent()
	
	--Note:It seems sometimes the code farts up and sets the toadstool to 0. Maybe try improving the checks?
	if bosshealth < PHASE4_HEALTH then
		SetLevel(inst, 3)
	elseif bosshealth > PHASE4_HEALTH and bosshealth <= PHASE3_HEALTH then
		SetLevel(inst, 2)
	elseif bosshealth > PHASE3_HEALTH and bosshealth <= PHASE2_HEALTH then
		SetLevel(inst, 1)
	elseif bosshealth > PHASE2_HEALTH then
		SetLevel(inst, 0)
	else
		print("DEBUG: Toadstool phase error! Setting level to 0.")
		SetLevel(inst, 0) --failsafe
	end
end

local function EnterPhase2Trigger(inst)
    SetPhaseLevel(inst, 2)
	SetLevel(inst, 1)
    if inst.components.health:GetPercent() > PHASE3_HEALTH then
        DropShroomSkin(inst)
    end
	if not inst.sg:HasStateTag("sleeping") then
		inst.sg:GoToState("special_atk1")
	end
end

local function EnterPhase3Trigger(inst)
	SetLevel(inst, 2)
    SetPhaseLevel(inst, 3)
    if not inst.components.health:IsDead() then
        DropShroomSkin(inst)
    end
	if not inst.sg:HasStateTag("sleeping") then
		inst.sg:GoToState("special_atk1")
	end
end

local function EnterPhase4Trigger(inst)
	SetLevel(inst, 3)
    SetPhaseLevel(inst, 3)
    if not inst.components.health:IsDead() then
        DropShroomSkin(inst)
    end
	if not inst.sg:HasStateTag("sleeping") then
		inst.sg:GoToState("special_atk1")
	end
end

--------------------------------------------------------------------------

local function FindSporeBombTargets(inst, preferredtargets)
    local targets = {}

    if preferredtargets ~= nil then
        for i, v in ipairs(preferredtargets) do
            if v:IsValid() and v.entity:IsVisible() and not v.sporeimmune and not (TheNet:GetPVPEnabled() and v:HasTag("player"))  and
                v.components.debuffable ~= nil and
                v.components.debuffable:IsEnabled() and
                not v.components.debuffable:HasDebuff("sporebomb") and
                not (v.components.health ~= nil and
                    v.components.health:IsDead()) and
                v:IsNear(inst, TUNING.TOADSTOOL_SPOREBOMB_HIT_RANGE) then
                table.insert(targets, v)
                if #targets >= inst.sporebomb_targets then
                    return targets
                end
            end
        end
    end

    local newtargets = {}
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, TUNING.TOADSTOOL_SPOREBOMB_ATTACK_RANGE, { "debuffable" }, GetExcludeTags(inst))
    for i, v in ipairs(ents) do
        if v.entity:IsVisible() and
            v.components.debuffable ~= nil and
            not v.components.debuffable:HasDebuff("sporebomb") and v.sporeimmune == nil and
            not (v.components.health ~= nil and 
                v.components.health:IsDead()) then
            table.insert(newtargets, v)
        end
    end

    for i = #targets + 1, inst.sporebomb_targets do
        if #newtargets <= 0 then
            return targets
        end
        table.insert(targets, table.remove(newtargets, math.random(#newtargets)))
    end

    return targets
end

local function DoSporeBomb(inst, targets)
    for i, v in ipairs(FindSporeBombTargets(inst, targets)) do
        v.components.debuffable:AddDebuff("sporebomb", "sporebomb")
    end
end

--------------------------------------------------------------------------

local function FindMushroomBombTargets(inst)
    --ring with a random gap
    local maxbombs = inst.mushroombomb_variance > 0 and inst.mushroombomb_count + math.random(inst.mushroombomb_variance) or inst.mushroombomb_count
    local delta = (1 + math.random()) * PI / maxbombs
    local offset = 2 * PI * math.random()
    local angles = {}
    for i = 1, maxbombs do
        table.insert(angles, i * delta + offset)
    end

    local pt = inst:GetPosition()
    local range = GetRandomMinMax(TUNING.TOADSTOOL_MUSHROOMBOMB_MIN_RANGE, TUNING.TOADSTOOL_MUSHROOMBOMB_MAX_RANGE)
    local targets = {}
    while #angles > 0 do
        local theta = table.remove(angles, math.random(#angles))
        local offset = FindWalkableOffset(pt, theta, range, 12, true)
        if offset ~= nil then
            offset.x = offset.x + pt.x
            offset.y = 0
            offset.z = offset.z + pt.z
            table.insert(targets, offset)
        end
    end

    return targets
end

local CUSTOM_MUSHROOM_DAMAGE = {100, 125, 150, 200}

local function SpawnMushroomBombProjectile(inst, targets)
    local x, y, z = inst.Transform:GetWorldPosition()
    local projectile = SpawnPrefab("mushroombomb_projectile")
	projectile.setreturn = CUSTOM_MUSHROOM_DAMAGE[inst.level]
    projectile.Transform:SetPosition(x, y, z)
    projectile.components.entitytracker:TrackEntity("toadstoolp", inst)
    projectile.SetSource = PlayablePets.SetProjectileSource

    --V2C: scale the launch speed based on distance
    --     because 15 does not reach our max range.
    local targetpos = table.remove(targets, 1)
    local dx = targetpos.x - x
    local dz = targetpos.z - z
    local rangesq = dx * dx + dz * dz
    local maxrange = 15
    local bigNum = 15 -- 13 + (math.random()*4)
    local speed = easing.linear(rangesq, bigNum, 3, maxrange * maxrange)
    projectile.components.complexprojectile:SetHorizontalSpeed(speed)
    projectile.components.complexprojectile:Launch(targetpos, inst, inst)

    if #targets > 0 then
        inst:DoTaskInTime(FRAMES, SpawnMushroomBombProjectile, targets)
    end
end

local function DoMushroomBomb(inst)
    local targets = FindMushroomBombTargets(inst)
    if #targets > 0 then
        inst:DoTaskInTime(FRAMES, SpawnMushroomBombProjectile, targets)
    end
end

--------------------------------------------------------------------------

local function FindMushroomSproutAngles(inst)
    --evenly spaced ring
    local maxspawns = TUNING.TOADSTOOL_MUSHROOMSPROUT_NUM
    local delta = 2 * PI / maxspawns
    local offset = 2 * PI * math.random()
    local angles = {}
    for i = 1, maxspawns do
        table.insert(angles, i * delta + offset)
    end
    return angles
end

local function DoMushroomSprout(inst, angles)
    if angles == nil or #angles <= 0 then
        return
    end

    local map = TheWorld.Map
    local pt = inst:GetPosition()
    local theta = table.remove(angles, math.random(#angles))
    local radius = GetRandomMinMax(TUNING.TOADSTOOL_MUSHROOMSPROUT_MIN_RANGE, TUNING.TOADSTOOL_MUSHROOMSPROUT_MAX_RANGE)
    local offset = FindWalkableOffset(pt, theta, radius, 12, true)
    pt.y = 0

    --number of attempts to find an unblocked spawn point
    for i = 1, 10 do
        if i > 1 then
            offset = FindWalkableOffset(pt, 2 * PI * math.random(), 2.5, 8, true)
        end
        if offset ~= nil then
            pt.x = pt.x + offset.x
            pt.z = pt.z + offset.z
            if map:CanDeployAtPoint(pt, inst) then --inst is dummy param cuz we can't pass nil
                local ent = SpawnPrefab("mushroomsprout")
                ent.Transform:SetPosition(pt:Get())
                ent:PushEvent("linktoadstoolp", inst)
				ent.isplayerbomb = true
				ent.setreturn = CUSTOM_MUSHROOM_DAMAGE[inst.level]
                break
            end
        end
    end
end

--------------------------------------------------------------------------

local function CalculateLevel(links)
    return (links < 1 and 0)
        or (links < 5 and 1)
        or (links < 8 and 2)
        or 3
end

local function OnUnlinkMushroomSprout(inst, link)
    if inst._links[link] ~= nil then
        inst:RemoveEventCallback("onremove", inst._links[link], link)
        inst._links[link] = nil
        inst._numlinks = inst._numlinks - 1
        SetLevel(inst, CalculateLevel(inst._numlinks))
    end
end

local function OnLinkMushroomSprout(inst, link)
    if inst._links[link] == nil then
        inst._numlinks = inst._numlinks + 1
        inst._links[link] = function(link) OnUnlinkMushroomSprout(inst, link) end
        inst:ListenForEvent("onremove", inst._links[link], link)
        SetLevel(inst, CalculateLevel(inst._numlinks))
    end
end

--==============================================
--				Custom Common Functions
--==============================================	
----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
		local healthpct = inst.components.health:GetPercent()
		SetPhaseLevel(
			inst,
			(healthpct > PHASE2_HEALTH and 1) or
			(healthpct > PHASE3_HEALTH and 2) or
			(healthpct > PHASE4_HEALTH and 3) or 3
		)
		SetLevel(
			inst,
			(healthpct > PHASE2_HEALTH and 0) or
			(healthpct > PHASE3_HEALTH and 1) or
			(healthpct > PHASE4_HEALTH and 2) or 3
		)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.level = data.level
		
	end
    
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.level = inst.level or 0
	
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 2
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("epic")
    inst:AddTag("monster")
    inst:AddTag("scarytoprey")
	inst:AddTag("toadstool")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)	
end

local master_postinit = function(inst)  
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 1, 0.5, 0, 250) --fire, acid, poison
	----------------------------------
	--Variables		
	inst.taunt = true
	inst.mobsleep = true
	inst.taunt2 = true
    --inst.taunt3 = true
	inst.specialatk2 = true
	inst.ghostbuild = "ghost_monster_build"
	inst.sporeimmune = true
	inst.poisonimmune = true
	
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)

	inst:AddComponent("groundpounder")
	inst.components.groundpounder.damageRings = 0
	inst:AddComponent("grouptargeter")
	inst:AddComponent("timer")

	inst:AddComponent("healthtrigger")
    inst.components.healthtrigger:AddTrigger(PHASE2_HEALTH, EnterPhase2Trigger)
    inst.components.healthtrigger:AddTrigger(PHASE3_HEALTH, EnterPhase3Trigger)
	inst.components.healthtrigger:AddTrigger(PHASE4_HEALTH, EnterPhase4Trigger)
	
	inst.hit_recovery = 2
	inst.shouldwalk = true

	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    --MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.freezable_extra_resist = 100
    inst._freezeresisttask = nil
	
	inst.FindSporeBombTargets = FindSporeBombTargets
    inst.DoSporeBomb = DoSporeBomb
    inst.DoMushroomBomb = DoMushroomBomb
    inst.FindMushroomSproutAngles = FindMushroomSproutAngles
    inst.DoMushroomSprout = DoMushroomSprout
    --inst.OnEscaped = OnEscaped
	inst.OnHealthUpdate = OnHealthUpdate

    inst.sporebomb_targets = TUNING.TOADSTOOL_SPOREBOMB_TARGETS_PHASE[1]
    inst.sporebomb_cd = TUNING.TOADSTOOL_SPOREBOMB_CD_PHASE[1]

    inst.mushroombomb_count = TUNING.TOADSTOOL_MUSHROOMBOMB_COUNT_PHASE[1]
    inst.mushroombomb_variance = TUNING.TOADSTOOL_MUSHROOMBOMB_VAR_LVL[0]
    inst.mushroombomb_maxchain = TUNING.TOADSTOOL_MUSHROOMBOMB_CHAIN_LVL[0]
    inst.mushroombomb_cd = TUNING.TOADSTOOL_MUSHROOMBOMB_CD

    inst.mushroomsprout_cd = TUNING.TOADSTOOL_MUSHROOMSPROUT_CD

    inst.pound_cd = TUNING.TOADSTOOL_POUND_CD
    inst.pound_speed = 0

    inst.hit_recovery = TUNING.TOADSTOOL_HIT_RECOVERY_LVL[0]

    inst.level = 0
    inst._numlinks = 0
    inst._links = {}
    inst:ListenForEvent("linkmushroomsprout", OnLinkMushroomSprout)
    inst:ListenForEvent("unlinkmushroomsprout", OnUnlinkMushroomSprout)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	
	inst:AddComponent("epicscare")
    inst.components.epicscare:SetRange(TUNING.TOADSTOOL_EPICSCARE_RANGE)
	----------------------------------
	--Eater--
	--inst.components.eater:SetDiet({ FOODTYPE.ROUGHAGE, FOODTYPE.VEGGIE }, { FOODTYPE.ROUGHAGE, FOODTYPE.VEGGIE}) 
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1.5) --This might multiply food stats.
	---------------------------------
	--Physics and Shadows--
	MakeGiantCharacterPhysics(inst, 1000, 2.2) --2.5
	inst.Physics:SetMass(99999)
	inst.isdark = true
	--MakeCharacterPhysics(inst, 10, .5)
	--MakeGhostPhysics(inst, 1, .5) --Special physics for flying characters(?)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
    inst.DynamicShadow:SetSize(6, 3.5)
	
	inst.Light:SetRadius(2)
    inst.Light:SetFalloff(.5)
    inst.Light:SetIntensity(.75)
    inst.Light:SetColour(255 / 255, 235 / 255, 153 / 255)
	
	inst.AnimState:SetLightOverride(.3)
	--inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	--inst:ListenForEvent("attacked", onattacked)
	inst:ListenForEvent("healthdelta", OnHealthUpdate)
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
