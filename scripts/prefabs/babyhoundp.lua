local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--Make it grow up into a hound.
---------------------------


local assets = 
{
	Asset("ANIM", "anim/pupington_shiny_build_01.zip"),
	
	Asset("ANIM", "anim/hound_basic.zip"),
    Asset("ANIM", "anim/hound_basic_water.zip"),
    Asset("ANIM", "anim/hound.zip"),
    Asset("ANIM", "anim/hound_ocean.zip"),
    Asset("ANIM", "anim/hound_red.zip"),
    Asset("ANIM", "anim/hound_red_ocean.zip"),
    Asset("ANIM", "anim/hound_ice.zip"),
    Asset("ANIM", "anim/hound_ice_ocean.zip"),
    Asset("ANIM", "anim/hound_mutated.zip"),
	
}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = 125,
	hunger = 75,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 100,
	runspeed = TUNING.CRITTER_WALK_SPEED,
	walkspeed = TUNING.CRITTER_WALK_SPEED,
	damage = 20,
	range = 1,
	bank = "pupington", --hopefully uses the same bank
	build = "pupington_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGbabyhoundp",
	minimap = "babyhoundp.tex",
	
}

local mob_adult = 
{
	health = 250,
	hunger = 125,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 100,
	runspeed = TUNING.HOUND_SPEED,
	walkspeed = TUNING.HOUND_SPEED/2,
	damage = 20,
	range = 2,
	hit_range = 3,
	bank = "hound", --hopefully uses the same bank
	build = "hound_ocean",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGhoundp",
	minimap = "babyhoundp.tex",
	
}

local adult_sounds =
{
    pant = "dontstarve/creatures/hound/pant",
    attack = "dontstarve/creatures/hound/attack",
    bite = "dontstarve/creatures/hound/bite",
    bark = "dontstarve/creatures/hound/bark",
    death = "dontstarve/creatures/hound/death",
    sleep = "dontstarve/creatures/hound/sleep",
    growl = "dontstarve/creatures/hound/growl",
    howl = "dontstarve/creatures/together/clayhound/howl",
    hurt = "dontstarve/creatures/hound/hurt",
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('babyhoundp',
-----Prefab---------------------Chance------------
{
    {'monstermeat',			1.00},
    
   
})

SetSharedLootTable('houndp',
-----Prefab---------------------Chance------------
{
    {'monstermeat',			1.00},
	{'houndstooth',			0.50},
	{'houndstooth',			0.50},    
   
})

SetSharedLootTable('hound_firep',
-----Prefab---------------------Chance------------
{
    {'monstermeat',			1.00},
	{'houndstooth',			0.50},
	{'houndstooth',			0.50},
	{'redgem', 				0.25},
})

SetSharedLootTable('hound_icep',
-----Prefab---------------------Chance------------
{
    {'monstermeat',			1.00},
	{'houndstooth',			0.50},
	{'houndstooth',			0.50},
	{'bluegem', 			0.25},
})

local ADULT_REQ = 150
local TEEN_REQ = 45

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("hound") and not dude.components.health:IsDead() end, 10)
    end
end

local function onhitother2(inst, other)
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("hound") and not dude.components.health:IsDead() end, 10)
end

-------------------------------------------------------
--Growth Code--

local function GetHoundType(inst)
	if inst.houndtype then
		return inst.houndtype
	else
		if not TheWorld.state.iswinter and not TheWorld.state.issummer then
			inst.houndtype = ""
		elseif TheWorld.state.iswinter then
			inst.houndtype = "_ice"
		elseif TheWorld.state.issummer then	
			inst.houndtype = "_red"
		else
			print("BABYHOUND ERROR: Couldn't get proper type! Setting to none!")
			inst.houndtype = "" --incase of weird events, became default hound.
		end
		return inst.houndtype
	end	
end

local function GetHoundBuild(inst)
	if inst.isadult then
		if not inst.houndtype or inst.houndtype == "" then
			return "hound_ocean"
		else
			return "hound"..inst.houndtype.."_ocean"
		end
	else
		return nil
	end
end

local function SetAdult(inst)
	inst.isadult = true
	local houndtype = GetHoundType(inst)
	inst.sounds = adult_sounds
	
	PlayablePets.CommonSetChar(inst, mob_adult)
	PlayablePets.SetCommonStats(inst, mob_adult, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, houndtype == "_fire" and 60 or nil, houndtype == "_ice" and 20 or nil) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, houndtype == "_fire" and 0 or 1, nil, nil, houndtype == "_ice" and 9999 or nil) --fire, acid, poison, freeze (flat value, not a multiplier)
	
	if not inst:HasTag("playerghost") then --just added incase of weirdness.
		inst.AnimState:SetBank("hound")
		if houndtype == "" then
			if inst.isshiny ~= 0 then
				inst.AnimState:SetBuild("hound_shiny_build_0"..inst.isshiny)
			else
				inst.AnimState:SetBuild(mob_adult.build)
			end
		else
			if inst.isshiny ~= 0 then
				inst.AnimState:SetBuild("hound"..houndtype.."_shiny_build_0"..inst.isshiny)
			else
				inst.AnimState:SetBuild("hound"..houndtype.."_ocean")
			end
		end
	end
	inst.sg:GoToState("taunt")
	
	MakeCharacterPhysics(inst, 10, .5)
    inst.DynamicShadow:SetSize(2.5, 1.5)
    inst.Transform:SetFourFaced()
	
	PlayablePets.SetAmphibious(inst, "hound", "hound_water")
	
	if inst:HasTag("swimming") then
		inst.components.locomotor.runspeed = TUNING.HOUND_SWIM_SPEED
	end
	
	inst.components.combat:SetHurtSound("dontstarve/creatures/hound/hit")
	
	local body_symbol = "hound_body"
	inst.poisonsymbol = body_symbol
	MakeMediumFreezableCharacter(inst, "hound_body")
	MakeMediumBurnableCharacter(inst, "hound_body")
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.components.lootdropper:SetChanceLootTable("hound"..houndtype.."p")


	PlayablePets.SetAmphibious(inst, "hound", "hound_water")
	
    inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT })
    inst.components.eater:SetCanEatHorrible()
	inst.components.eater:SetAbsorptionModifiers(1,1.5,1.5)
	
	inst.components.eater.strongstomach = true -- can eat monster meat!
end

local function OnGrowUp(inst)
	inst.Transform:SetPosition(inst.Transform:GetWorldPosition())
	SetAdult(inst)
end

local function applyupgrades(inst)

    local health_percent = inst.components.health:GetPercent()
	local hunger_percent = inst.components.hunger:GetPercent()
	--local sanity_percent = inst.components.sanity:GetPercent()
	
	if inst.isadult then
	inst.components.health.maxhealth = mob.health
	inst.components.hunger.max = mob.hunger
	end
	
    if inst.houndtype == "" then
		inst.components.health.maxhealth = mob_adult.health --give bonus hp for grown up pups
		inst.components.hunger.max = mob_adult.hunger	
	else
		inst.components.health.maxhealth = mob_adult.health
		inst.components.hunger.max = mob_adult.hunger
    end

    inst.components.health:SetPercent(health_percent)
end

local function OnEat(inst, food)
	local age = inst.components.age:GetAgeInDays()
    if food and food.components.edible then
		if food.components.edible.foodtype == FOODTYPE.MEAT and MOBGROW~= "Disable" then
			--food helps you grow!
			inst.dogfood = inst.dogfood + 1
			if inst.dogfood >= 45 and not inst.isadult then
				OnGrowUp(inst)
				applyupgrades(inst)
			end
		end
    end
end

local function ondeath(inst)
	inst.dogfood = 0
	inst.houndtype = nil
    applyupgrades(inst)
	if inst.components.amphibiouscreature then
		inst:RemoveComponent("amphibiouscreature")
	end
	inst.components.combat:SetHurtSound(nil)
	inst.amphibious = nil
	inst.isadult = nil
end
-------------------------------------------------------
----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.dogfood = data.dogfood or 0
		inst.houndtype = data.houndtype or ""
		
		inst.isadult = data.isadult or nil
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.dogfood = inst.dogfood > 0 and inst.birdfood or nil
	data.houndtype = inst.houndtype or nil
	data.isadult = inst.isadult or false
	
end

local function OnPreload(inst, data)
    if data ~= nil and data.houndtype ~= nil then
        inst.houndtype = data.houndtype or nil
		inst.isadult = data.isadult or nil
        applyupgrades(inst)
        --re-set these from the save data, because of load-order clipping issues
        if data.health and data.health.health then inst.components.health:SetCurrentHealth(data.health.health) end
        if data.hunger and data.hunger.hunger then inst.components.hunger.current = data.hunger.hunger end
        --if data.sanity and data.sanity.current then inst.components.sanity.current = data.sanity.current end
        inst.components.health:DoDelta(0)
        inst.components.hunger:DoDelta(0)
        inst.components.sanity:DoDelta(0)
		if inst.isadult then
			SetAdult(inst)
		end
    end
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
	inst:WatchWorldState("issummer", function() PlayablePets.SetSandstormImmunity(inst) end) --makes immune to Sandstorm visual issues.
	
	PlayablePets.SetSandstormImmunity(inst)	
end

local function GetMobTable(inst)
	if inst.isadult then
		return mob_adult
	else
		return mob
	end
end

local master_postinit = function(inst)  
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)	
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('babyhoundp')
	----------------------------------
	--Tags--
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	inst.mobplayer = true
	--inst:AddTag("special_atk1") --allows taunting.
	
	inst:AddTag("hound")
	inst:AddTag("monster")
	
	inst.taunt = true
	inst.taunt2 = true
	inst.specialatk2 = true
	inst.mobsleep = true
	inst.ghostbuild = "ghost_monster_build"
	
	
	inst.sounds = adult_sounds
	
	inst.isadult = false
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.scared_task = nil
	----------------------------------
	--Growth--
	inst.dogfood = 0
	inst.houndtype = nil
	
	inst.userfunctions =
    {
        SetAdult = SetAdult
    }
	
	----------------------------------
	--PvP Mode Stuff--
	inst.mobteleported = false
	----------------------------------
	--Eater--
	
	--Kinds of diets I know of: MEAT, ROUGHAGE(grass and stuff), GEARS, OMNI(no idea it prevents eating entirely, just disable this if you want both meat and veggies).
    inst.components.eater:SetAbsorptionModifiers(1,2,2) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
    inst.components.eater:SetOnEatFn(OnEat) --Runs a function when the player eats something.
	---------------------------------
	--Physics and Scale--
	MakeCharacterPhysics(inst, 1, .5)
    inst.DynamicShadow:SetSize(2, .75)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", onattacked)
	inst:ListenForEvent("death", ondeath)
	inst.components.combat.onhitotherfn = onhitother2
	---------------------------------
	--Functions that saves and loads data.
	
	--inst.OnLongUpdate = onlongupdate
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    inst.OnPreLoad = OnPreload
    ------------------------------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst)) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst), nil, nil, GetHoundBuild(inst)) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
    inst:ListenForEvent("respawnfromghost", function() 
		inst:DoTaskInTime(5, function(inst) PlayablePets.SetCommonStats(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst)) end)
    end)
	
    return inst
	
end

return MakePlayerCharacter("babyhoundp", prefabs, assets, common_postinit, master_postinit, start_inv)
