local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--Make it grow up into a hound.
---------------------------


local assets = 
{
	Asset("ANIM", "anim/glomling_shiny_build_01.zip"),
	Asset("ANIM", "anim/glommer_shiny_build_01.zip"),
}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = 100,
	hunger = 75,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 100,
	runspeed = TUNING.CRITTER_WALK_SPEED,
	walkspeed = TUNING.CRITTER_WALK_SPEED,
	damage = 10,
	range = 1,
	hit_range = 2,
	bank = "glomling", 
	build = "glomling_build",
	scale = 1,
	shiny = "glomling",
	--build2 = "alternate build here",
	stategraph = "SGbabyglomp",
	minimap = "babyglommerp.tex",
	
}

local mob_adult = 
{
	health = 250,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 125,
	runspeed = 7,
	walkspeed = 7,
	damage = 20,
	range = 2,
	hit_range = 3,
	bank = "glommer",
	build = "glommer",
	scale = 1,
	shiny = "glommer",
	--build2 = "alternate build here",
	stategraph = "SGglom_p",
	minimap = "babyglommerp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('babyglomp',
-----Prefab---------------------Chance------------
{
	{'monstermeat',			1.00},
    
   
})

SetSharedLootTable('bigglomp',
-----Prefab---------------------Chance------------
{
    {'monstermeat',             1.00},
    {'monstermeat',             1.00},
    {'monstermeat',             1.00},
    {'glommerwings',            1.00},
    {'glommerfuel',             1.00},
    {'glommerfuel',             1.00},
})

-------------------------------------------------------------------
--Glommer functions--
local function OnSpawnFuel(inst, fuel)
	if not inst.sg:HasStateTag("busy") and not inst.components.health:IsDead() then
		inst.sg:GoToState("goo", fuel)
	end
end
-------------------------------------------------------
--Growth Code--

local function SetAdult(inst)
	inst.sg:GoToState("idle")
	
	PlayablePets.SetCommonStats(inst, mob_adult, nil, true) --mob table, ishuman, ignorepvpmultiplier
	inst.components.ppskin_manager:LoadSkin(mob_adult, true)
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	
	MakeGhostPhysics(inst, 1, .5) --Special physics for flying characters(?)
    inst.DynamicShadow:SetSize(2, .75)
	
	PlayablePets.SetAmphibious(inst, nil, nil, true, nil, true)
	
	inst.isadult = true --added as a failsafe
	
	inst.components.lootdropper:SetChanceLootTable('bigglomp')

	inst.components.eater:SetAbsorptionModifiers(4,4,3)
	
	inst.components.eater.strongstomach = true -- can eat monster meat!
	
	inst.taunt2 = false
	inst.taunt2 = false
	inst.specialatk2 = false
	
	inst:AddComponent("periodicspawner")
    inst.components.periodicspawner:SetOnSpawnFn(OnSpawnFuel)
    inst.components.periodicspawner.prefab = "glommerfuel"
    inst.components.periodicspawner.basetime = TUNING.TOTAL_DAY_TIME * 2
    inst.components.periodicspawner.randtime = TUNING.TOTAL_DAY_TIME * 2
    inst.components.periodicspawner:Start()
	
	inst.Transform:SetFourFaced()
end

local function OnGrowUp(inst)
	inst.Transform:SetPosition(inst.Transform:GetWorldPosition())
	inst.isadult = true 
	inst.sg:GoToState("growup")
end

local function applyupgrades(inst)

    local health_percent = inst.components.health:GetPercent()
	local hunger_percent = inst.components.hunger:GetPercent()
	--local sanity_percent = inst.components.sanity:GetPercent()
	
	if inst.isadult == false then
		inst.components.health.maxhealth = mob.health
		inst.components.hunger.max = mob.hunger
	
	else
		inst.components.health.maxhealth = mob_adult.health --give bonus hp for grown up pups
		inst.components.hunger.max = mob_adult.hunger	
	end
    inst.components.health:SetPercent(health_percent)
end

local ADULT_REQ = 80

local function OnEat(inst, food)
	local age = inst.components.age:GetAgeInDays()
	--print(age)
    if food and food.components.edible then
		if MOBGROW~= "Disable" then
			--food helps you grow!
			inst.glomfood = inst.glomfood + 1
			if inst.glomfood >= ADULT_REQ and not inst.isadult then
				--inst.sg:GoToState("transform")
				OnGrowUp(inst)
				applyupgrades(inst)
			end
		end
    end
end

local function ondeath(inst)
    inst.glomfood = 0
	inst.isadult = false
    applyupgrades(inst)
end
-------------------------------------------------------
----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.glomfood = data.glomfood or 0
		inst.isadult = data.isadult or false
		
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.glomfood = inst.glomfood > 0 and inst.glomfood or nil
	data.isadult = inst.isadult or false
	
end

local function OnPreload(inst, data)
    if data ~= nil and data.isadult ~= nil then
        inst.isadult = data.isadult
        applyupgrades(inst)
        --re-set these from the save data, because of load-order clipping issues
        if data.health and data.health.health then inst.components.health:SetCurrentHealth(data.health.health) end
        if data.hunger and data.hunger.hunger then inst.components.hunger.current = data.hunger.hunger end
        --if data.sanity and data.sanity.current then inst.components.sanity.current = data.sanity.current end
        inst.components.health:DoDelta(0)
        inst.components.hunger:DoDelta(0)
        inst.components.sanity:DoDelta(0)
		if inst.isadult then
			SetAdult(inst)
		end
    end
end

local function SetSkinDefault(inst, data)
	if not inst:HasTag("playerghost") then
		if data then
			if inst.isadult ~= nil and inst.isadult == true then
				inst.AnimState:SetBuild(data.adult_build)
			else	
				inst.AnimState:SetBuild(data.build)			
			end
		else
			inst.AnimState:SetBuild(inst.isadult and mob_adult.build or mob.build)
		end	
	end
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end

local function GetMobTable(inst)
	if inst.glomfood < ADULT_REQ then
		return mob
	elseif inst.glomfood >= ADULT_REQ then
		return mob_adult
	end
end


local master_postinit = function(inst)  
	------------------------------------------
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, nil, nil, 9999) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('babyglomp')
	----------------------------------
	--Tags--
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	inst.mobplayer = true
	
    inst:AddTag("flying")
	
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	
	inst.ghostbuild = "ghost_monster_build"
	----------------------------------
	--Growth--
	inst.glomfood = 0
	inst.isadult = false
	
	inst.userfunctions =
    {
        SetAdult = SetAdult,	
    }
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	----------------------------------
	--PvP Mode Stuff--
	inst.mobteleported = false
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,2,2) --This might multiply food stats.
    inst.components.eater:SetOnEatFn(OnEat) --Runs a function when the player eats something.
	---------------------------------
	--Physics and Scale--
	MakeGhostPhysics(inst, 1, .5) --Special physics for flying characters(?)
    inst.DynamicShadow:SetSize(2, .75)
    inst.Transform:SetSixFaced()
	
	PlayablePets.SetAmphibious(inst, nil, nil, true, nil, true)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("death", ondeath)
	---------------------------------
	--Functions that saves and loads data.
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    inst.OnPreLoad = OnPreload
  	------------------------------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst), true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
    inst:ListenForEvent("respawnfromghost", function() 
		inst:DoTaskInTime(5, function(inst) PlayablePets.SetCommonStats(inst, GetMobTable(inst), nil, true) end)
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst), true, true) end)
		inst.Transform:SetSixFaced()
		PlayablePets.SetAmphibious(inst, nil, nil, true, nil, true)
		inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/together/glomling/flap_LP", "flying")
    end)
	
    return inst
	
end

return MakePlayerCharacter("babyglommerp", prefabs, assets, common_postinit, master_postinit, start_inv)
