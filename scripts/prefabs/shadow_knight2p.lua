local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local MOB_HEALTH = BOSS_STATS and {900, 2700, 8100} or {175, 550, 4500}
local MOB_HUNGER = {150, 200, 250}
local MOB_DAMAGE = BOSS_STATS and {40, 90, 150} or {35 * 2, 50 * 2, 75 * 2 }
local MOB_ATTACKPERIOD = BOSS_STATS and {3, 2.5, 2} or {0, 1, 4}
local MOB_RANGE = {2.3, 4.5, 4.5} --BOSS_STATS and {4, 6, 8}  or {4, 6, 8}
local MOB_HIT_RANGE = {3, 5, 7}

--kills
local LVL2_REQ = 30
local LVL3_REQ = 130

local prefabname = "shadow_knight2p"

local assets = 
{
	
}



local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = MOB_HEALTH[3], --unused, used table instead.
	hunger = MOB_HUNGER[3],
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 300,
	runspeed = 7,
	walkspeed = 7,
	damage = MOB_DAMAGE[3],
	attackperiod = MOB_ATTACKPERIOD[3],
	range = MOB_RANGE[3],
	hit_range = MOB_HIT_RANGE[3],
	bank = "shadow_knight",
	build = "shadow_knight", --this or shadow_rook_upg_build
	shiny = "shadow_knight",
	scale = TUNING.SHADOW_KNIGHT.LEVELUP_SCALE[3],
	--build2 = "alternate build here",
	stategraph = "SGshadow_knightp",
	minimap = "shadow_knight2p.tex",
}
-----------------------
--Loot that drops when you die, duh.
local function lootsetfn(lootdropper) --very neat stuff here.
    local loot = {}

    if lootdropper.inst.level >= 2 then
        for i = 1, math.random(2, 3) do
            table.insert(loot, "nightmarefuel")
        end

        if lootdropper.inst.level >= 3 then

        end
    end

    lootdropper:SetLoot(loot)
end

local sounds =
{
    attack = "dontstarve/sanity/knight/attack_1",
    attack_grunt = "dontstarve/sanity/knight/attack_grunt",
    death = "dontstarve/sanity/knight/die",
    idle = "dontstarve/sanity/knight/idle",
    taunt = "dontstarve/sanity/knight/taunt",
    appear = "dontstarve/sanity/creature1/appear",
    disappear = "dontstarve/sanity/knight/dissappear",
    levelup = "dontstarve/sanity/creature1/taunt",
    hit = "dontstarve/sanity/knight/hit_response",
}

local FORGE_STATS = PPNR_FORGE[string.upper(prefabname)] --SHADOW_KNIGHT
--==============================================
--					Mob Functions
--==============================================
----Level Up----


local MAX_LEVEL = 3

local function IsMaxLevel(inst)
    return inst.level == MAX_LEVEL
end

local function commonlevelup(inst, overridelevel)
    if inst.components.health:IsDead() then
        return
    end
    local level = math.min(overridelevel or (inst.level + 1), MAX_LEVEL)
    if level ~= inst.level then
        inst.level = level

        local scale = TUNING.SHADOW_KNIGHT.LEVELUP_SCALE[level]

        local x, y, z = inst.Transform:GetWorldPosition()
        inst.Transform:SetScale(scale, scale, scale)
        --inst.Physics:SetCapsule(PHYS_RADIUS["shadow_rook"] * scale, 1)
        inst.Physics:Teleport(x, y, z)

        inst.AnimState:SetMultColour(1, 1, 1, 0.5 + (0.12*(level-1)))

        inst.components.health:SetMaxHealth(MOB_HEALTH[level])
		inst.components.hunger:SetMax(MOB_HUNGER[level])
        
        if level > 1 then
            inst:AddTag("epic")
        else
            inst:RemoveTag("epic")
        end

        return level, scale
    end
end

local function levelup(inst, overridelevel)
    local level, scale = commonlevelup(inst, overridelevel)
    if level ~= nil then
        inst.components.combat:SetDefaultDamage(MOB_DAMAGE[level])
        inst.components.combat:SetRange(TUNING.SHADOW_BISHOP.ATTACK_RANGE[level], TUNING.SHADOW_BISHOP.HIT_RANGE * scale)
        inst.components.combat:SetAttackPeriod(TUNING.SHADOW_BISHOP.ATTACK_PERIOD[level])

        if level > 1 then
            local suffix = tostring(level - 1)
            inst.AnimState:OverrideSymbol("arm",       "shadow_knight_upg_build", "arm"..suffix)
            inst.AnimState:OverrideSymbol("ear",       "shadow_knight_upg_build", "ear"..suffix)
            inst.AnimState:OverrideSymbol("face",      "shadow_knight_upg_build", "face"..suffix)
            inst.AnimState:OverrideSymbol("head",      "shadow_knight_upg_build", "head"..suffix)
            inst.AnimState:OverrideSymbol("leg_low",   "shadow_knight_upg_build", "leg_low"..suffix)
            inst.AnimState:OverrideSymbol("neck",      "shadow_knight_upg_build", "neck"..suffix)
            inst.AnimState:OverrideSymbol("spring",    "shadow_knight_upg_build", "spring"..suffix)
        else
            inst.AnimState:ClearAllOverrideSymbols()
        end
    end
end


local function ForceLevelUp(inst, level)

end

local function OnKill(inst, data) 
	if data.victim and not data.victim:HasTag("insect") 
	and not data.victim:HasTag("smallcreature") and not data.victim:HasTag("bird")
	and not data.victim:HasTag("soulless") then
	inst.components.health:DoDelta((MOB_HEALTH[inst.level] * 0.0125), false)	
	end
end
--==============================================
--				Custom Common Functions
--==============================================	
local function OnDeath(inst)

end

----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end
--==============================================
--					Loading/Saving
--==============================================
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
end

local function OnPreload(inst, data)

end
--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("monster")
    inst:AddTag("flying")
    inst:AddTag("shadowchesspiece")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
	inst:WatchWorldState("issummer", function() PlayablePets.SetSandstormImmunity(inst) end) --makes immune to Sandstorm visual issues.
	
	PlayablePets.SetSandstormImmunity(inst)
	
end

local master_postinit = function(inst)  
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0, 9999) --fire, acid, poison
	inst.components.health:StartRegen(10, 25)
	----------------------------------
	--Variables		
	inst.poisonimmune = true
	inst.debuffimmune = true
	inst.acidimmune = true
	inst.inkimmune = true
	inst.taunt = true
	inst.killcount = 0
	inst.should_taunt = 0
	inst.ghostbuild = "ghost_monster_build"
	
	inst.level = 3
   	inst.levelupsource = {}
    inst.sounds = sounds
	inst.LevelUp = levelup
	
	local suffix = tostring(inst.level - 1)
    inst.AnimState:OverrideSymbol("arm",       "shadow_knight_upg_build", "arm"..suffix)
    inst.AnimState:OverrideSymbol("ear",       "shadow_knight_upg_build", "ear"..suffix)
    inst.AnimState:OverrideSymbol("face",      "shadow_knight_upg_build", "face"..suffix)
    inst.AnimState:OverrideSymbol("head",      "shadow_knight_upg_build", "head"..suffix)
    inst.AnimState:OverrideSymbol("leg_low",   "shadow_knight_upg_build", "leg_low"..suffix)
    inst.AnimState:OverrideSymbol("neck",      "shadow_knight_upg_build", "neck"..suffix)
    inst.AnimState:OverrideSymbol("spring",    "shadow_knight_upg_build", "spring"..suffix)
	
	--local body_symbol = "swap_fire"
	--inst.poisonsymbol = body_symbol
	--MakeLargeBurnableCharacter(inst, body_symbol)
    --MakeLargeFreezableCharacter(inst)
	--inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	inst:AddComponent("lootdropper")
	inst.components.lootdropper:SetLootSetupFn(lootsetfn)
	
	inst:AddComponent("epicscare")
    inst.components.epicscare:SetRange(TUNING.SHADOW_CHESSPIECE_EPICSCARE_RANGE)	
	----------------------------------
	--Eater--
	inst.components.eater:SetAbsorptionModifiers(2,2,2) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 10, .5)
	RemovePhysicsColliders(inst)
	PlayablePets.SetAmphibious(inst, nil, nil, true, nil, true)
	inst.Physics:CollidesWith(COLLISION.SANITY)
	--MakeGhostPhysics(inst, 1, .5) --Special physics for flying characters(?)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
    inst.DynamicShadow:SetSize(1.5, .5)
	inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("killed", OnKill)
	inst:ListenForEvent("death", OnDeath)
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) 
		PlayablePets.SetSkin(inst, mob) 
		inst.AnimState:SetMultColour(1, 1, 1, 0.5 + (0.12*(inst.level-1)))
		inst.AnimState:SetFinalOffset(1)
	end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	inst.OnPreload = OnPreload
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
