local assets =
{
    Asset("ANIM", "anim/lavaarena_firestaff_meteor.zip"),
}

local assets_splash =
{
    Asset("ANIM", "anim/lavaarena_fire_fx.zip"),
}

local prefabs =
{
    "klaus_fire_meteorp_splash",
}

local prefabs_splash =
{
    "klaus_fire_meteorp_splashbase",
    "klaus_fire_meteorp_splashhit",
}

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return  {"magicimmune", "INLIMBO", "notarget", "playerghost"}
	elseif PP_FORGE_ENABLED then
		return {"player", "companion", "INLIMBO", "notarget", "magicimmune"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "wall", "playerghost", "magicimmune"}
	end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("lavaarena_firestaff_meteor")
    inst.AnimState:SetBuild("lavaarena_firestaff_meteor")
    inst.AnimState:PlayAnimation("crash")
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")
    inst:AddTag("notarget")

    inst.entity:SetPristine()
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:DoTaskInTime(0, function(inst)
		if inst.ischarged then
			inst.Transform:SetScale(1*inst.ischarged, 1*inst.ischarged, 1*inst.ischarged)
		end
	end)	
	
    inst:ListenForEvent("animover", function(inst)
		inst:DoTaskInTime(FRAMES*3, function(inst)
			SpawnPrefab("klaus_fire_meteorp_splash").Transform:SetPosition(inst:GetPosition():Get())
			local found_mobs = {}
			local x, y, z = inst:GetPosition():Get()
			local ents = TheSim:FindEntities(x, 0, z, inst.ischarged and 4*inst.ischarged or 4, nil, GetExcludeTags(inst))
			for _,ent in ipairs(ents) do
				if ent.entity:IsValid() and ent.entity:IsVisible() and ent.components.combat and ent.components.health and not ent.components.health:IsDead() and not (inst.master and ent == inst.master) then
					local firescale = ent.components.health.fire_damage_scale or nil
					local pos = ent:GetPosition():Get()
					local hitfx = SpawnPrefab("klaus_fire_meteorp_splashhit")
					hitfx.SetTarget(inst, ent)
					if inst.ischarged then
						hitfx.ischarged = inst.ischarged or nil
						hitfx.Transform:SetScale(1*inst.ischarged, 1*inst.ischarged, 1*inst.ischarged)
					end
					ent.components.combat:GetAttacked(inst.master and inst.master or inst,(firescale and firescale > 1) and 250*firescale or 250, nil, "explosive")
				end
			end
			inst:Remove()
		end)
	end)

    return inst
end

local function splashfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("lavaarena_fire_fx")
    inst.AnimState:SetBuild("lavaarena_fire_fx")
    inst.AnimState:PlayAnimation("firestaff_ult")
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.AnimState:SetFinalOffset(1)

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.persists = false

	inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/meteor_strike")
	inst:DoTaskInTime(0, function(inst)
	local base = SpawnPrefab("klaus_fire_meteorp_splashbase")
	if inst.ischarged then
		base.Transform:SetScale(1*inst.ischarged, 1*inst.ischarged, 1*inst.ischarged)
	end
	base.Transform:SetPosition(inst:GetPosition():Get())
	end)
	
	inst:ListenForEvent("animover", inst.Remove)
	

    return inst
end

local function splashbasefn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("lavaarena_fire_fx")
    inst.AnimState:SetBuild("lavaarena_fire_fx")
    inst.AnimState:PlayAnimation("firestaff_ult_projection")
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.persists = false
	
	inst:ListenForEvent("animover", inst.Remove)

    return inst
end

local function splashhitfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("lavaarena_fire_fx")
    inst.AnimState:SetBuild("lavaarena_fire_fx")
    inst.AnimState:PlayAnimation("firestaff_ult_hit")
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.AnimState:SetFinalOffset(1)
    inst.AnimState:SetScale(.5, .5)

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.persists = false
	
	inst.SetTarget = function(inst, target)
		inst.Transform:SetPosition(target:GetPosition():Get())
		local scale = target:HasTag("minion") and .5 or (target:HasTag("largecreature") and 1.3 or .8)
		inst.AnimState:SetScale(scale, scale)
	end

    inst:ListenForEvent("animover", inst.Remove)

    return inst
end

return Prefab("klaus_fire_meteorp", fn, assets, prefabs),
    Prefab("klaus_fire_meteorp_splash", splashfn, assets_splash, prefabs_splash),
    Prefab("klaus_fire_meteorp_splashbase", splashbasefn, assets_splash),
    Prefab("klaus_fire_meteorp_splashhit", splashhitfn, assets_splash)
