local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "stalker_atriump"

local assets = 
{
	Asset("ANIM", "anim/stalker_atrium_shiny_build_02.zip"),
	
	Asset("ANIM", "anim/stalker_basic.zip"),
    Asset("ANIM", "anim/stalker_action.zip"),
    Asset("ANIM", "anim/stalker_atrium.zip"),
    Asset("ANIM", "anim/stalker_shadow_build.zip"),
    Asset("ANIM", "anim/stalker_atrium_build.zip"),
}



local prefabs = 
{	
	"fossil_piece",
    "fossilspike",
    "fossilspike2",
    "stalker_shield",
    "stalker_minion",
    "shadowchanneler",
    "mindcontroller",
    "nightmarefuel",
    "thurible",
    "armorskeleton",
    "skeletonhat",
    "flower_rose",
    "winter_ornament_boss_fuelweaver",
	"chesspiece_stalker_sketch",
}
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = BOSS_STATS and TUNING.STALKER_ATRIUM_HEALTH or 5500,
	hunger = 750,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 300,
	runspeed = TUNING.STALKER_SPEED + 1,
	walkspeed = TUNING.STALKER_SPEED + 1,
	damage = 120*2,
	hit_range = 6,
	attackperiod = 3,
	range = 6,
	bank = "stalker",
	build = "stalker_shadow_build",
	build2 = "stalker_atrium_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGstalker_atriump",
	minimap = "stalker_atriump.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('stalker_atriump',
{
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"fossil_piece_clean",  1.00},
    {"nightmarefuel",       1.00},
    {"nightmarefuel",       1.00},
	{"nightmarefuel",       1.00},
    {"nightmarefuel",       1.00},
	{"nightmarefuel",       1.00},
    {"nightmarefuel",       1.00},
    {"nightmarefuel",       0.50},
    {"nightmarefuel",       0.50},
})

local FORGE_STATS = PPNR_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================
local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return  {"playerghost", "shadowcreature", "shadow", "shadowminion", "INLIMBO", "bird", "stalkerminion", "notarget"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"playerghost", "shadowcreature", "shadow", "shadowminion", "INLIMBO", "bird", "stalkerminion", "notarget", "player", "companion"}
	else	
		return {"playerghost", "shadowcreature", "shadow", "shadowminion", "INLIMBO", "bird", "stalkerminion", "notarget"}
	end
end


local function SetSkinDefault(inst, data)
	if data then
		inst.AnimState:SetBuild("stalker_shadow_build")
		inst.AnimState:AddOverrideBuild(data.build)
	else
		inst.AnimState:SetBuild("stalker_shadow_build")
		inst.AnimState:AddOverrideBuild(mob.build2)
	end
end

local function OnDoneTalking(inst)
    if inst.talktask ~= nil then
        inst.talktask:Cancel()
        inst.talktask = nil
    end
    inst.SoundEmitter:KillSound("talk")
end

local function OnTalk(inst)
    OnDoneTalking(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/stalker/talk_LP", "talk")
    inst.talktask = inst:DoTaskInTime(1.5 + math.random() * .5, OnDoneTalking)
end

local PHASE2_HEALTH = .75

local function EnterPhase2Trigger(inst)
    --inst.components.timer:ResumeTimer("snare_cd")
    --inst:PushEvent("roar")
end

local SNARE_OVERLAP_MIN = 1
local SNARE_OVERLAP_MAX = 3
local function NoSnareOverlap(x, z, r)
    return #TheSim:FindEntities(x, 0, z, r or SNARE_OVERLAP_MIN, { "fossilspike" }) <= 0
end

--Hard limit target list size since casting does multiple passes it
local SNARE_MAX_TARGETS = 100
local SNARE_TAGS = { "_combat", "locomotor"}
local SNARE_YES_TAGS = {"monster", "hostile", "player", "character", "epic"}
local SNARE_NO_TAGS = { "flying", "ghost", "playerghost", "shadowcreature", "shadow", "shadowminion", "INLIMBO", "bird", "stalker", "stalkerminion" }
local function FindSnareTargets(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local targets = {}
    local priorityindex = 1
    local priorityindex2 = 1
    local ents = TheSim:FindEntities(x, y, z, 30, SNARE_TAGS, SNARE_NO_TAGS, SNARE_YES_TAGS)
    for i, v in ipairs(ents) do
        if not (v.components.health ~= nil and v.components.health:IsDead()) then
            if v:HasTag("player") then
                table.insert(targets, priorityindex, v)
                priorityindex = priorityindex + 1
                priorityindex2 = priorityindex2 + 1
            elseif v.components.combat:TargetIs(inst) then
                table.insert(targets, priorityindex2, v)
                priorityindex2 = priorityindex2 + 1
            else
                table.insert(targets, v)
            end
            if #targets >= SNARE_MAX_TARGETS then
                return targets
            end
        end
    end
    return #targets > 0 and targets or nil
end

local function FindPlayers(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 30, {"player"}, {"playerghost", "INLIMBO"})
    return #ents > 0 and ents or nil
end

local function UpdateStats(inst)
	local pals = FindPlayers()
	if pals ~= nil then
		local health_percent = inst.components.health:GetPercent()
		inst.components.health.maxhealth = 200 * pals
		
		inst.components.health:SetPercent(health_percent)	
	end
end

local function ChangeToCustomObstacle(inst)
    inst:RemoveEventCallback("animover", ChangeToCustomObstacle)
    local x, y, z = inst.Transform:GetWorldPosition()
    inst.Physics:Stop()
    --inst.Physics:SetMass(99999)
    --inst.Physics:SetCapsule(0.2, 2)
	MakeObstaclePhysics(inst, 1.5)
	--MakeGiantCharacterPhysics(inst, 1000, .75)
	inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
    --inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.ITEMS)
	inst.Physics:CollidesWith(COLLISION.GIANTS)
	inst.Physics:CollidesWith(COLLISION.WORLD)
	--inst.Physics:CollidesWith(COLLISION.PLAYERS)
    inst.Physics:CollidesWith(COLLISION.CHARACTERS)
    inst.Physics:Teleport(x, 0, z)
end

local function ChangeToObstacle(inst) --added to prevent crash when removing fossilspike's on listenforevent.
    inst:RemoveEventCallback("animover", ChangeToObstacle)
    local x, y, z = inst.Transform:GetWorldPosition()
    inst.Physics:Stop()
    inst.Physics:SetMass(0) 
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.ITEMS)
    inst.Physics:CollidesWith(COLLISION.CHARACTERS)
    inst.Physics:Teleport(x, 0, z)
end

local function SpawnSnare(inst, x, z, r, num, target)
    local vars = { 1, 2, 3, 4, 5, 6, 7 }
    local used = {}
    local queued = {}
    local count = 0
    local dtheta = PI * 2 / num
    local thetaoffset = math.random() * PI * 2
    local delaytoggle = 0
    local map = TheWorld.Map
    for theta = math.random() * dtheta, PI * 2, dtheta do
        local x1 = x + r * math.cos(theta)
        local z1 = z + r * math.sin(theta)
        if map:IsPassableAtPoint(x1, 0, z1) then
            local spike = SpawnPrefab("fossilspike")
			--spike:DoTaskInTime(1, function(inst) inst.Physics:CollidesWith(COLLISION.GIANTS) end)
			spike:RemoveEventCallback("animover", ChangeToObstacle)
			spike:ListenForEvent("animover", ChangeToCustomObstacle)
			spike:DoTaskInTime(2, ChangeToCustomObstacle)
            spike.Transform:SetPosition(x1, 0, z1)

            local delay = delaytoggle == 0 and 0 or .2 + delaytoggle * math.random() * .2
            delaytoggle = delaytoggle == 1 and -1 or 1

            local duration = GetRandomWithVariance(TUNING.STALKER_SNARE_TIME, TUNING.STALKER_SNARE_TIME_VARIANCE)

            local variation = table.remove(vars, math.random(#vars))
            table.insert(used, variation)
            if #used > 3 then
                table.insert(queued, table.remove(used, 1))
            end
            if #vars <= 0 then
                local swap = vars
                vars = queued
                queued = vars
            end

            spike:RestartSpike(delay, duration, variation)
            count = count + 1
        end
    end
    if count <= 0 then
        return false
    elseif target:IsValid() then
        target:PushEvent("snared", { attacker = inst })
    end
    return true
end

local function SpawnSnares(inst, targets)
    local count = 0
    local nextpass = {}
	--local x, y, z = inst.Transform:GetWorldPosition()
	--Note for future use: local ents = TheSim:FindEntities(cx, 0, cy, 8, musthavetags, canthavetags, musthaveoneoftags)
    --local ents = TheSim:FindEntities(x, y, z, 10, SNARE_TAGS, SNARE_NO_TAGS, SNARE_YES_TAGS)
    for i, v in ipairs(targets) do
        if v:IsValid() and v:IsNear(inst, 30) and v.prefab ~= "stalkerp" then
            local x, y, z = v.Transform:GetWorldPosition()
            local islarge = v:HasTag("largecreature") or v:HasTag("epic")
            local r = (v.Physics ~= nil and v.Physics:GetRadius() or 0) + (islarge and 1.5 or 1.5)
            local num = islarge and 12 or 6
            if NoSnareOverlap(x, z, r + SNARE_OVERLAP_MAX) then
                if SpawnSnare(inst, x, z, r, num, v) then
                    count = count + 1
                    if count >= TUNING.STALKER_MAX_SNARES then
                        return
                    end
                end
            else
                table.insert(nextpass, { x = x, z = z, r = r, n = num, inst = v })
            end
        end
    end
    if #nextpass <= 0 then
        return
    end
    for range = SNARE_OVERLAP_MAX - 1, SNARE_OVERLAP_MIN, -1 do
        local i = 1
        while i <= #nextpass do
            local v = nextpass[i]
            if NoSnareOverlap(v.x, v.z, v.r + range) then
                if SpawnSnare(inst, v.x, v.z, v.r, v.n, v.inst) then
                    count = count + 1
                    if count >= TUNING.STALKER_MAX_SNARES or #nextpass <= 1 then
                        return
                    end
                end
                table.remove(nextpass, i)
            else
                i = i + 1
            end
        end
    end
end
---------------------------------------
--------------------------------------------------------------------------

local CHANNELER_SPAWN_RADIUS = 8.7
local CHANNELER_SPAWN_PERIOD = 1

local function DoSpawnChanneler(inst)
    if inst.components.health:IsDead() then
        inst.channelertask = nil
        inst.channelerparams = nil
        return
    end

    local x = inst.channelerparams.x + CHANNELER_SPAWN_RADIUS * math.cos(inst.channelerparams.angle)
    local z = inst.channelerparams.z + CHANNELER_SPAWN_RADIUS * math.sin(inst.channelerparams.angle)
    if TheWorld.Map:IsAboveGroundAtPoint(x, 0, z) then
        local channeler = SpawnPrefab("shadowchannelerp")
        channeler.Transform:SetPosition(x, 0, z)
        channeler:ForceFacePoint(Vector3(inst.channelerparams.x, 0, inst.channelerparams.z))
        inst.components.commander:AddSoldier(channeler)
    end

    if inst.channelerparams.count > 1 then
        inst.channelerparams.angle = inst.channelerparams.angle + inst.channelerparams.delta
        inst.channelerparams.count = inst.channelerparams.count - 1
        inst.channelertask = inst:DoTaskInTime(CHANNELER_SPAWN_PERIOD, DoSpawnChanneler)
    else
        inst.channelertask = nil
        inst.channelerparams = nil
    end
end

local function SpawnChannelers(inst)
   -- ResetAbilityCooldown(inst, "channelers")
   --print("DEBUG: Starting to spawn channelers")

    local count = TUNING.STALKER_CHANNELERS_COUNT
    if count <= 0 or inst.channelertask ~= nil then
       return
    end

    local x, y, z = inst.Transform:GetWorldPosition()
    inst.channelerparams =
    {
        x = x,
        z = z,
        angle = math.random() * 2 * PI,
        delta = -2 * PI / count,
        count = count,
    }
    DoSpawnChanneler(inst)
end

local function DespawnChannelers(inst)
    if inst.channelertask ~= nil then
        inst.channelertask:Cancel()
        inst.channelertask = nil
        inst.channelerparams = nil
    end
    for i, v in ipairs(inst.components.commander:GetAllSoldiers()) do
        if not v.components.health:IsDead() then
            v.components.health:Kill()
        end
    end
end

local function nodmgshielded(inst, amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb)
    return inst.hasshield and amount <= 0 and not ignore_absorb
end

local function OnSoldiersChanged(inst)
    if inst.hasshield ~= (inst.components.commander:GetNumSoldiers() > 0) then
        inst.hasshield = not inst.hasshield
        if not inst.hasshield then
            --inst.components.timer:StopTimer("channelers_cd")
            --inst.components.timer:StartTimer("channelers_cd", TUNING.STALKER_CHANNELERS_CD)
        end
    end
end

--------------------------------------------------------------------------

local MINION_RADIUS = .3
local MINION_SPAWN_PERIOD = .75
local NUM_RINGS = 3
local RING_SIZE = 7.5 / NUM_RINGS
local RING_TOTAL = 1
for i = 2, NUM_RINGS do
    RING_TOTAL = RING_TOTAL + i * i
end

local function DoSpawnMinion(inst)
    local pt = table.remove(inst.minionpoints, math.random(#inst.minionpoints))
    local minion = SpawnPrefab("stalker_minion")
    minion.Transform:SetPosition(pt:Get())
    minion:ForceFacePoint(pt)
    minion:OnSpawnedBy(inst)
	--TODO add skin support for minions
	if TheNet:GetServerGameMode() == "lavaarena" then
		minion:AddTag("companion")
	end
    if #inst.minionpoints <= 0 then
        inst.miniontask:Cancel()
        inst.miniontask = nil
        inst.minionpoints = nil
    end
end

--count is specified on load only
local function SpawnMinions(inst, count)
    if count == nil then
        --ResetAbilityCooldown(inst, "minions")
        count = TUNING.STALKER_MINIONS_COUNT
    end

    if count <= 0 or inst.miniontask ~= nil then
        return
    end

    --SetMusicLevel(inst, 2)

    local stargate = inst
    local x, y, z = (stargate or inst).Transform:GetWorldPosition()
    local map = TheWorld.Map
    inst.minionpoints = {}
    for ring = 1, NUM_RINGS do
        local ringweight = ring * ring / RING_TOTAL
        local ringcount = math.floor(count * ringweight + .5)
        if ringcount > 0 then
            local delta = 2 * PI / ringcount
            local radius = ring * RING_SIZE
            for i = 1, ringcount do
                local angle = delta * i
                local x1 = x + radius * math.cos(angle) + math.random() - .5
                local z1 = z + radius * math.sin(angle) + math.random() - .5
                if map:IsAboveGroundAtPoint(x1, 0, z1) then
                    table.insert(inst.minionpoints, Vector3(x1, 0, z1))
                end
            end
        end
    end
    if #inst.minionpoints > 0 then
        inst.miniontask = inst:DoPeriodicTask(MINION_SPAWN_PERIOD, DoSpawnMinion, 0)
    else
        inst.minionpoints = nil
    end
end

local function FindMinions(inst, proximity)
    local x, y, z = inst.Transform:GetWorldPosition()
    return TheSim:FindEntities(x, y, z, MINION_RADIUS + inst.Physics:GetRadius() + (proximity or .5), { "stalkerminion" }, { "NOCLICK" })
end

local STALKER_HEALINGS = TheNet:GetServerGameMode() == "lavaarena" and 200/6 or 200

local function EatMinions(inst)
    local minions = FindMinions(inst)
    local num = math.min(3, #minions)
    for i = 1, num do
        minions[i]:PushEvent("stalkerconsumed")
    end
    if not inst.components.health:IsDead() then
        inst.components.health:DoDelta(STALKER_HEALINGS * num)
    end
    return num
end

local function OnMinionDeath(inst)
    --inst.components.timer:StopTimer("minions_cd")
    --inst.components.timer:StartTimer("minions_cd", TUNING.STALKER_MINIONS_CD)
end

--------------------------------------------------------------------------
--X attack--
--code by Electroely.

local CHAIN_MAX_TARGETS = TheNet:GetServerGameMode() == "lavaarena" and 4 or 7
local CHAIN_TAGS = { "_combat", "locomotor"}
local CHAIN_YES_TAGS = {"monster", "hostile", "character", "epic"}
local CHAIN_NO_TAGS = { "playerghost", "shadowcreature", "shadow", "shadowminion", "INLIMBO", "bird", "stalkerminion" }

local function FindChainTargets(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local targets = {}
    local priorityindex = 1
    local priorityindex2 = 1
    local ents = TheSim:FindEntities(x, y, z, 30, CHAIN_TAGS, GetExcludeTagsp(inst), CHAIN_YES_TAGS)
    for i, v in ipairs(ents) do
        if not (v.components.health ~= nil and v.components.health:IsDead()) then
            if v:HasTag("player") then
                table.insert(targets, priorityindex, v)
                priorityindex = priorityindex + 1
                priorityindex2 = priorityindex2 + 1
            elseif v.components.combat:TargetIs(inst) then
                table.insert(targets, priorityindex2, v)
                priorityindex2 = priorityindex2 + 1
            else
                table.insert(targets, v)
            end
            if #targets >= CHAIN_MAX_TARGETS then
                return targets
           end
        end
    end
    return #targets > 0 and targets or nil
end

local function GetDistance(xone, xtwo, zone, ztwo)
	if xone ~= nil and xtwo ~= nil and zone ~= nil and ztwo ~= nil then
		local xtwominusxone = xtwo - xone
		local ztwominuszone = ztwo - zone
		local xdifferencesquared = xtwominusxone * xtwominusxone
		local zdifferencesquared = ztwominuszone * ztwominuszone
		local xandzsquareddifferencesaddedtogether = xdifferencesquared + zdifferencesquared
		--Electroely: don't even ask
		local thedistance = math.sqrt(xandzsquareddifferencesaddedtogether)
		local thetruedistance = math.abs(thedistance)
		return(thetruedistance)
	end
end

local CHAIN_DAMAGE = TheNet:GetServerGameMode() == "lavaarena" and 25 or 50

local function SpawnChain(inst, target)
    local targetx, targety, targetz = target.Transform:GetWorldPosition()
    local instx, targety, instz = inst.Transform:GetWorldPosition()
    local distance = GetDistance(targetx, instx, targetz, instz);
    local twowallsperdistance = distance / 2
    local numberofbones = math.ceil(twowallsperdistance)
    local distancexbetweenbones = (instx - targetx) / twowallsperdistance
    local distancezbetweenbones = (instz - targetz) / twowallsperdistance
    local delay = 0
    local bonexpos = instx
    local bonezpos = instz
    local bonexposoffset = nil
    local bonezposoffset = nil
    for i = 1, numberofbones, 1 do
            inst:DoTaskInTime(delay, function(inst)
			        if target ~= nil then 
                    targetx, targety, targetz = target.Transform:GetWorldPosition()
                    instx, targety, instz = inst.Transform:GetWorldPosition()
					if distance ~= nil and targetx ~= nil and targetz ~= nil then
                    distance = GetDistance(targetx, instx, targetz, instz)
                    twowallsperdistance = distance / 2
                    if i == numberofbones then
						if targetx ~= nil and targetz ~= nil then
                        bonexposoffset = targetx - bonexpos
                        bonezposoffset = targetz - bonezpos
                        bonexpos = bonexpos + bonexposoffset
                        bonezpos = bonezpos + bonezposoffset
                        local potato = SpawnPrefab("fossilspike2_p")
						potato.owner = inst
						--TODO add skin support
						potato.components.combat:SetDefaultDamage(CHAIN_DAMAGE * 2)
                        potato.Transform:SetPosition(bonexpos, 0, bonezpos)
                        potato.scale = 2
						potato.Transform:SetScale(2, 2, 2)
						end
                    elseif i == 1 then
	    				if target ~= nil then
                        distancexbetweenbones = (instx - targetx) / twowallsperdistance
                        distancezbetweenbones = (instz - targetz) / twowallsperdistance
                        bonexpos = bonexpos - distancexbetweenbones
                        bonezpos = bonezpos - distancezbetweenbones
                        local potato = SpawnPrefab("fossilspike2_p")
						--[[
						if inst.isshiny ~= 0 and inst.isshiny ~= 2 then
							potato.isshiny = inst.isshiny
							potato.AnimState:SetBuild("fossil_spike2_shiny_build_0"..inst.isshiny)
						end]]
						potato.components.combat:SetDefaultDamage(CHAIN_DAMAGE)
                        potato.Transform:SetPosition(bonexpos, 0, bonezpos)
						end
				    else
					if target ~= nil then
						if targetx ~= nil and targetz ~= nil then
						distance = GetDistance(targetx, bonexpos, targetz, bonezpos);
						twowallsperdistance = distance / 2
                        distancexbetweenbones = (bonexpos - targetx) / twowallsperdistance
                        distancezbetweenbones = (bonezpos - targetz) / twowallsperdistance
                        bonexpos = bonexpos - distancexbetweenbones
                        bonezpos = bonezpos - distancezbetweenbones
                        local potato = SpawnPrefab("fossilspike2_p")
						--[[
						if inst.isshiny ~= 0 and inst.isshiny ~= 2 then
							potato.isshiny = inst.isshiny
							potato.AnimState:SetBuild("fossil_spike2_shiny_build_0"..inst.isshiny)
						end]]
						potato.components.combat:SetDefaultDamage(CHAIN_DAMAGE)
                        potato.Transform:SetPosition(bonexpos, 0, bonezpos)
						end
						end
                    end
					end
				end
            end)
        delay = delay + 0.1
    end
	return true
end

local function SpawnChains(inst, targets)
    local count = 0
    local nextpass = {}
	if targets and #targets > 0 then
		for i, v in ipairs(targets) do
			if v:IsValid() and v:IsNear(inst, 25) then
				local x, y, z = v.Transform:GetWorldPosition()
				local islarge = v:HasTag("largecreature") or v:HasTag("epic")
				local r = (v.Physics ~= nil and v.Physics:GetRadius() or 0) + (islarge and 1.5 or 1.5)
				local num = islarge and 12 or 6
				-- if v ~= nil then
					if SpawnChain(inst, v) then
						count = count + 1
						if count >= 900 then
							return
						end
					end
				--else
					--table.insert(nextpass, { x = x, z = z, r = r, n = num, inst = v })
				--end
			end
		end
		if #nextpass <= 0 then
			return
		end

		for range = SNARE_OVERLAP_MAX - 1, SNARE_OVERLAP_MIN, -1 do
			local i = 1
			while i <= #nextpass do
				local v = nextpass[i]
				if NoSnareOverlap(v.x, v.z, v.r + range) then
					if SpawnChain(inst, v.inst) then
						count = count + 1
						if count >= TUNING.STALKER_MAX_SNARES or #nextpass <= 1 then
							return
						end
					end
					table.remove(nextpass, i)
				else
					i = i + 1
				end
			end
		end
	end
end

--Old X attack
--[[
local function DoSpawnSpikes(inst, pts, level, cache)
    if not inst.components.health:IsDead() then
        for i, v in ipairs(pts) do
            local variation = table.remove(cache.vars, math.random(#cache.vars))
            table.insert(cache.used, variation)
            if #cache.used > 3 then
                table.insert(cache.queued, table.remove(cache.used, 1))
            end
            if #cache.vars <= 0 then
                local swap = cache.vars
                cache.vars = cache.queued
                cache.queued = swap
            end

            local spike = SpawnPrefab("fossilspike2")
            spike.Transform:SetPosition(v:Get())
            spike:RestartSpike(0, variation, level)
        end
    end
end

local function GenerateSpiralSpikes(inst)
    local spawnpoints = {}
    local source = inst
    local x, y, z = source.Transform:GetWorldPosition()
    local spacing = 1.7
    local radius = 2
    local deltaradius = .2 
    local angle = 2 * PI * math.random()
    local deltaanglemult = (inst.reversespikes and -2 or 2) * PI * spacing
    inst.reversespikes = not inst.reversespikes
    local delay = 0
    local deltadelay = 2 * FRAMES
    local num = 70
    local map = TheWorld.Map
    for i = 1, num do
        local oldradius = radius
        radius = radius + deltaradius
        local circ = PI * (oldradius + radius)
        local deltaangle = deltaanglemult / circ
        angle = angle + deltaangle
        local x1 = x + radius * math.cos(angle)
        local z1 = z + radius * math.sin(angle)
        if map:IsPassableAtPoint(x1, 0, z1) then
            table.insert(spawnpoints, {
                t = delay,
                level = i / num,
                pts = { Vector3(x1, 0, z1) },
            })
            delay = delay + deltadelay
        end
    end
    return spawnpoints, source
end

local function PlayFlameSound(inst, source)
    source.SoundEmitter:PlaySound("dontstarve/creatures/together/stalker/flame")
end

local function SpawnSpikes(inst)
    --ResetAbilityCooldown(inst, "spikes")

    local spikes, source = GenerateSpiralSpikes(inst)
    if #spikes > 0 then
        local cache =
        {
            vars = { 1, 2, 3, 4, 5, 6, 7 },
            used = {},
            queued = {},
        }
        local flames = {}
        local flameperiod = .8
        for i, v in ipairs(spikes) do
            flames[math.floor(v.t / flameperiod)] = true
            inst:DoTaskInTime(v.t, DoSpawnSpikes, v.pts, v.level, cache)
        end
        if source ~= nil and source.SoundEmitter ~= nil then
            for k, v in pairs(flames) do
                inst:DoTaskInTime(k, PlayFlameSound, source)
            end
        end

        --DelaySharedAbilityCooldown(inst, "spikes")
    end
end
]]

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.acidimmune = true
	inst.mobsleep = false
	inst.altattack = false
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 2
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
--------------------------------------------------

local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function OnDestroyOther(inst, other)
	if other ~= nil and other:HasTag("wall") then
		local isname = inst:GetDisplayName()
		local selfpos = inst:GetPosition()
		print("LOGGED: "..isname.." destroyed a wall at "..selfpos.x..","..selfpos.y..","..selfpos.z.." as "..inst.prefab)
	end	
    if other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.DIG and
        other.components.workable.action ~= ACTIONS.NET and other.prefab ~= "fossil_stalker" and
        not inst.recentlycharged[other] then
        SpawnPrefab("collapse_small").Transform:SetPosition(other.Transform:GetWorldPosition())
        other.components.workable:Destroy(inst)
        if other:IsValid() and other.components.workable ~= nil and other.components.workable:CanBeWorked() then
            inst.recentlycharged[other] = true
            inst:DoTaskInTime(3, ClearRecentlyCharged, other)
        end
    end
end

local function OnCollide(inst, other)
    if other ~= nil and (other:HasTag("boulder") or other:HasTag("tree")) and
        other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.DIG and
        other.components.workable.action ~= ACTIONS.NET and
        not inst.recentlycharged[other] then
        inst:DoTaskInTime(2 * FRAMES, OnDestroyOther, other)
    end
end

local wilton_sayings =
{
		"A pity.",
        "You will not suffer.",
        "It must be this way.",
        "My world... threadbare.",
        "My city... in tatters...",
        "This will be quick.",
        "How we've fallen.",
		"You will fall as we did.",
        "This is for the best.",
        "You will be unraveled.",
        "They are coming. It cannot be stopped.",
        "This world's fabric is frayed and torn.",
        "They are unfathomable.",
        "I will save you.",
}

local function test_talk(inst)
    return wilton_sayings[math.random(#wilton_sayings)]
end
--==============================================
--				Custom Common Functions
--==============================================	

----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.components.combat.areahitrange = nil
	inst.components.combat.areahitcheck = nil
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("epic")
	inst:AddTag("stalker")
	inst:AddTag("monster")
	inst:AddTag("shadowancient")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)	
	
	inst.components.talker.fontsize = 40
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.colour = Vector3(238 / 255, 69 / 255, 105 / 255)
    inst.components.talker.offset = Vector3(0, -700, 0)
    inst.components.talker.symbol = "fossil_chest"
	--inst.components.talker.mod_str_fn = test_talk
	
	inst:WatchWorldState("issummer", function() PlayablePets.SetSandstormImmunity(inst) end) --makes immune to Sandstorm visual issues.
	
	PlayablePets.SetSandstormImmunity(inst)	
end

local master_postinit = function(inst)  
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0, 9999) --fire, acid, poison
	inst.components.combat:SetAreaDamage(4, TUNING.STALKER_AOE_SCALE)
	inst.components.combat:SetHurtSound("dontstarve/creatures/together/stalker/hit")
	----------------------------------
	--Variables		
	inst.taunt = true
	inst.taunt2 = true
    inst.taunt3 = true
	inst.mobsleep = true --doesn't sleep, uses taunt state instead.
	inst.altattack = true
	inst.acidimmune = true
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	inst.canspawnminions = true
	inst.canspike = true
	inst.canmindcontrol = true
	inst.canspawnchannelers = true
	
	
	inst.hit_recovery = 5
	inst.mobplayer = true
	inst.poisonimmune = true
	inst.ghostbuild = "ghost_monster_build"
	
	inst.AnimState:AddOverrideBuild("stalker_atrium_build")
	
	local body_symbol = "fossil_chest"
	inst.poisonsymbol = body_symbol
	--MakeLargeBurnableCharacter(inst, body_symbol)
    --MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.recentlycharged = {}
    inst.Physics:SetCollisionCallback(OnCollide)
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	
	inst:AddComponent("commander")
	
	inst:AddComponent("epicscare")
    inst.components.epicscare:SetRange(TUNING.STALKER_EPICSCARE_RANGE)
	
	--inst.SetEngaged = SetEngaged
    inst.FindSnareTargets = FindSnareTargets
    inst.SpawnSnares = SpawnSnares
	inst.components.health.redirect = nodmgshielded

    inst.SpawnChannelers = SpawnChannelers
    inst.DespawnChannelers = DespawnChannelers
    inst.SpawnMinions = SpawnMinions
    inst.FindMinions = FindMinions
    inst.EatMinions = EatMinions
	inst.SpawnChains = SpawnChains
	inst.FindChainTargets = FindChainTargets
    --inst.HasMindControlTarget = HasMindControlTarget
    --inst.MindControl = MindControl

    inst.hasshield = false
    inst:ListenForEvent("soldierschanged", OnSoldiersChanged)
    inst:ListenForEvent("miniondeath", OnMinionDeath)
	
	inst.components.talker.fontsize = 40
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.colour = Vector3(238 / 255, 69 / 255, 105 / 255)
    inst.components.talker.offset = Vector3(0, -700, 0)
    inst.components.talker.symbol = "fossil_chest"
	--inst.components.talker.mod_str_fn = test_talk
    --inst.components.talker:MakeChatter()
	
	--inst.components.talker.ontalk = OnTalk
	inst:ListenForEvent("ontalk", OnTalk)
    inst:ListenForEvent("donetalking", OnDoneTalking)
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1.5) --This might multiply food stats.
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1000, .75)
    inst.DynamicShadow:SetSize(4, 2)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	inst.components.talker:StopIgnoringAll()
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) inst.components.talker:StopIgnoringAll() end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
