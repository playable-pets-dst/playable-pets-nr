local MakePlayerCharacter = require "prefabs/player_common"


local assets = 
{
	Asset("ANIM", "anim/kittington_shiny_build_01.zip"),
	Asset("ANIM", "anim/catcoon_shiny_build_01.zip"),
	Asset("ANIM", "anim/catcoon_shiny_build_02.zip"),
	
}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = 60,
	hunger = 75,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 100,
	runspeed = TUNING.CRITTER_WALK_SPEED,
	walkspeed = TUNING.CRITTER_WALK_SPEED,
	damage = 20,
	range = 1,
	hit_range = 2,
	attackperiod = 0,
	bank = "kittington", 
	build = "kittington_build",
	scale = 1,
	shiny = "kittington",
	--build2 = "alternate build here",
	stategraph = "SGbabycatp",
	minimap = "babycatp.tex",
	
}

local mob_adult = 
{
	health = TUNING.CATCOON_LIFE * 1.2, --health bonus for growing
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 200,
	runspeed = TUNING.CATCOON_SPEED,
	walkspeed = TUNING.CATCOON_SPEED,
	damage = TUNING.CATCOON_DAMAGE * 1.2, --damage bonus for growing
	range = 3,
	hit_range = 4,
	attackperiod = 0,
	bank = "catcoon", 
	build = "catcoon_build",
	scale = 1,
	shiny = "catcoon",
	--build2 = "alternate build here",
	stategraph = "SGcatp",
	minimap = "babycatp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('babycatp',
-----Prefab---------------------Chance------------
{
    {'smallmeat',			1.00},
})

SetSharedLootTable('bigcatp',
-----Prefab---------------------Chance------------
{
    {'meat',				1.00},
	{'coontail',			0.50},
})

-------------------------------------------------------------------
--Catcoon functions--
local neutralGiftPrefabs =
{
	{ --tier 1
		"wetgoop",
	},
	{ --tier 2
		"spoiled_food",
		"wetgoop",
	},
	{ --tier 3
		"cutgrass",
		"spoiled_food",
	},
	{ --tier 4
		"cutgrass",
		"spoiled_food",
	},
	{ --tier 5
		"cutgrass",
		"rocks",
		"petals_evil",
	},
	{ --tier 6
		"rocks",
		"flint",
		"petals",
	},
	{ --tier 7
		"ice",
		"flint",
		"pinecone",
	},
	{ --tier 8
		"flint",
		"pinecone",
		"feather_robin",
	},
	{ --tier 9
		"mole",
		"acorn",
	}
}

local friendGiftPrefabs =
{
	{ -- tier 1 (basic seeds)
		"carrot_seeds",
		"corn_seeds",
	},
	{ -- tier 2 (basic, generic stuff)
		"flint",
		"cutgrass",
		"twigs",
		"rocks",
		"ash",
		"pinecone",
		"petals",
		"petals_evil",
	},
	{ -- tier 3 (non-food animal bits)
		"feather_robin",
		"feather_robin_winter",
		"feather_crow",
		"boneshard",
	},
	{ -- tier 4 (better seeds)
		"pumpkin_seeds",
		"eggplant_seeds",
		"durian_seeds",
		"pomegranate_seeds",
		"dragonfruit_seeds",
		"watermelon_seeds",
	},
	{ --tier 5 (food)
		"ice",
		"batwing",
		"acorn",
		"berries",
		"smallmeat",
		"red_cap",
		"blue_cap",
		"green_cap",
		"fish",
		"froglegs",
	},
	{ --tier 6 (live animals + tumbleweed)
		"mole",
		"rabbit",
		"bee",
		"butterfly",
		"robin",
		"robin_winter",
		"crow",
		"tumbleweed",
	},
	{ -- tier 7 (good generic stuff)
		"goldnugget",
		"silk",
		"cutreeds",
		"tentaclespots",
		"beefalowool",
		"transistor",
	},
}

local function onupdate(inst, dt)
    inst.charge_time = inst.charge_time - dt
    if inst.charge_time <= 0 then
        inst.charge_time = 0
		if inst.charge_task ~= nil then
            inst.charge_task:Cancel()
            inst.charge_task = nil
        end
        inst.specialatk2 = true
		inst.sg:GoToState("ready")
    else
    --    
    end
end

local function OnLongUpdate(inst, dt)
    inst.charge_time = math.max(0, inst.charge_time - dt)
end

local function StartTimer(inst, duration)
    inst.charge_time = duration
	inst.specialatk = false

    if inst.charge_task == nil then
        inst.charge_task = inst:DoPeriodicTask(1, onupdate, nil, 1)
        onupdate(inst, 0)
    end
end

local function PickRandomGift(inst, tier)
	local table = friendGiftPrefabs
	if tier > #table then tier = #table end
	return GetRandomItem(table[tier])
end

local function ShouldAcceptItem(inst, item)
	if item:HasTag("cattoy") or item:HasTag("catfood") or item:HasTag("cattoyairborne") then
		return true
	else
		return false
	end
end

local function OnGetItemFromPlayer(inst, item) 
        --giver:PushEvent("makefriend") --commenting this out because it could be used for something later.
        inst.last_hairball_time = GetTime()
        inst.hairball_friend_interval = math.random(2,4) -- Jumpstart the hairball timer (slot machine time!)
end

local function OnRefuseItem(inst, item) -- this might go unused
	inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/catcoon/hiss_pre")
	if inst.components.sleeper:IsAsleep() then
        inst.components.sleeper:WakeUp()
    -- elseif not inst.sg:HasStateTag("busy") then 
    -- 	inst.sg:GoToState("hiss")
    end
end
------------------------------------------------------

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("catcoon") and not dude.components.health:IsDead() end, 10)
    end
end

local function onhitother2(inst, other)
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("catcoon") and not dude.components.health:IsDead() end, 10)
end
-------------------------------------------------------
--Growth Code--

local function SetAdult(inst)
	inst.sg:GoToState("idle")

	PlayablePets.SetCommonStats(inst, mob_adult, nil, true) --mob table, ishuman, ignorepvpmultiplier
	inst.components.ppskin_manager:LoadSkin(mob_adult, true)
	PlayablePets.SetCommonWeatherResistances(inst, nil, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	inst.components.combat:SetHurtSound("dontstarve_DLC001/creatures/catcoon/hurt")
	
	MakeCharacterPhysics(inst, 10, .5)
    inst.DynamicShadow:SetSize(1.5, 0.5)
    inst.Transform:SetFourFaced()
	
	inst:AddTag("catcoonp")
	
	inst.isadult = true --added as a failsafe
	
	inst.components.lootdropper:SetChanceLootTable('bigcatp')
	inst.components.eater:SetAbsorptionModifiers(1,1.25,1)
	inst.components.eater.strongstomach = true -- can eat monster meat!
end

local function OnGrowUp(inst)
	inst.Transform:SetPosition(inst.Transform:GetWorldPosition())
	inst.isadult = true 
	inst.sg:GoToState("growup")
end

local function applyupgrades(inst)

    local health_percent = inst.components.health:GetPercent()
	local hunger_percent = inst.components.hunger:GetPercent()
	--local sanity_percent = inst.components.sanity:GetPercent()
	
	if inst.isadult == false then
	inst.components.health.maxhealth = mob.health
	inst.components.hunger.max = 75
	
	else
		inst.components.health.maxhealth = 300 --give bonus hp for grown up pups
		inst.components.hunger.max = 200	
	end
    inst.components.health:SetPercent(health_percent)
end

local ADULT_REQ = 45

local function OnEat(inst, food)
	local age = inst.components.age:GetAgeInDays()
    if food and food.components.edible then
		if food.components.edible.foodtype == FOODTYPE.MEAT and MOBGROW~= "Disable" and inst.catfood < 999 then
			--food helps you grow!
			if food.prefab == "fishsticks" then
			inst.catfood = inst.catfood + 5
			else
			inst.catfood = inst.catfood + 1
			end
			if inst.catfood >= ADULT_REQ and inst.isadult == false then
				--inst.sg:GoToState("transform")
				OnGrowUp(inst)
				applyupgrades(inst)
			end
		end
    end
end

local function ondeath(inst)
   inst.catfood = 0
   inst.isadult = false
   inst.components.combat:SetHurtSound(nil)
   applyupgrades(inst)
   
end
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.catfood = data.catfood or 0
		inst.isadult = data.isadult or false
		
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.catfood = inst.catfood > 0 and inst.catfood or nil
	data.isadult = inst.isadult or false
	
end

local function OnPreload(inst, data)
    if data ~= nil and data.isadult ~= nil then
        inst.isadult = data.isadult
        applyupgrades(inst)
        --re-set these from the save data, because of load-order clipping issues
        if data.health and data.health.health then inst.components.health:SetCurrentHealth(data.health.health) end
        if data.hunger and data.hunger.hunger then inst.components.hunger.current = data.hunger.hunger end
        --if data.sanity and data.sanity.current then inst.components.sanity.current = data.sanity.current end
        inst.components.health:DoDelta(0)
        inst.components.hunger:DoDelta(0)
        inst.components.sanity:DoDelta(0)
		if inst.isadult then
			SetAdult(inst)
		end
    end
end

local function GetMobTable(inst)
	if inst.catfood < ADULT_REQ then
		return mob
	elseif inst.catfood >= ADULT_REQ then
		return mob_adult
	end
end

local function SetSkinDefault(inst, data)
	if not inst:HasTag("playerghost") then
		if data then
			if inst.isadult ~= nil and inst.isadult == true then
				inst.AnimState:SetBuild(data.adult_build)
			else	
				inst.AnimState:SetBuild(data.build)			
			end
		else
			inst.AnimState:SetBuild(inst.isadult and mob_adult.build or mob.build)
		end	
	end
end

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end

local master_postinit = function(inst)  
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, nil, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('babycatp')
	----------------------------------
	--Tags--
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	inst.mobplayer = true
	
	inst:AddTag("catcoonp")
	
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	inst.ghostbuild = "ghost_monster_build"
	----------------------------------
	--Growth--
	inst.catfood = 0
	
	inst.isadult = false
	
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	inst.userfunctions =
    {
        SetAdult = SetAdult,
		StartTimer = StartTimer,	
    }
	
	inst.charge_task = nil
    inst.charge_time = 0

	inst.hairball_friend_interval = math.random(TUNING.MIN_HAIRBALL_FRIEND_INTERVAL, TUNING.MAX_HAIRBALL_FRIEND_INTERVAL)
    inst.hairball_neutral_interval = math.random(TUNING.MIN_HAIRBALL_NEUTRAL_INTERVAL, TUNING.MAX_HAIRBALL_NEUTRAL_INTERVAL)
	
	inst.neutralGiftPrefabs = neutralGiftPrefabs
	inst.friendGiftPrefabs = friendGiftPrefabs
	inst.PickRandomGift = PickRandomGift
	---------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,2,1) --This might multiply food stats.
    inst.components.eater:SetOnEatFn(OnEat) --Runs a function when the player eats something.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 1, .5)
    inst.DynamicShadow:SetSize(2, .75)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", onattacked)
	inst:ListenForEvent("death", ondeath)
	inst.components.combat.onhitotherfn = onhitother2
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    inst.OnPreLoad = OnPreload
  	------------------------------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst)) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
    inst:ListenForEvent("respawnfromghost", function() 
		inst:DoTaskInTime(5, function(inst) PlayablePets.SetCommonStats(inst, GetMobTable(inst), nil, true) end)
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst)) end)
		inst.Transform:SetSixFaced()
    end)
	
    return inst	
end

return MakePlayerCharacter("babycatp", prefabs, assets, common_postinit, master_postinit, start_inv)
