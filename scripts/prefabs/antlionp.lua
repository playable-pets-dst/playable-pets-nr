local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------

---------------------------
local prefabname = "antlionp"

local assets = 
{
	Asset("ANIM", "anim/antlion_build.zip"),
    Asset("ANIM", "anim/antlion_basic.zip"),
    Asset("ANIM", "anim/antlion_action.zip"),
    Asset("ANIM", "anim/sand_splash_fx.zip"),

	Asset("ANIM", "anim/antlion_shiny_build_01.zip"),
	Asset("ANIM", "anim/antlion_shiny_build_03.zip"),
	Asset("ANIM", "anim/antlion_shiny_build_04.zip"),
	Asset("ANIM", "anim/antlion_shiny_build_06.zip"),
}

local prefabs = 
{	
	"antlion_sinkhole",
    "townportal_blueprint",
    "townportaltalisman",
    "sandspike",
    "sandblock",

    --loot
    "meat",
    "rocks",
    "trinket_1",
    "trinket_3",
    "trinket_8",
    "trinket_9",
    "antliontrinket",
	"chesspiece_antlion_sketch",
}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = 6000,
	hunger = 450,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 150,
	runspeed = 7,
	walkspeed = 6,
	damage = 300,
	range = 15,
	hit_range = 6,
	attackperiod = 3,
	bank = "antlion",
	build = "antlion_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGantlionp",
	minimap = "antlionp.tex",
	
}

local FORGE_STATS = PPNR_FORGE[string.upper(prefabname)] 
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('antlionp',
-----Prefab---------------------Chance------------
{

    {'townportaltalisman',  1.00},
    {'townportaltalisman',  1.00},
    {'townportaltalisman',  1.00},
    {'townportaltalisman',  1.00},
    {'townportaltalisman',  1.00},
    {'townportaltalisman',  1.00},
    {'townportaltalisman',  0.50},
    {'townportaltalisman',  0.50},

    {'meat',                1.00},
    {'meat',                1.00},
    {'meat',                1.00},
    {'meat',                1.00},

    {'rocks',               1.00},
    {'rocks',               1.00},
    {'rocks',               0.50},
    {'rocks',               0.50},
   
})

local function OnEat(inst)
    inst.components.hunger:DoDelta(15, false)
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	
end
-------------------------------------------------------
local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	

	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.	
end

local master_postinit = function(inst)  
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.5) --fire, acid, poison, freeze (flat value, not a multiplier)
	PlayablePets.SetSandstormImmune(inst, true)
	inst.components.combat:SetHurtSound("dontstarve/creatures/together/antlion/hit")
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('antlionp')
	----------------------------------
	--Tags--
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.

	inst:AddTag("epic")
	inst:AddTag("antlion")
	
	inst.taunt = true
	inst._isunder = false
	inst.taunt2 = true
	inst.mobsleep = true
	
	inst.hitrecovery = 2
	inst.canunder = true
	inst.altattack = true
	inst.damage_memory = mob.damage
	
	inst.AnimState:OverrideSymbol("sand_splash", "sand_splash_fx", "sand_splash")
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.ELEMENTAL, FOODTYPE.MEAT }, { FOODTYPE.ELEMENTAL, FOODTYPE.MEAT}) 
    inst.components.eater:SetAbsorptionModifiers(1,2,2) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 1000, .5)
	inst.Physics:SetMass(99999)
	inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetTwoFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	------------------------------------------------------
	--Groundpound--
	inst:AddComponent("groundpounder")
	inst.components.groundpounder.destroyer = false
	inst.components.groundpounder.groundpounddamagemult = 0.5
	inst.components.groundpounder.damageRings = 0
	inst.components.groundpounder.destructionRings = 0
	inst.components.groundpounder.numRings = 3    	
  	------------------------------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, true, true) end)
    end)
	
    return inst
	
end

return MakePlayerCharacter("antlionp", prefabs, assets, common_postinit, master_postinit, start_inv)
