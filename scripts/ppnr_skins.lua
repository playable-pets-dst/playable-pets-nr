local SKINS = {
	--[[
	prefab = {
		skinname = {
			name = "", --this is for the purpose of printing
			fname = "Name of Skin",
			build = "build name",
			teen_build = "",
			adult_build = "adult build name",
			etc_build = "blah blah build", --add as many as these as you need
			fn = functionhere,
			owners = {}, --userids go here
			locked = true, --this skin can't be used.
		},
	},
	]]
	antlionp = {
		summer = {
			name = "summer",
			fname = "Summer Antlion",
			build = "antlion_shiny_build_01",
		},
		odd = {
			name = "odd",
			fname = "Odd Antlion",
			build = "antlion_shiny_build_03",
		},
		dark = {
			name = "dark",
			fname = "Dark Antlion",
			build = "antlion_shiny_build_04",
		},
		spring = {
			name = "spring",
			fname = "Spring Antlion",
			build = "antlion_shiny_build_06",
		},
	},
	stagehandp = {
		odd = {
			name = "odd",
			fname = "Odd Table",
			build = "stagehand_shiny_build_01",
		},
	},
	babycatp = {
		brown = {
			name = "brown",
			fname = "Brown Catcoon",
			build = "kittington_shiny_build_01",
			adult_build = "catcoon_shiny_build_01",
		},
	},
	babysheepp = {
		odd = {
			name = "odd",
			fname = "Odd Ewecus",
			build = "sheepington_shiny_build_01",
			adult_build = "spat_shiny_build_01",
		},
	},
	sheepp = {
		odd = {
			name = "odd",
			fname = "Odd Ewecus",
			build = "spat_shiny_build_01",
		},
	},
	gibletp = {
		odd = {
			name = "odd",
			fname = "Odd Gobbler",
			build = "perdling_shiny_build_01",
			adult_build = "perd_shiny_build_01",
		},
	},
	gibletp = {
		hf = {
			name = "hf",
			fname = "Screechlet",
			build = "waddler",
			--adult_build = "perd_shiny_build_01",
		},
	},
	babyglommerp = {
		pink = {
			name = "pink",
			fname = "Cotton Candy Glommer",
			build = "glomling_shiny_build_01",
			adult_build = "glommer_shiny_build_01",
		},
	},
	babydragonp = {
		red = {
			name = "red",
			fname = "Red Dragonfly",
			build = "dragonling_shiny_build_01",
			adult_build = "dragonfly_shiny_build_01",
			rage_build = "dragonfly_fire_shiny_build_01",
		},
	},
	klausp = {
		blue = {
			name = "blue",
			fname = "Blue Klaus",
			build = "klaus_shiny_build_01",
		},
		dark = {
			name = "dark",
			fname = "Dark Klaus",
			build = "klaus_shiny_build_03",
		},
	},
	beequeenp = {
		killer = {
			name = "killer",
			fname = "Killer Bee Queen",
			build = "bee_queen_shiny_build_01",
		},
	},
	beeguardp = {
		killer = {
			name = "killer",
			fname = "Killer Bee Guard",
			puffy_build = "bee_guard_puffy_shiny_build_01",
			build = "bee_guard_shiny_build_01",
		},
	},
	deerp = {
		odd = {
			name = "odd",
			fname = "Odd Deer",
			build = "deer_shiny_build_01",
		},
		moon = {
			name = "moon",
			fname = "Moon Deer",
			build = "deer_moon",
		},
		shadow = {
			name = "shadow",
			fname = "Shadow Deer",
			build = "deer_shadow",
		},
	},
	deer_redp = { --TODO: Enable overrides somehow so we don't just have multiples of the same skin data
		odd = {
			name = "odd",
			fname = "Odd Deer",
			build = "deer_shiny_build_01",
		},
		shadow = {
			name = "shadow",
			fname = "Shadow Deer",
			build = "deer_shadow",
		},
	},
	deer_bluep = {
		odd = {
			name = "odd",
			fname = "Odd Deer",
			build = "deer_shiny_build_01",
		},
		moon = {
			name = "moon",
			fname = "Moon Deer",
			build = "deer_moon",
		},
		ruins = {
			name = "ruins",
			fname = "Ruins Deer",
			build = "deer_ruins",
		},
	},
	toadstoolp = {
		odd = {
			name = "odd",
			fname = "Odd Toadstool",
			build = "toadstool_shiny_build_01",
			build2 = "toadstool_upg_shiny_build_01",
		},
	},
	toadstool_darkp = {
		odd = {
			name = "odd",
			fname = "Odd Toadstool",
			build = "toadstool_dark_shiny_build_01",
			build2 = "toadstool_dark_upg_shiny_build_01",
		},
	},
	stalkerp = {},
	stalker_atriump = {
		rainbow = {
			name = "rainbow",
			fname = "Rainbow Fuelweaver",
			build = "stalker_atrium_shiny_build_02",
			owners = {"KU_DBJnvvCy"},
		},
	},
	stalker_minionp = {
		alt = {
			name = "alt",
			fname = "Alternate Form",
			alt = true,
			build = "stalker_minion_2",
		},
	},
	stalker_forestp = {
		autumn = {
			name = "autumn",
			fname = "Autumn Skeleton",
			build = "stalker_forest_shiny_build_01",
		},
	},
	clayvargp = {
		white = {
			name = "white",
			fname = "White Clay Warg",
			build = "claywarg_shiny_build_01",
		},
		black = {
			name = "black",
			fname = "Black Clay Warg",
			build = "claywarg_shiny_build_03",
		},
	},
	clayhoundp = {
		white = {
			name = "white",
			fname = "White Clay Hound",
			build = "clayhound_shiny_build_01",
		},
		black = {
			name = "black",
			fname = "Black Clay Warg",
			build = "clayhound_shiny_build_03",
		},
	},
}

return SKINS