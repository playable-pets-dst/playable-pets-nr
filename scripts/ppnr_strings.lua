
------------------------------------------------------------------
-- Recipe strings
------------------------------------------------------------------

------------------------------------------------------------------
-- Character select screen strings
------------------------------------------------------------------
-----------Recipe Descs------------------------------------------
--STRINGS.RECIPE_DESC.PIGHOUSE_PLAYER = "A house for best pigs"
---------------------------------------------------------
--PostInits
STRINGS.CHARACTERS.STALKERP = STRINGS.CHARACTERS.GENERIC
STRINGS.CHARACTERS.STALKER_ATRIUMP = STRINGS.CHARACTERS.GENERIC
---------------------------------------------------------
-- The character select screen lines

STRINGS.CHARACTER_TITLES.stagehandp = "Stagehand"
STRINGS.CHARACTER_NAMES.stagehandp = "STAGEHAND"
STRINGS.CHARACTER_DESCRIPTIONS.stagehandp = "*Is a table?\n*Maybe a table?\n*Probably just a table"
STRINGS.CHARACTER_QUOTES.stagehandp = "A totally normal table."

STRINGS.CHARACTER_TITLES.stalker_forestp = "Reanimated Surface Skeleton"
STRINGS.CHARACTER_NAMES.stalker_forestp = "REANIMATED SURFACE SKELETON"
STRINGS.CHARACTER_DESCRIPTIONS.stalker_forestp = "*Is a Giant\n*Can perform base maintenance\n*Can't attack..."
STRINGS.CHARACTER_QUOTES.stalker_forestp = "The Gentle Giant?"

STRINGS.CHARACTER_TITLES.stalkerp = "Reanimated Skeleton"
STRINGS.CHARACTER_NAMES.stalkerp = "REANIMATED SKELETON"
STRINGS.CHARACTER_DESCRIPTIONS.stalkerp = "*Is a Giant\n*Can trap prey\n*Is mysterious..."
STRINGS.CHARACTER_QUOTES.stalkerp = "2SPOOKY4U"

STRINGS.CHARACTER_TITLES.stalker_atriump = "Ancient Fuelweaver"
STRINGS.CHARACTER_NAMES.stalker_atriump = "ANCIENT FUELWEAVER"
STRINGS.CHARACTER_DESCRIPTIONS.stalker_atriump = "*Is a Giant\n*Has quite a few powers\n*Is mysterious..."
STRINGS.CHARACTER_QUOTES.stalker_atriump = "He just wants to save you..."

STRINGS.CHARACTER_TITLES.stalker_minionp = "Woven Shadow"
STRINGS.CHARACTER_NAMES.stalker_minionp = "WOVEN SHADOW"
STRINGS.CHARACTER_DESCRIPTIONS.stalker_minionp = "*Is a harmless\n*Is creepy...\n*And adorable!"
STRINGS.CHARACTER_QUOTES.stalker_minionp = "snik"

STRINGS.CHARACTER_TITLES.bird2p = "Canary"
STRINGS.CHARACTER_NAMES.bird2p = "CANARY"
STRINGS.CHARACTER_DESCRIPTIONS.bird2p = "*Is a pretty bird\n*Can fly\n*Can't attack..."
STRINGS.CHARACTER_QUOTES.bird2p = "WHO'S A PRETTY BIRD?"

STRINGS.CHARACTER_TITLES.antlionp = "Antlion"
STRINGS.CHARACTER_NAMES.antlionp = "ANTLION"
STRINGS.CHARACTER_DESCRIPTIONS.antlionp = "*Is a Giant?\n*Is immune to sandstorms\n*Destructive"
STRINGS.CHARACTER_QUOTES.antlionp = "King of the Desert"

STRINGS.CHARACTER_TITLES.toadstoolp = "Toadstool"
STRINGS.CHARACTER_NAMES.toadstoolp = "TOADSTOOL"
STRINGS.CHARACTER_DESCRIPTIONS.toadstoolp = "*Is a Giant\n*Can spew deadly spores\n*Is very slow"
STRINGS.CHARACTER_QUOTES.toadstoolp = "Insert obvious high joke here."

STRINGS.CHARACTER_TITLES.toadstool_darkp = "Misery Toadstool"
STRINGS.CHARACTER_NAMES.toadstool_darkp = "MISERY TOADSTOOL"
STRINGS.CHARACTER_DESCRIPTIONS.toadstool_darkp = "*Is a Giant\n*Can spew special spores\n*Prefers spores over mushroombombs"
STRINGS.CHARACTER_QUOTES.toadstool_darkp = "Insert obvious high joke here."

STRINGS.CHARACTER_TITLES.shadow_knightp = "Shadow Knight"
STRINGS.CHARACTER_NAMES.shadow_knightp = "SHADOW KNIGHT"
STRINGS.CHARACTER_DESCRIPTIONS.shadow_knightp = "*Is a shadow\n*Can grow stronger with kills\n*Is scary"
STRINGS.CHARACTER_QUOTES.shadow_knightp = "..."

STRINGS.CHARACTER_TITLES.shadow_knight2p = "Shadow Knight Lv 3"
STRINGS.CHARACTER_NAMES.shadow_knight2p = "SHADOW KNIGHT LV 3"
STRINGS.CHARACTER_DESCRIPTIONS.shadow_knight2p = "*Is a shadow\n*Is a giant\n*Is scary"
STRINGS.CHARACTER_QUOTES.shadow_knight2p = "..."

STRINGS.CHARACTER_TITLES.shadow_bishopp = "Shadow Bishop"
STRINGS.CHARACTER_NAMES.shadow_bishopp = "SHADOW BISHOP"
STRINGS.CHARACTER_DESCRIPTIONS.shadow_bishopp = "*Is a shadow\n*Can grow stronger with kills\n*Is scary"
STRINGS.CHARACTER_QUOTES.shadow_bishopp = "..."

STRINGS.CHARACTER_TITLES.shadow_bishop2p = "Shadow Bishop Lv 3"
STRINGS.CHARACTER_NAMES.shadow_bishop2p = "SHADOW BISHOP LV 3"
STRINGS.CHARACTER_DESCRIPTIONS.shadow_bishop2p = "*Is a shadow\n*Is a Giant \n*Is scary"
STRINGS.CHARACTER_QUOTES.shadow_bishop2p = "..."

STRINGS.CHARACTER_TITLES.shadow_rookp = "Shadow Rook"
STRINGS.CHARACTER_NAMES.shadow_rookp = "SHADOW ROOK"
STRINGS.CHARACTER_DESCRIPTIONS.shadow_rookp = "*Is a shadow\n*Can grow stronger with kills\n*Is scary"
STRINGS.CHARACTER_QUOTES.shadow_rookp = "..."

STRINGS.CHARACTER_TITLES.shadow_rook2p = "Shadow Rook Lv 3"
STRINGS.CHARACTER_NAMES.shadow_rook2p = "SHADOW ROOK LV 3"
STRINGS.CHARACTER_DESCRIPTIONS.shadow_rook2p = "*Is a shadow\n*Is a giant\n*Is scary"
STRINGS.CHARACTER_QUOTES.shadow_rook2p = "..."

STRINGS.CHARACTER_TITLES.beequeenp = "Bee Queen"
STRINGS.CHARACTER_NAMES.beequeenp = "BEE QUEEN"
STRINGS.CHARACTER_DESCRIPTIONS.beequeenp = "*Is a Giant\n*Can spawn bee guards\n*Loved by all bees"
STRINGS.CHARACTER_QUOTES.beequeenp = "PEASANTS"

STRINGS.CHARACTER_TITLES.beeguardp = "Grumple Bee"
STRINGS.CHARACTER_NAMES.beeguardp = "GRUMPLE BEE"
STRINGS.CHARACTER_DESCRIPTIONS.beeguardp = "*Is a bee\n*Slightly better than your average bee\n*...what?"
STRINGS.CHARACTER_QUOTES.beeguardp = "LONG LIVE THE QUEEEEEEEEN!"

STRINGS.CHARACTER_TITLES.babyhoundp = "Vargling"
STRINGS.CHARACTER_NAMES.babyhoundp = "VARGLING"
STRINGS.CHARACTER_DESCRIPTIONS.babyhoundp = "*Is a baby\n*Can grow up\n*Pretty weak"
STRINGS.CHARACTER_QUOTES.babyhoundp = "Wish that would stay this cute when they grow up..."

STRINGS.CHARACTER_TITLES.babycatp = "Kittycoon"
STRINGS.CHARACTER_NAMES.babycatp = "KITTYCOON"
STRINGS.CHARACTER_DESCRIPTIONS.babycatp = "*Is a baby\n*Can grow up\n*Pretty weak"
STRINGS.CHARACTER_QUOTES.babycatp = "Cuteness is a deadly weapon."

STRINGS.CHARACTER_TITLES.gibletp = "Giblet"
STRINGS.CHARACTER_NAMES.gibletp = "GIBLET"
STRINGS.CHARACTER_DESCRIPTIONS.gibletp = "*Is a baby\n*Can grow up\n*Pretty weak"
STRINGS.CHARACTER_QUOTES.gibletp = "Adorably delicious!"

STRINGS.CHARACTER_TITLES.babyglommerp = "Glom Glom"
STRINGS.CHARACTER_NAMES.babyglommerp = "GLOM GLOM"
STRINGS.CHARACTER_DESCRIPTIONS.babyglommerp = "*Is a baby\n*Can grow up\n*Pretty weak"
STRINGS.CHARACTER_QUOTES.babyglommerp = "Mini bouncy ball edition."

STRINGS.CHARACTER_TITLES.babysheepp = "Ewelet"
STRINGS.CHARACTER_NAMES.babysheepp = "EWELET"
STRINGS.CHARACTER_DESCRIPTIONS.babysheepp = "*Is a baby\n*Can grow up.\n*Pretty weak."
STRINGS.CHARACTER_QUOTES.babysheepp = "They're so cute when they aren't shooting snot."

STRINGS.CHARACTER_TITLES.babydragonp = "Broodling"
STRINGS.CHARACTER_NAMES.babydragonp = "BROODLING"
STRINGS.CHARACTER_DESCRIPTIONS.babydragonp = "*Is a baby\n*Can grow up\n*Pretty weak"
STRINGS.CHARACTER_QUOTES.babydragonp = "Its training to burn the world down."

STRINGS.CHARACTER_TITLES.klausp = "Klaus"
STRINGS.CHARACTER_NAMES.klausp = "KLAUS"
STRINGS.CHARACTER_DESCRIPTIONS.klausp = "*Is a Giant\n*Has a secret\n*Has goodies"
STRINGS.CHARACTER_QUOTES.klausp = "The best Klaus ever."

STRINGS.CHARACTER_TITLES.deerp = "No-Eyed Deer"
STRINGS.CHARACTER_NAMES.deerp = "NO-EYED DEER"
STRINGS.CHARACTER_DESCRIPTIONS.deerp = "*Is a deer\n*Can grow horns\n*Is pretty fast"
STRINGS.CHARACTER_QUOTES.deerp = ""

STRINGS.CHARACTER_TITLES.deer_redp = "RedGem Deer"
STRINGS.CHARACTER_NAMES.deer_redp = "REDGEM DEER"
STRINGS.CHARACTER_DESCRIPTIONS.deer_redp = "*Is a deer\n*Can use magic\n*Is pretty fast"
STRINGS.CHARACTER_QUOTES.deer_redp = ""

STRINGS.CHARACTER_TITLES.deer_bluep = "BlueGem Deer"
STRINGS.CHARACTER_NAMES.deer_bluep = "BlueGEM DEER"
STRINGS.CHARACTER_DESCRIPTIONS.deer_bluep = "*Is a Deer\n*Can use magic.\n*Is pretty fast."
STRINGS.CHARACTER_QUOTES.deer_bluep = ""

STRINGS.CHARACTER_TITLES.sheepp = "Ewecus"
STRINGS.CHARACTER_NAMES.sheepp = "EWECUS"
STRINGS.CHARACTER_DESCRIPTIONS.sheepp = "*Is an ugly thing\n*Can do mean kicks\n*Sheds wool"
STRINGS.CHARACTER_QUOTES.sheepp = "Its quite snotty."


------------

----------------------------------------------------------------
-- The character's name as appears in-game 
STRINGS.NAMES.STAGEHANDP = "Stagehand"
STRINGS.NAMES.TOADSTOOLP = "Toadstool"
STRINGS.NAMES.SHADOW_KNIGHTP = "Shadow Knight"
STRINGS.NAMES.SHADOW_BISHOPP = "Shadow Bishop"
STRINGS.NAMES.SHADOW_ROOKP = "Shadow Rook"

STRINGS.NAMES.SHADOWCHANNELERP = "Channeler"
STRINGS.NAMES.NRMONSTER_WPN = "Mysterious Power"
---------
--STRINGS.NAMES.MERMHOUSE_PLAYER = "Merm House"
----------
--STRINGS.NAMES.MONSTER_WPN = "Mysterious Power"
----------------------------------------------------------------
-- Custom speech strings
--STRINGS.CHARACTERS.STALKERP = require "speech_stalkerp"
----------------------------------------------------------------
-- The default responses of examining the prefab

STRINGS.CHARACTERS.GENERIC.DESCRIBE.SHADOWCHANNELERP = 
{
	GENERIC = "Theres power emitting from that.",
}

---------------------------------------------------------------------------
------------------------------------------------------------------
-- The prefab names as they appear in-game
------------------------------------------------------------------

------------------------------------------------------------------
--Reforged Strings
------------------------------------------------------------------
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.toadstoolp = "*Is a Giant\n*Can spore enemies\n*Can groundpound with C\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.stalker_forestp = "*Is a Giant\n*Can revive others and heals 25% better\n*Can't attack...\n\nExpertise:\nStaves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.stalkerp = "*Is a Giant\n*Can Fossilize with C\n*Immune to debuffs\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.stalker_atriump = "*Is a Giant\n*Can Mind Control with C\n*Can heal with Z\n\nExpertise:\nNone"
------------------------------------------------------------------
-- Mob strings
------------------------------------------------------------------


------------------------------------------------------------------
-- Shadow skin strings
------------------------------------------------------------------

--[[
STRINGS.SKIN_QUOTES.houndplayer_shadow = "The Rare Varg Hound"
STRINGS.SKIN_NAMES.houndplayer_shadow = "Shadow Hound"
--]]

return STRINGS