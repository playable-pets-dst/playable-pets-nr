require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.REVIVE_CORPSE, "noaction"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

--Soldier Stuff
local normalsounds =
{
    attack = "dontstarve/bee/killerbee_attack",
    --attack = "dontstarve/creatures/together/bee_queen/beeguard/attack",
    buzz = "dontstarve/bee/bee_fly_LP",
    hit = "dontstarve/creatures/together/bee_queen/beeguard/hurt",
    death = "dontstarve/creatures/together/bee_queen/beeguard/death",
}

local poofysounds =
{
    attack = "dontstarve/bee/killerbee_attack",
    --attack = "dontstarve/creatures/together/bee_queen/beeguard/attack",
    buzz = "dontstarve/bee/killerbee_fly_LP",
    hit = "dontstarve/creatures/together/bee_queen/beeguard/hurt",
    death = "dontstarve/creatures/together/bee_queen/beeguard/death",
}

------------------


local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .5, .02, .15, inst, 30)
end

local function StartFlapping(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/wings_LP", "flying")
end

local function RestoreFlapping(inst)
    if not inst.SoundEmitter:PlayingSound("flying") then
        StartFlapping(inst)
    end
end

local function StopFlapping(inst)
    inst.SoundEmitter:KillSound("flying")
end

local function DoScreech(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, 1, .015, .3, inst, 30)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/taunt")
end

local function DoScreechAlert(inst)
    inst.components.epicscare:Scare(5)
    inst.components.commander:AlertAllSoldiers()	
end

local function MinionSetSkinDefault(inst, data)
	if data then
		if inst.is_poofy then
			inst.AnimState:SetBuild(data.puffy_build)
		else	
			inst.AnimState:SetBuild(data.build)			
		end
	else
		inst.AnimState:SetBuild(inst.is_poofy and inst.mob_table.build2 or inst.mob_table.build)
	end	
end

local function BuffSoldiers(inst)
local soldiers = inst.components.commander:GetAllSoldiers()
	if #soldiers > 0 and inst.canbuff == true then
		inst.canbuff = false
		inst:DoTaskInTime(40, function(inst) inst.canbuff = true end)
		for i, v in ipairs(soldiers) do
			if not v.components.health:IsDead() then
				v.is_poofy = true
				if v.components.ppskin_manager then
					v.components.ppskin_manager:LoadSkin(v.mob_table, true)
				end
				v.components.locomotor.walkspeed = TUNING.BEEGUARD_DASH_SPEED
				v.components.combat:SetDefaultDamage(TUNING.BEEGUARD_PUFFY_DAMAGE)
				v.components.combat:SetAttackPeriod(TUNING.BEEGUARD_PUFFY_ATTACK_PERIOD)
				v.sounds = poofysounds
				if v.SoundEmitter:PlayingSound("buzz") then
					v.SoundEmitter:KillSound("buzz")
					v.SoundEmitter:PlaySound(v.sounds.buzz, "buzz")
				end
				SpawnPrefab("bee_poof_big").Transform:SetPosition(v.Transform:GetWorldPosition())
				v:DoTaskInTime(20, function(inst) 
					inst.is_poofy = false
					if inst.components.ppskin_manager then
						inst.components.ppskin_manager:LoadSkin(inst.mob_table, true)
					end
					inst.components.locomotor.walkspeed = TUNING.BEEGUARD_SPEED
					inst.components.combat:SetDefaultDamage(TUNING.BEEGUARD_PUFFY_DAMAGE)
					inst.components.combat:SetAttackPeriod(TUNING.BEEGUARD_PUFFY_ATTACK_PERIOD)
					inst.sounds = normalsounds
					if inst.SoundEmitter:PlayingSound("buzz") then
						inst.SoundEmitter:KillSound("buzz")
						inst.SoundEmitter:PlaySound(inst.sounds.buzz, "buzz")
					end
					SpawnPrefab("bee_poof_small").Transform:SetPosition(inst.Transform:GetWorldPosition())	
				end)
			end 
		end
	end	
end

local events=
{
    EventHandler("attacked", function(inst)
        if not inst.components.health:IsDead() and
            (   not inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("caninterrupt")
            ) and
            (   inst.sg.mem.last_hit_time == nil or
                inst.sg.mem.last_hit_time + inst.hit_recovery < GetTime()
            ) then
            inst.sg:GoToState("hit")
        end
    end),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
	PP_CommonHandlers.OnKnockback(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst.AnimState:PushAnimation("idle_loop", true)
        end,

		timeline =
        {
            TimeEvent(0, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/breath")
            end),
        },
    },
     State{
        name = "attack",
        tags = { "attack", "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("atk")
            inst.components.combat:StartAttack()
        end,

        timeline =
        {
            TimeEvent(0, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/attack_pre")
            end),
            TimeEvent(13 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/attack")
            end),
            TimeEvent(14 * FRAMES, function(inst)
                inst:PerformBufferedAction()
            end),
            CommonHandlers.OnNoSleepTimeEvent(23 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("idle"),
        },
    }, 
	
	State{
        name = "special_atk2",
        tags = { "spawnguards", "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
			local oldnum = inst.components.commander:GetNumSoldiers()
			local soldiers = inst.components.commander:GetAllSoldiers()
			local numbees = #soldiers
			if numbees >= 8 or inst.specialatk2 == false then
				inst.sg:GoToState("idle")
			else
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("spawn")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/spawn")
			PlayablePets.StartSpecialAttack(inst)
			end
        end,

        timeline =
        {
            TimeEvent(16 * FRAMES, function(inst)
                inst.sg.mem.wantstospawnguards = nil
                inst.components.timer:StartTimer("spawnguards_cd", inst.spawnguards_cd)

                local oldnum = inst.components.commander:GetNumSoldiers()
                local x, y, z = inst.Transform:GetWorldPosition()
                local rot = inst.Transform:GetRotation()
                local num = math.random(TUNING.BEEQUEEN_MIN_GUARDS_PER_SPAWN, TUNING.BEEQUEEN_MAX_GUARDS_PER_SPAWN)
                if num + oldnum > TUNING.BEEQUEEN_TOTAL_GUARDS then
                    num = math.max(TUNING.BEEQUEEN_MIN_GUARDS_PER_SPAWN, TUNING.BEEQUEEN_TOTAL_GUARDS - oldnum)
                end
                local drot = 360 / num
                for i = 1, num do
                    local minion = SpawnPrefab("beeguard")
                    local angle = rot + i * drot
                    local radius = minion.Physics:GetRadius()
                    minion.Transform:SetRotation(angle)
                    minion.OnEntitySleep = minion.Remove
					if inst.components.ppskin_manager:GetSkin() then
						minion:AddComponent("ppskin_manager")
						minion.components.ppskin_manager:SetSkin(inst.components.ppskin_manager:GetSkin(), "beeguardp")
						minion.components.ppskin_manager:SetSkinDefaultFn(MinionSetSkinDefault)
					end
					minion.mob_table = {
						build = "bee_guard_build",
						build2 = "bee_guard_puffy_build",
					}
					minion.persists = false
					PlayablePets.SetCommonStatResistances(minion, 3, 3)
                    angle = -angle * DEGREES
                    minion.Transform:SetPosition(x + radius * math.cos(angle), 0, z + radius * math.sin(angle))
                    minion:OnSpawnedGuard(inst)
                end
				inst.specialatk2 = false
                if oldnum > 0 then
                    local soldiers = inst.components.commander:GetAllSoldiers()
                    num = #soldiers
                    drot = 360 / num
                    for i = 1, num do
                        local angle = -(rot + i * drot) * DEGREES
                        local xoffs = TUNING.BEEGUARD_GUARD_RANGE * math.cos(angle)
                        local zoffs = TUNING.BEEGUARD_GUARD_RANGE * math.sin(angle)
                        local mindistsq = math.huge
                        local closest = 1
                        for i2, v in ipairs(soldiers) do
                            local offset = v.components.knownlocations:GetLocation("queenoffset")
                            if offset ~= nil then
                                local distsq = distsq(xoffs, zoffs, offset.x, offset.z)
                                if distsq < mindistsq then
                                    mindistsq = distsq
                                    closest = i2
                                end
                            end
                        end
                        table.remove(soldiers, closest).components.knownlocations:RememberLocation("queenoffset", Vector3(xoffs, 0, zoffs), false)
                    end
                end
            end),
            CommonHandlers.OnNoSleepTimeEvent(32 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },
		
		onexit = function(inst)
			inst:DoTaskInTime(10, function(inst) inst.specialatk2 = true end)
		end,
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	 State{
        name = "focustarget",
        tags = { "focustarget", "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("command2")
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, DoScreech),
            TimeEvent(9 * FRAMES, DoScreechAlert),
            TimeEvent(11 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/attack_pre")
            end),
            TimeEvent(18 * FRAMES, function(inst)
                inst.sg.mem.wantstofocustarget = nil
                inst.sg.mem.focuscount = 0
                inst.components.timer:StartTimer("focustarget_cd", inst.focustarget_cd)

                local soldiers = inst.components.commander:GetAllSoldiers()
                if #soldiers > 0 then
                    local players = {}
                    for k, v in pairs(inst.components.grouptargeter:GetTargets()) do
                        if inst:IsNear(k, TUNING.BEEQUEEN_FOCUSTARGET_RANGE) then
                            table.insert(players, k)
                        end
                    end
                    local groupsize = math.ceil(#soldiers * .5)
                    local numtargets = groupsize >= TUNING.BEEQUEEN_MIN_GUARDS_PER_SPAWN and 2 or 1
                    local targets = {}
                    for i = 1, numtargets do
                        if #players > 0 then
                            table.insert(targets, table.remove(players, math.random(#players)))
                        else
                            if inst.components.combat.target ~= nil and not inst.components.combat.target:HasTag("player") then
                                table.insert(targets, inst.components.combat.target)
                            end
                            break
                        end
                    end
                    if #targets > 1 then
                        local sorted = {}
                        for i, v in ipairs(soldiers) do
                            local dists = {}
                            local totaldist = 0
                            for i1, v1 in ipairs(targets) do
                                local distsq = v:GetDistanceSqToInst(v1)
                                table.insert(dists, distsq)
                                totaldist = totaldist + distsq
                            end
                            for i1, v1 in ipairs(dists) do
                                dists[i1] = v1 / totaldist
                            end
                            table.insert(sorted, { inst = v, scores = dists })
                        end
                        for i, v in ipairs(targets) do
                            table.sort(sorted, function(a, b) return a.scores[i] < b.scores[i] end)
                            for i1 = 1, math.min(groupsize, #sorted) do
                                table.remove(sorted, 1).inst:FocusTarget(v)
                            end
                        end
                    elseif #targets > 0 then
                        for i, v in ipairs(soldiers) do
                            v:FocusTarget(targets[1])
                        end
                    end
                    inst.sg.mem.focustargets = targets
                end
            end),
            CommonHandlers.OnNoSleepTimeEvent(25 * FRAMES, function(inst)
                inst.sg:AddStateTag("caninterrupt")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("focustarget_loop"),
        },
    },

    State{
        name = "focustarget_loop",
        tags = { "focustarget", "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
            if inst.sg.mem.focuscount >= 3 or
                inst.sg.mem.focustargets == nil or
                inst.components.commander:GetNumSoldiers() <= 0 then
                inst.sg:GoToState("focustarget_pst")
            else
                inst.sg.statemem.variation = (inst.sg.mem.focuscount % 2) + 1
                inst.sg.mem.focuscount = inst.sg.mem.focuscount + 1
                inst.components.locomotor:StopMoving()
                if inst.sg.statemem.variation > 1 then
                    inst.sg:GoToState("focustarget_loop2")
                else
                    inst.AnimState:PlayAnimation("command1")
                end
            end
        end,

        timeline =
        {
            TimeEvent(6 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/attack_pre")
            end),
            TimeEvent(7 * FRAMES, DoScreech),
            TimeEvent(8 * FRAMES, DoScreechAlert),
            TimeEvent(20 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/attack_pre")
            end),
            TimeEvent(22 * FRAMES, DoScreech),
            TimeEvent(23 * FRAMES, DoScreechAlert),
            CommonHandlers.OnNoSleepTimeEvent(35 * FRAMES, function(inst)
                inst.sg:AddStateTag("caninterrupt")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("focustarget_loop"),
        },
    },

    State{
        name = "focustarget_loop2",
        tags = { "focustarget", "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("command2")
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, DoScreech),
            TimeEvent(9 * FRAMES, DoScreechAlert),
            TimeEvent(11 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/attack_pre")
            end),
            CommonHandlers.OnNoSleepTimeEvent(25 * FRAMES, function(inst)
                inst.sg:AddStateTag("caninterrupt")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("focustarget_loop"),
        },
    },

    State{
        name = "focustarget_pst",
        tags = { "focustarget", "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
            inst.sg.mem.focuscount = nil
            inst.sg.mem.focustargets = nil
            if inst.components.commander:GetNumSoldiers() <= 0 then
                inst.sg.statemem.ended = true
                inst.sg:GoToState("idle")
            else
                inst.components.locomotor:StopMoving()
                inst.AnimState:PlayAnimation("command3")
            end
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, DoScreech),
            TimeEvent(9 * FRAMES, DoScreechAlert),
            TimeEvent(19 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/attack_pre")
            end),
            TimeEvent(23 * FRAMES, function(inst)
                inst.sg.statemem.ended = true
                for i, v in ipairs(inst.components.commander:GetAllSoldiers()) do
                    v:FocusTarget(nil)
                end
            end),
            CommonHandlers.OnNoSleepTimeEvent(32 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("idle"),
        },

        onexit = function(inst)
            if not inst.sg.statemem.ended then
                for i, v in ipairs(inst.components.commander:GetAllSoldiers()) do
                    v:FocusTarget(nil)
                end
            end
        end,
    },

	State{
        name = "run_start",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			if inst.components.locomotor.runspeed <= TUNING.BEEQUEEN_SPEED then            
				inst.AnimState:PlayAnimation("walk_pre")
			else
				inst.AnimState:PlayAnimation("walk_loop")
			end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("run")
                end
            end),
        },
    },

    State{
        name = "run",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("walk_loop")
        end,

        timeline =
        {
            TimeEvent(0, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/breath")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("run")
                end
            end),
        },
    },

    State{
        name = "run_stop",
        tags = { "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("walk_pst")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
        name = "walk_stop",
        tags = { "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("walk_pst")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
        name = "hit",
        tags = { "hit", "busy" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/hit")
            inst.sg.mem.last_hit_time = GetTime()
			PlayablePets.RaiseFlyingCreature(inst)
        end,

        timeline =
        {
            TimeEvent(10 * FRAMES, function(inst)
                if inst.sg.statemem.doattack then
                    if not inst.components.health:IsDead() and ChooseAttack(inst) then
                        return
                    end
                    inst.sg.statemem.doattack = nil
                end
                inst.sg:RemoveStateTag("busy")
            end),
        },

        events =
        {
            EventHandler("doattack", function(inst)
                inst.sg.statemem.doattack = true
            end),
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    if inst.sg.statemem.doattack and ChooseAttack(inst) then
                        return
                    end
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
        name = "special_atk1",
        tags = { "screech", "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
            local screechanim = math.random(1,4)
				if screechanim <= 2 then
					inst.sg:GoToState("taunt")
				elseif screechanim > 2 then
					inst.sg:GoToState("taunt"..screechanim)	
				end
        end,
    },
	
	State{
        name = "taunt",
        tags = { "screech", "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("screech")
            inst.sg.mem.wantstoscreech = nil
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, DoScreech),
            TimeEvent(9 * FRAMES, DoScreechAlert),
			TimeEvent(18 * FRAMES, BuffSoldiers),
            TimeEvent(33 * FRAMES, DoScreech),
            TimeEvent(34 * FRAMES, DoScreechAlert),
            CommonHandlers.OnNoSleepTimeEvent(57 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("idle"),
        },
    },
	
	State{
        name = "taunt1",
        tags = { "screech", "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("taunt")
            inst.sg.mem.wantstoscreech = nil
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, DoScreech),
            TimeEvent(9 * FRAMES, DoScreechAlert),
			TimeEvent(18 * FRAMES, BuffSoldiers),
            TimeEvent(19 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/attack_pre")
            end),
            TimeEvent(23 * FRAMES, function(inst)
                inst.sg.statemem.ended = true
            end),
            CommonHandlers.OnNoSleepTimeEvent(32 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("idle"),
        },
    },
	
	State{
        name = "taunt2",
        tags = { "screech", "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("taunt")
            inst.sg.mem.wantstoscreech = nil
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, DoScreech),
            TimeEvent(9 * FRAMES, DoScreechAlert),
			TimeEvent(18 * FRAMES, BuffSoldiers),
            CommonHandlers.OnNoSleepTimeEvent(57 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("idle"),
        },
    },
	
	State{
        name = "taunt3",
        tags = { "screech", "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("command2")
            inst.sg.mem.wantstoscreech = nil
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, DoScreech),
            TimeEvent(9 * FRAMES, DoScreechAlert),
            TimeEvent(11 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/attack_pre")
            end),
			TimeEvent(18 * FRAMES, BuffSoldiers),
            CommonHandlers.OnNoSleepTimeEvent(25 * FRAMES, function(inst)
                inst.sg:AddStateTag("caninterrupt")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("idle"),
        },
    },
	
	State{
        name = "taunt4",
        tags = { "screech", "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("command3")
            inst.sg.mem.wantstoscreech = nil
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, DoScreech),
            TimeEvent(9 * FRAMES, DoScreechAlert),
            TimeEvent(19 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/attack_pre")
            end),
			TimeEvent(23 * FRAMES, BuffSoldiers),
            CommonHandlers.OnNoSleepTimeEvent(32 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("idle"),
        },
    },
	
	State{
        name = "screech",
        tags = { "screech", "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("screech")
            inst.sg.mem.wantstoscreech = nil
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, DoScreech),
            TimeEvent(9 * FRAMES, DoScreechAlert),
            TimeEvent(33 * FRAMES, DoScreech),
            TimeEvent(34 * FRAMES, DoScreechAlert),
            CommonHandlers.OnNoSleepTimeEvent(57 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("idle"),
        },
    },
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:KillSound("flying")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
        end,

		 timeline =
        {
            TimeEvent(14 * FRAMES, DoScreech),
            TimeEvent(15 * FRAMES, DoScreechAlert),
            TimeEvent(28 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/bee/beehive_hit")
                ShakeIfClose(inst)
                inst.components.lootdropper:DropLoot(inst:GetPosition())
				inst.components.inventory:DropEverything(true)
            end),
        },
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
			inst.sg:AddStateTag("caninterrupt")
        end,
		
		timeline = {
        TimeEvent(8 * FRAMES, StopFlapping),
        TimeEvent(28 * FRAMES, function(inst)
            inst.sg:RemoveStateTag("caninterrupt")
        end),
        TimeEvent(31 * FRAMES, function(inst)
			inst:StopHoney()
            inst.SoundEmitter:PlaySound("dontstarve/bee/beehive_hit")
			PlayablePets.LandFlyingCreature(inst)
            ShakeIfClose(inst)
        end),
		},
		
		onexit = function(inst)
			PlayablePets.LandFlyingCreature(inst)
		end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			PlayablePets.SleepHeal(inst)			
			inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/sleep")
			inst.AnimState:PlayAnimation("sleep_loop")
		end,
			
		onexit = function(inst)

		end,
        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
			
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			
			PlayablePets.RaiseFlyingCreature(inst)
			--StartFlapping(inst)
			inst:StartHoney()
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,
		
		timeline = {
			TimeEvent(19 * FRAMES, StartFlapping),
				CommonHandlers.OnNoSleepTimeEvent(24 * FRAMES, function(inst)
				inst.sg:RemoveStateTag("busy")
				inst.sg:RemoveStateTag("nosleep")
			end),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
}


--[[CommonStates.AddSleepStates(states,
{
	sleeptimeline = {
        TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/sleep") end),
	},
})

]]

local function OnOverrideFrozenSymbols(inst)
    StopFlapping(inst)
    inst:StopHoney()
end
local function OnClearFrozenSymbols(inst)
    StartFlapping(inst)
    inst:StartHoney()
    inst.sg.mem.wantstoscreech = true
end
CommonStates.AddFrozenStates(states, OnOverrideFrozenSymbols, OnClearFrozenSymbols)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(14 * FRAMES, DoScreech),
            TimeEvent(15 * FRAMES, DoScreechAlert),
            TimeEvent(28 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/bee/beehive_hit")
                ShakeIfClose(inst)
            end),
		},
		
		corpse_taunt =
		{
			TimeEvent(19 * FRAMES, StartFlapping),
				CommonHandlers.OnNoSleepTimeEvent(24 * FRAMES, function(inst)
				inst.sg:RemoveStateTag("busy")
				inst.sg:RemoveStateTag("nosleep")
			end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "sleep_pst"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "walk_pst")
PP_CommonStates.AddSailStates(states, {}, "idle_loop", "walk_pst")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
CommonStates.AddHopStates(states, false, {pre = simplemove, loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = simpleanim,
}
)
    
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = simpleanim,
	
	castspelltime = 10,
})
    
return StateGraph("beequeenp", states, events, "idle", actionhandlers)

