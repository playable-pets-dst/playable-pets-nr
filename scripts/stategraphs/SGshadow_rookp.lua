require("stategraphs/commonstates")
local ShadowChess = require("stategraphs/SGshadow_chesspiecesp")

require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	--ActionHandler(ACTIONS.ATTACK, "attack_pre"),
	ActionHandler(ACTIONS.REVIVE_CORPSE, "idle"), --Can't revive
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return  {"INLIMBO", "notarget", "invisible", "noattack", "flight", "playerghost", "shadow", "shadowcreature", "shadowminion", "wall", "structure"}
	elseif PP_FORGE_ENABLED then
		return {"player", "companion", "INLIMBO", "notarget", "magicimmune"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "wall", "playerghost", "magicimmune"}
	end
end

local SWARM_PERIOD = .5
local SWARM_START_DELAY = .25

local function DoSwarmAttack(inst)
    inst.components.combat:DoAreaAttack(inst, inst.components.combat.hitrange, nil, nil, nil, { })
end

local function DoSwarmWork(inst)
    PlayablePets:DoWork(inst, inst.level * 1.5)
end

local function DoSwarmFX(inst)
    local fx = SpawnPrefab("shadow_bishop_fx")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx.Transform:SetScale(inst.Transform:GetScale())
    fx.AnimState:SetMultColour(inst.AnimState:GetMultColour())
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") and not inst.sg:HasStateTag("busy") then inst.sg:GoToState("hit") end end),
    PP_CommonHandlers.OnDeath(),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(true,false),
	EventHandler("locomote", function(inst, data)
        if inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("sleeping") and not inst.sg:HasStateTag("home") then
            return
        end
        local is_moving = inst.sg:HasStateTag("moving")
        local should_move = inst.components.locomotor:WantsToMoveForward()

        if is_moving and not should_move and not inst.sg:HasStateTag("attack") then
            inst.sg:GoToState("run_stop")
        elseif not is_moving and should_move and not inst.sg:HasStateTag("attack") then
            inst.sg:GoToState("run_start")
        elseif data.force_idle_state and not (is_moving or should_move or inst.sg:HasStateTag("idle") or inst.sg:HasStateTag("attack") ) then
            inst.sg:GoToState("idle")
        end
    end),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.OpenGift(),
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.Physics:Stop()
			inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },
	
	State{
        name = "attack",
        tags ={ "attack", "busy" },

        onenter = function(inst, target)
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			if target and target:IsValid() then
				inst.current_target = target
				inst:FacePoint(target:GetPosition())
			end
            inst.Physics:Stop()
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("teleport_pre")
            inst.AnimState:PushAnimation("teleport", false)
        end,

        timeline =
        {
            ShadowChess.Functions.ExtendedSoundTimelineEvent(0, "attack_grunt"),
            ShadowChess.Functions.ExtendedSoundTimelineEvent(12 * FRAMES, "teleport"),
			TimeEvent(19 * FRAMES, function(inst)
                inst.sg:AddStateTag("noattack")
                inst.components.health:SetInvincible(true)
            end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg.statemem.attack = true
                    inst.sg:GoToState("attack_teleport", inst.current_target)
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.attack then
                inst.components.health:SetInvincible(false)
            end
        end,
    },

    State{
        name = "attack_teleport",
        tags = { "attack", "busy", "noattack" },

        onenter = function(inst, target)
            inst.components.health:SetInvincible(true)
            if inst.current_target and inst.current_target:IsValid() then
                inst:FacePoint(inst.current_target:GetPosition())
				local pos = inst.current_target:GetPosition()
				pos.y = 0
				inst.Transform:SetPosition(pos:Get()) 
			end
            inst.AnimState:PlayAnimation("teleport_atk")
            inst.AnimState:PushAnimation("teleport_pst", false)
			inst.components.combat:StartAttack()
        end,

        timeline =
        {
			ShadowChess.Functions.ExtendedSoundTimelineEvent(1 * FRAMES, "attack"),
            TimeEvent(17 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("noattack")
                inst.components.health:SetInvincible(false)
                inst.components.combat:DoAreaAttack(inst, inst.components.combat.hitrange, nil, nil, nil, { "INLIMBO", "notarget", "invisible", "noattack", "flight", "playerghost", "shadow", "shadowcreature", "wall", "structure" })
            end),
            TimeEvent(34 * FRAMES, function(inst) inst.sg:RemoveStateTag("busy") end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
					if inst.level == 3 then
						inst.sg:GoToState("special_atk1")
					else
						inst.sg:GoToState("idle")
					end
                end
            end),
        },

        onexit = function(inst)
            inst.components.health:SetInvincible(false)
        end,
    },
	
	State{
        name = "work",
        tags ={ "attack", "busy" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("teleport_pre")
            inst.AnimState:PushAnimation("teleport", false)
        end,

        timeline =
        {
            ShadowChess.Functions.ExtendedSoundTimelineEvent(0, "attack_grunt"),
            ShadowChess.Functions.ExtendedSoundTimelineEvent(12 * FRAMES, "teleport"),
			TimeEvent(19 * FRAMES, function(inst)
                inst.sg:AddStateTag("noattack")
                inst.components.health:SetInvincible(true)
            end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("work_teleport", inst.sg.statemem.target)
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.attack then
                inst.components.health:SetInvincible(false)
            end
        end,
    },

    State{
        name = "work_teleport",
        tags = { "attack", "busy", "noattack" },

        onenter = function(inst)
            inst.components.health:SetInvincible(true)
			local pos2 = inst:GetPosition()

			--inst.Transform:SetPosition(pos2:Get()) --we don't need to teleport.
            inst.AnimState:PlayAnimation("teleport_atk")
            inst.AnimState:PushAnimation("teleport_pst", false)
        end,

        timeline =
        {
			ShadowChess.Functions.ExtendedSoundTimelineEvent(1 * FRAMES, "attack"),
            TimeEvent(17 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("noattack")
                inst.components.health:SetInvincible(false)
                inst:PerformBufferedAction()
				inst:PerformBufferedAction()
				inst:PerformBufferedAction()
				inst:PerformBufferedAction() --might work multiple times.
            end),
            TimeEvent(34 * FRAMES, function(inst) inst.sg:RemoveStateTag("busy") end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            inst.components.health:SetInvincible(false)
        end,
    },
    
    State{
		name = "taunt",
        tags = { "taunt", "busy" },

        onenter = function(inst, remaining)
			--inst.sg.statemem.remaining = (remaining or 2) - 1
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
            
            if inst.sg.statemem.remaining == 0 then
				-- change target
                local rangesq = TUNING.SHADOWCREATURE_TARGET_DIST * TUNING.SHADOWCREATURE_TARGET_DIST
				local x, y, z = inst.Transform:GetWorldPosition()
				local players = shuffleArray(FindPlayersInRangeSq(x, y, z, rangesq, true))
				RemoveByValue(players, inst.components.combat.target)
				for i, v in ipairs(players) do
					if inst.components.combat:CanTarget(v) then
						inst.components.combat:SetTarget(v)
						break
					end
				end
			end
			
        end,

		timeline=
        {
			ShadowChess.Functions.ExtendedSoundTimelineEvent(20 * FRAMES, "taunt"),
			TimeEvent(30*FRAMES, function(inst)
                ShadowChess.Functions.AwakenNearbyStatues(inst)
                ShadowChess.Functions.TriggerEpicScare(inst)
            end),
            TimeEvent(44*FRAMES, function(inst) inst.sg:RemoveStateTag("busy") end),
        },

        events=
        {
            EventHandler("animover", function(inst) if inst.AnimState:AnimDone() then inst.sg:GoToState(inst.sg.statemem.remaining > 0 and "taunt" or "idle", inst.sg.statemem.remaining) end end),
        },
    },
	
	State{
		name = "special_atk1", --used to be special_atk1
        tags = {"busy", "taunt"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,

		timeline=
        {

			ShadowChess.Functions.ExtendedSoundTimelineEvent(20 * FRAMES, "taunt"),
			TimeEvent(30*FRAMES, function(inst)
                ShadowChess.Functions.AwakenNearbyStatues(inst)
                ShadowChess.Functions.TriggerEpicScare(inst)
            end),
            TimeEvent(44*FRAMES, function(inst) inst.sg:RemoveStateTag("busy") end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
		name = "taunt",
        tags = { "taunt", "busy" },

        onenter = function(inst, remaining)
			--inst.sg.statemem.remaining = (remaining or 2) - 1
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
            
            if inst.sg.statemem.remaining == 0 then
				-- change target
                local rangesq = TUNING.SHADOWCREATURE_TARGET_DIST * TUNING.SHADOWCREATURE_TARGET_DIST
				local x, y, z = inst.Transform:GetWorldPosition()
				local players = shuffleArray(FindPlayersInRangeSq(x, y, z, rangesq, true))
				RemoveByValue(players, inst.components.combat.target)
				for i, v in ipairs(players) do
					if inst.components.combat:CanTarget(v) then
						inst.components.combat:SetTarget(v)
						break
					end
				end
			end
			
        end,

		timeline=
        {
			ShadowChess.Functions.ExtendedSoundTimelineEvent(3 * FRAMES, "taunt"),
			TimeEvent(30*FRAMES, function(inst)
                ShadowChess.Functions.AwakenNearbyStatues(inst)
                ShadowChess.Functions.TriggerEpicScare(inst)
            end),
            TimeEvent(44*FRAMES, function(inst) inst.sg:RemoveStateTag("busy") end),
        },

        events=
        {
            EventHandler("animover", function(inst) if inst.AnimState:AnimDone() then inst.sg:GoToState(inst.sg.statemem.remaining > 0 and "taunt" or "idle", inst.sg.statemem.remaining) end end),
        },
    },
	
	State{
		name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,

		timeline=
        {
			ShadowChess.Functions.ExtendedSoundTimelineEvent(3 * FRAMES, "taunt"),
			TimeEvent(12*FRAMES, function(inst)
                --ShadowChess.Functions.AwakenNearbyStatues(inst)
                --ShadowChess.Functions.TriggerEpicScare(inst)
            end),
            TimeEvent(47*FRAMES, function(inst) inst.sg:RemoveStateTag("busy") end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.inventory:DropEverything(true)
            --inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition())      
			--if inst.components.playercontroller ~= nil then
               --inst.components.playercontroller:RemotePausePrediction()
            --end
        end,

		timeline = 
		{
			ShadowChess.Functions.DeathSoundTimelineEvent(45 * FRAMES),
			ShadowChess.Functions.DeathSoundTimelineEvent(64 * FRAMES),
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
					inst.noskeleton = true
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },		
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("walk_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline =
        {
            
        },
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")            
        end,


        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")            
        end,


        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State
    {
		name = "levelup",
        tags = {"busy", "levelup"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("transform")
        end,

		timeline=
        {
			TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.levelup) end),
			TimeEvent(58*FRAMES, function(inst)
				inst:LevelUp(math.min(inst.level + 1, 3))
                --AwakenNearbyStatues(inst)
                --TriggerEpicScare(inst)
			end),
            TimeEvent(95*FRAMES, function(inst) inst.sg:RemoveStateTag("busy") end),
        },

        events=
        {
            EventHandler("animover", function(inst) if inst.AnimState:AnimDone() then inst.sg:GoToState("idle") end end),
        },
    },
	
	State
    {
        name = "hit",
        tags = { "busy" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("hit")
        end,

		timeline =
		{
			ShadowChess.Functions.ExtendedSoundTimelineEvent(0 * FRAMES, "hit"),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
}

CommonStates.AddFrozenStates(states)
local moveanim = "walk"
local idleanim = "idle_loop"
local actionanim = "walk_pst"
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            PlayablePets.DoWork(inst, inst.level * 1.5)
        end),
	}, 
	actionanim, nil, nil, "idle_loop", actionanim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			ShadowChess.Functions.DeathSoundTimelineEvent(45 * FRAMES),
			ShadowChess.Functions.DeathSoundTimelineEvent(64 * FRAMES),
		},
		
		corpse_taunt =
		{
			ShadowChess.Functions.ExtendedSoundTimelineEvent(7 * FRAMES, "taunt"),
			TimeEvent(12*FRAMES, function(inst)
                --ShadowChess.Functions.AwakenNearbyStatues(inst)
                --ShadowChess.Functions.TriggerEpicScare(inst)
            end),
            TimeEvent(47*FRAMES, function(inst) inst.sg:RemoveStateTag("busy") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt",
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, actionanim)
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
--PP_CommonStates.AddHomeState(states, nil, "eat", "taunt", true)
CommonStates.AddHopStates(states, false, {pre = moveanim.."_pre", loop = moveanim.."_loop", pst = moveanim.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = actionanim,
	plank_idle_loop = idleanim,
	plank_idle_pst = actionanim,
	
	plank_hop_pre = moveanim.."_pre",
	plank_hop = moveanim.."_loop",
	
	steer_pre = actionanim,
	steer_idle = idleanim,
	steer_turning = actionanim,
	stop_steering = actionanim,
	
	row = actionanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = actionanim,
	
	leap_pre = moveanim.."_pre",
	leap_loop = moveanim.."_loop",
	leap_pst = moveanim.."_pst",
	
	lunge_pre = moveanim.."_pre",
	lunge_loop = moveanim.."_loop",
	lunge_pst = moveanim.."_pst",
	
	superjump_pre = moveanim.."_pre",
	superjump_loop = moveanim.."_loop",
	superjump_pst = moveanim.."_pst",
	
	castspelltime = 10,
})
    
return StateGraph("shadow_rookp", states, events, "idle", actionhandlers)

