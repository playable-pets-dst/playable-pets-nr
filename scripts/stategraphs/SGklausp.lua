require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local function GetAttack(inst)
	return PP_FORGE_ENABLED and "quickattack" or "attack"
end

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function (inst)
		if inst:IsUnchained() then
			if inst.canchomp == true then
				return "attack_chomp"
			else
				return GetAttack(inst)
			end	
		else
			return GetAttack(inst)
		end
	end),
	ActionHandler(ACTIONS.REVIVE_CORPSE, "idle"), --Can't revive
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local CHOMP_DAMAGE = 100*2

local function DoCast(inst, target)
	local tar = target or nil
	local spell = SpawnPrefab(inst.magicattack == "fire" and "klaus_fire_meteorp" or "deer_ice_circlep")
	spell.master = inst
	if inst:IsUnchained() then
		spell.ischarged = 1.5
	end
	if tar ~= nil then
		local x, y, z = tar.Transform:GetWorldPosition() 
		spell.Transform:SetPosition(x, 0, z)
	else
		local x, y, z = inst.Transform:GetWorldPosition() 
		spell.Transform:SetPosition(x, 0, z)
	end
	if inst.magicattack ~= "fire" then
		spell:DoTaskInTime(5, spell.KillFX)
	end	
	inst.magicattack = inst.magicattack == "fire" and "ice" or "fire"
end

local function ShakeIfClose(inst)
    if inst.enraged then
        ShakeAllCameras(CAMERASHAKE.FULL, .5, .02, .15, inst, 30)
    end
end

local function DoRoarShake(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, 1.5, .015, inst.enraged and .3 or .15, inst, 30)
end

local function DoRoarAlert(inst)
    inst.components.epicscare:Scare(5)
end

local function DoChompShake(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, 1.5, .015, inst.enraged and .2 or .1, inst, 20)
end

local function DoFoleySounds(inst, volume)
    inst:DoFoleySounds(volume)
end

local function DoFootstep(inst, volume)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/step", nil, volume)
    ShakeIfClose(inst)
end

local function DoLanding(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/step")
    ShakeAllCameras(CAMERASHAKE.FULL, .5, .02, .2, inst, 30)
end

local function DoSwipeSound(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/swipe", nil, not inst.enraged and .7 or nil)
end

local function DoScratchSound(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/scratch")
end

local function DoBodyfall(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/bodyfall")
    ShakeIfClose(inst)
end

local function TryChomp(inst)
    local target = inst:FindChompTarget()
    if target ~= nil then
        if not inst.components.combat:TargetIs(target) then
            inst.components.combat:SetTarget(target)
        end
        inst.sg:GoToState("attack_chomp", target)
        return true
    end
end

local function CalcChompSpeed(inst, target)
	--local target = inst.components.combat.target
    local x, y, z = target.Transform:GetWorldPosition()
    local distsq = inst:GetDistanceSqToPoint(x, y, z)
    if distsq > 0 then
        inst:ForceFacePoint(x, y, z)
        local dist = math.sqrt(distsq) - (target.Physics ~= nil and inst.Physics:GetRadius() + target.Physics:GetRadius() or inst.Physics:GetRadius())
        if dist > 0 then
            return math.min(8, dist) / (10 * FRAMES)
        end
    end
    return 0
end
-----------------------------------------

local function StartLaughing(inst)
    inst.sg.mem.laughsremaining = 3
end

local function ReduceLaughing(inst, amt)
    inst.sg.mem.laughsremaining = (inst.sg.mem.laughsremaining or 0) > amt and inst.sg.mem.laughsremaining - amt or nil
end

local function StopLaughing(inst)
    inst.sg.mem.laughsremaining = nil
end

--------------------------------------------------------------------------

local events=
{
    EventHandler("attacked", function(inst)
        if not inst.components.health:IsDead() and
            (not inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("sleeping")) and
            (inst.sg.mem.last_hit_time or 0) + inst.hit_recovery < GetTime() then
				inst.sg:GoToState("hit")
        end
    end),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
}


 local states=
{

    State{
        name = "idle",
        tags = { "idle", "canrotate" },

        onenter = function(inst)
                inst.Physics:Stop()
                inst.AnimState:PlayAnimation("idle_loop")
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, function(inst)
                DoFoleySounds(inst, .25)
            end),
            TimeEvent(27 * FRAMES, function(inst)
                DoFoleySounds(inst, .2)
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	 State{
        name = "hit",
        tags = { "hit", "busy" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("hit")
            inst.sg.mem.last_hit_time = GetTime()
        end,

        timeline =
        {
            TimeEvent(0, DoFoleySounds),
            TimeEvent(12 * FRAMES, function(inst)
                DoFoleySounds(inst)
                DoFootstep(inst, .6)
            end),
            TimeEvent(14 * FRAMES, function(inst)
                if inst.components.health:IsDead() then
                    inst.sg:RemoveStateTag("busy")
                elseif inst.sg.mem.wantstotransition ~= nil then
                    inst.sg:GoToState("transition", inst.sg.mem.wantstotransition)
                elseif inst.sg.mem.laughsremaining ~= nil then
                    inst.sg:GoToState("laugh_pre")
                elseif not ((inst.sg.mem.wantstochomp and TryChomp(inst)) or
                            (inst.sg.statemem.doattack and ChooseAttack(inst))) then
                    inst.sg.statemem.doattack = nil
                    inst.sg:RemoveStateTag("busy")
                end
            end),
        },

        events =
        {
            EventHandler("doattack", function(inst)
                inst.sg.statemem.doattack = true
            end),
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    if inst.sg.mem.wantstotransition ~= nil then
                        inst.sg:GoToState("transition", inst.sg.mem.wantstotransition)
                    elseif inst.sg.mem.laughsremaining ~= nil then
                        inst.sg:GoToState("laugh_pre")
                    elseif not ((inst.sg.mem.wantstochomp and TryChomp(inst)) or
                                (inst.sg.statemem.doattack and ChooseAttack(inst))) then
                        inst.sg:GoToState("idle")
                    end
                end
            end),
        },
    },

	State{
        name = "opengift",
        tags = { "busy", "pausepredict" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
            inst:ClearBufferedAction()

            --if IsNearDanger(inst) then
                --inst.sg.statemem.isdanger = true
                --inst.sg:GoToState("idle")
                --if inst.components.talker ~= nil then
                   -- inst.components.talker:Say(GetString(inst, "ANNOUNCE_NODANGERGIFT"))
                --end
                --return
           -- end

            inst.SoundEmitter:PlaySound("dontstarve/common/player_receives_gift")
            inst.AnimState:PlayAnimation("lol_pre")
            inst.AnimState:PushAnimation("lol_loop", true)
            -- NOTE: the previously used ripping paper anim is called "giift_loop"

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:EnableMapControls(false)
                inst.components.playercontroller:Enable(false)
            end
            inst.components.inventory:Hide()
            inst:PushEvent("ms_closepopups")
            inst:ShowActions(false)
            inst:ShowPopUp(POPUPS.GIFTITEM, true)

            if inst.components.giftreceiver ~= nil then
                inst.components.giftreceiver:OnStartOpenGift()
            end
        end,

        timeline =
        {
            -- Timing of the gift box opening animation on giftitempopup.lua
            TimeEvent(155 * FRAMES, function(inst)
               -- inst.AnimState:PlayAnimation("gift_open_pre")
                inst.AnimState:PushAnimation("taunt", true)
            end),
        },

        events =
        {
            EventHandler("firedamage", function(inst)
                inst.AnimState:PlayAnimation("taunt")
                inst.sg:GoToState("idle", true)
                if inst.components.talker ~= nil then
                    inst.components.talker:Say(GetString(inst, "ANNOUNCE_NODANGERGIFT"))
                end
            end),
            EventHandler("ms_doneopengift", function(inst, data)
				inst.sg:GoToState("idle", true)
                
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.isdanger then
                return
            elseif not inst.sg.statemem.isopeningwardrobe then
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:EnableMapControls(true)
                    inst.components.playercontroller:Enable(true)
                end
                inst.components.inventory:Show()
                inst:ShowActions(true)
            end
            inst:ShowPopUp(POPUPS.GIFTITEM, false)
        end,
    },

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()   
            --inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition())      
			--if inst.components.playercontroller ~= nil then
               --inst.components.playercontroller:RemotePausePrediction()
            --end
        end,
		timeline = 
		{
			TimeEvent(FRAMES, DoFoleySounds),
            TimeEvent(3 * FRAMES, function(inst)
                DoBodyfall(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/death")
            end),
            TimeEvent(23 * FRAMES, DoFoleySounds),
            TimeEvent(25 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/bodyfall")
                ShakeIfClose(inst)
            end),
            TimeEvent(27 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
                if inst:IsUnchained() then
                    inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
					inst.components.inventory:DropEverything(true)
                end
            end),
            TimeEvent(3, function(inst)
                if not inst:IsUnchained() then
                    inst.sg.statemem.resurrecting = true
                    inst.sg:GoToState("resurrect")
                end
            end),
            TimeEvent(5, function(inst)
                if inst:IsUnchained() then
					 RemovePhysicsColliders(inst)  
                     PlayablePets.DoDeath(inst)
                end
            end),
		},
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					--TheWorld:PushEvent("ms_playerdespawnanddelete", inst)
                    --inst:PushEvent(inst.ghostenabled and "makeplayerghost" or "playerdied", { skeleton = false })
                end
            end),
        },

    },
	
	 State{
        name = "resurrect",
        tags = { "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("death_amulet")
        end,

        timeline =
        {
            TimeEvent(0, function(inst)
                local stafflight = SpawnPrefab("staff_castinglight")
                stafflight.Transform:SetPosition(inst.Transform:GetWorldPosition())
                stafflight:SetUp({ 150 / 255, 46 / 255, 46 / 255 }, 1, 20 * FRAMES)
                inst.SoundEmitter:PlaySound("dontstarve/common/rebirth_amulet_raise")
            end),
            TimeEvent(39 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/common/rebirth_amulet_poof")
            end),
			TimeEvent(48 * FRAMES, DoBodyfall),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg.statemem.resurrecting = true
                    inst.sg:GoToState("resurrect_pst")
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.resurrecting then
                inst.components.health:SetPercent(0.5)
                inst:Unchain()
				inst.components.inventory:Show()
			inst:ShowActions(true)
            inst:ShowHUD(true)
            inst:SetCameraDistance()
			inst.sg:RemoveStateTag("busy")
            SerializeUserSession(inst) 
            end
        end,
    },
	
	 State{
        name = "resurrect_pst",
        tags = { "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("death_pst")
        end,

        timeline =
        {
            TimeEvent(0, function(inst)
                DoFoleySounds(inst, .3)
                DoScratchSound(inst)
            end),
            TimeEvent(4 * FRAMES, DoBodyfall),
            TimeEvent(10 * FRAMES, function(inst)
                DoFoleySounds(inst, .2)
            end),
            TimeEvent(23 * FRAMES, DoFoleySounds),
            TimeEvent(25 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/lock_break")
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/breath_out")
            end),
            TimeEvent(26 * FRAMES, DoFoleySounds),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver(function(inst)
                inst.sg.statemem.resurrected = true
                inst.components.health:SetPercent(0.5)
				inst:ShowActions(true)
				inst:ShowHUD(true)
				-- inst:SetCameraDistance()
				inst.sg:RemoveStateTag("busy")
				SerializeUserSession(inst) 
                inst.sg:GoToState("idle")
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.resurrected then
                inst.components.health:SetPercent(0.5)
            end
			inst.components.inventory:Show()
			inst:ShowActions(true)
            inst:ShowHUD(true)
            inst:SetCameraDistance()
			inst:RemoveTag("corpse")
            SerializeUserSession(inst) 
			-------------------
			inst.components.age:ResumeAging()
			inst.components.inventory:Open()
			
			-------------------
            inst:Unchain()
        end,
    },
	
	 State{
        name = "special_atk1",
        tags = { "busy" },

        onenter = function(inst)
            if inst:IsUnchained() then
                inst.sg:GoToState("taunt_roar")
            else
                inst.components.locomotor:StopMoving()
                inst.AnimState:PlayAnimation("taunt1")
            end
        end,

        timeline =
        {
            TimeEvent(0, DoFoleySounds),
            TimeEvent(2 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/breath_in_fast")
            end),
            TimeEvent(9 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/breath_in_fast", nil, .75)
            end),
            TimeEvent(12 * FRAMES, function(inst)
                DoFoleySounds(inst, .5)
            end),
            TimeEvent(25 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/breath_in_fast", nil, .8)
            end),
            TimeEvent(25 * FRAMES, function(inst)
                DoFoleySounds(inst, .7)
            end),
            TimeEvent(32 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/breath_in_fast")
            end),
            TimeEvent(43 * FRAMES, DoFoleySounds),
            TimeEvent(48 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },

    State{
        name = "taunt_roar",
        tags = { "busy" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("taunt2")
        end,

        timeline =
        {
            TimeEvent(11 * FRAMES, function(inst)
                inst.sg:AddStateTag("nofreeze")
                inst.sg:AddStateTag("nosleep")
            end),
            TimeEvent(12 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/taunt")
            end),
            TimeEvent(14 * FRAMES, DoRoarShake),
            TimeEvent(15 * FRAMES, DoRoarAlert),
            CommonHandlers.OnNoSleepTimeEvent(56 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("idle"),
        },
    },
	
	State{
        name = "attack",
        tags = { "attack", "busy" },

        onenter = function(inst)
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			inst.components.combat:SetTarget(target)
			if target ~= nil then
				if target:IsValid() then
					inst:ForceFacePoint(target:GetPosition())
					inst.sg.statemem.attacktarget = target
				end
			end
			if inst.altattack2 and inst.altattack2 == true then
				inst.sg:GoToState("attack_spell", inst.sg.statemem.attacktarget)
			else
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation("attack_doubleclaw")
				inst.components.combat:StartAttack()
				inst.sg.statemem.target = inst.components.combat.target
			end
        end,

        timeline =
        {
            TimeEvent(FRAMES, DoFoleySounds),
            TimeEvent(4 * FRAMES, function(inst)
                inst.sg:AddStateTag("nosleep")
                inst.sg:AddStateTag("nofreeze")
            end),
            TimeEvent(13 * FRAMES, DoSwipeSound),
            TimeEvent(14 * FRAMES, DoFoleySounds),
            TimeEvent(16 * FRAMES, function(inst)
                inst.components.combat:DoAttack(inst.sg.statemem.target)
            end),
            TimeEvent(23 * FRAMES, DoSwipeSound),
            TimeEvent(24 * FRAMES, DoFoleySounds),
            TimeEvent(27 * FRAMES, function(inst)
                inst.components.combat:DoAttack(inst.sg.statemem.target)
            end),
            CommonHandlers.OnNoSleepTimeEvent(40 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("idle"),
        },
    },

    State{
        name = "quickattack",
        tags = { "attack", "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			inst.components.combat:SetTarget(target)
			if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
				end
			end
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("attack_doubleclaw")
            inst.AnimState:SetTime(15 * FRAMES)
            inst.components.combat:StartAttack()
            DoFoleySounds(inst)
            DoSwipeSound(inst)
        end,

        timeline =
        {
            TimeEvent(FRAMES, function(inst)
                inst.components.combat:DoAttack(inst.sg.statemem.target)
            end),
            TimeEvent(8 * FRAMES, DoSwipeSound),
            TimeEvent(9 * FRAMES, DoFoleySounds),
            TimeEvent(12 * FRAMES, function(inst)
                inst.components.combat:DoAttack(inst.sg.statemem.target)
            end),
            CommonHandlers.OnNoSleepTimeEvent(25 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
                end
            end),
        },
    },

	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("walk_pre")
        end,

		timeline =
        {
            TimeEvent(8 * FRAMES, DoFoleySounds),
			TimeEvent(9 * FRAMES, DoFootstep),
        },
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline =
        {
            TimeEvent(20 * FRAMES, DoFoleySounds),
			TimeEvent(21 * FRAMES, DoFootstep),
			TimeEvent(44 * FRAMES, DoFoleySounds),
			TimeEvent(45 * FRAMES, DoFootstep),
        },
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")            
        end,

		timeline =
		{
			TimeEvent(0, function(inst)
				DoFoleySounds(inst)
				DoFootstep(inst, .6)
			end),
			TimeEvent(10 * FRAMES, DoFoleySounds),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
    State{
        name = "attack_chomp",
        tags = { "attack", "busy", "nosleep", "nofreeze" },

        onenter = function(inst, target)
			
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("attack_chomp")
			inst.canchomp = false
            local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			inst.components.combat:SetTarget(target)
			if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
				inst.sg.statemem.speed = CalcChompSpeed(inst, target)
				end
			end
            inst.sg.mem.wantstochomp = nil
			PlayablePets.StartSpecialAttack(inst)
            --inst.components.timer:StartTimer("chomp_cd", inst.chomp_cd)
        end,

        onupdate = function(inst)
            if inst.sg.statemem.jump then
                inst.Physics:SetMotorVel(inst.sg.statemem.speed, 0, 0)
            end
        end,

        timeline =
        {
            TimeEvent(2 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/attack_3")
            end),
            TimeEvent(6 * FRAMES, DoChompShake),
            --TimeEvent(7 * FRAMES, DoRoarAlert),
            TimeEvent(26 * FRAMES, function(inst)
                if inst.sg.statemem.target ~= nil and inst.sg.statemem.target:IsValid() then
                    --update in case target moved, if target still exists
					inst:ForceFacePoint(inst.sg.statemem.target:GetPosition())
                    inst.sg.statemem.speed = CalcChompSpeed(inst, inst.sg.statemem.target)
                end
                if (inst.sg.statemem.speed or 0) > 0 then
                    inst.sg.statemem.jump = true
                    inst.components.locomotor:EnableGroundSpeedMultiplier(false)
                end
            end),
            TimeEvent(33 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/bite")
            end),
            TimeEvent(34 * FRAMES, DoLanding),
            TimeEvent(35 * FRAMES, function(inst)
                if inst.components.combat.target ~= nil then
                    inst.components.combat:SetRange(6)
					inst.components.combat:SetDefaultDamage(CHOMP_DAMAGE)
                    inst.components.combat:DoAttack(inst.sg.statemem.target)
					inst.components.combat:SetDefaultDamage(45 * 2)
					--inst:PerformBufferedAction()
                    inst.components.combat:SetRange(TUNING.KLAUS_ATTACK_RANGE * 1.68, TUNING.KLAUS_HIT_RANGE * 1.68)
                end
            end),
            TimeEvent(36 * FRAMES, function(inst)
                if inst.sg.statemem.jump then
                    inst.sg.statemem.jump = nil
                    inst.components.locomotor:Stop()
                    inst.components.locomotor:EnableGroundSpeedMultiplier(true)
                end
            end),
            CommonHandlers.OnNoSleepTimeEvent(49 * FRAMES, function(inst)
                
                    inst.sg:RemoveStateTag("busy")
                    inst.sg:RemoveStateTag("nosleep")
                    inst.sg:RemoveStateTag("nofreeze")
                
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("idle"),
        },

        onexit = function(inst)
            if inst.sg.statemem.jump then
                inst.sg.statemem.jump = nil
                inst.components.locomotor:Stop()
                inst.components.locomotor:EnableGroundSpeedMultiplier(true)
				
            end
			inst.canchomp = false --failsafe
			if inst.altattack2 and inst.altattack2 == true then
				inst.components.combat:SetRange(20, TUNING.KLAUS_HIT_RANGE * 1.4)
			else
				inst.components.combat:SetRange(TUNING.KLAUS_HIT_RANGE * 1.4)
			end

			if inst.canchomp == false then
				inst:DoTaskInTime(12, function(inst) inst.canchomp = true inst.components.combat:SetRange(30, TUNING.KLAUS_HIT_RANGE * 1.4) end)
			end
        end,
    },
	
	State{
        name = "attack_spell",
        tags = { "attack", "busy", "nosleep", "nofreeze", "nointerrupt" },

        onenter = function(inst, target)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("command_pre")
			inst.AnimState:PushAnimation("command_loop", true)
			inst.altattack2 = false
			inst.components.combat:StartAttack()
			if target ~= nil then
				if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
				end
			end
            
			inst.sg.statemem.fx = SpawnPrefab(inst.magicattack == "fire" and "deer_fire_charge" or "deer_ice_charge")
            inst.sg.statemem.fx.entity:SetParent(inst.entity)
            inst.sg.statemem.fx.entity:AddFollower()
			inst.sg.statemem.fx.Transform:SetScale(1.5, 1.5, 1.5)
            inst.sg.statemem.fx.Follower:FollowSymbol(inst.GUID, "klaus_claw", 0, 20, 0)
			inst.sg.statemem.fx.Transform:SetPosition(0, 20, 0)
        end,
		
		timeline =
        {
            TimeEvent(3 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/huff")
            end),
            TimeEvent(30 * FRAMES, function(inst)
                --DoCast(inst, inst.sg.statemem.attacktarget)
                
            end),
			TimeEvent(45 * FRAMES, function(inst)
				inst.sg.statemem.fx:KillFX("blast" or nil)
				if inst.sg.statemem.attacktarget and inst.sg.statemem.attacktarget:IsValid() then
					DoCast(inst, inst.sg.statemem.attacktarget)
				end
            end),
			TimeEvent(50 * FRAMES, function(inst)
                inst.AnimState:PlayAnimation("command_pst")
            end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },

        onexit = function(inst)
			inst.altattack2 = false --failsafe
			if inst.canchomp == true and inst:IsUnchained() then
				inst.components.combat:SetRange(30, TUNING.KLAUS_HIT_RANGE * 1.4)
				inst:DoTaskInTime(12, function(inst) inst.canchomp = true inst.components.combat:SetRange(30, TUNING.KLAUS_HIT_RANGE * 1.4) end)
			else
				inst.components.combat:SetRange(TUNING.KLAUS_HIT_RANGE * 1.4)
			end
			inst:DoTaskInTime(10, function(inst) inst.altattack2 = true inst.components.combat:SetRange(20, TUNING.KLAUS_HIT_RANGE * 1.4) end)
        end,
    },
	
	 State{
        name = "special_atk2",
        tags = { "busy" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("lol_pre")
			StartLaughing(inst)
        end,

        timeline =
        {
            TimeEvent(FRAMES, function(inst)
                DoFoleySounds(inst, .3)
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg.statemem.laughing = true
                inst.sg:GoToState("laugh_loop")
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.laughing then
                ReduceLaughing(inst, 7)
            end
        end,
    },

    State{
        name = "laugh_loop",
        tags = { "busy"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            if not inst.AnimState:IsCurrentAnimation("lol_loop") then
                inst.AnimState:PlayAnimation("lol_loop", true)
            end
            ReduceLaughing(inst, 1)
            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
			inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/breath_out")
        end,

        timeline =
        {
            TimeEvent(0, DoFoleySounds),
            TimeEvent(13 * FRAMES, function(inst)
                DoFoleySounds(inst, .7)
            end),
        },

        ontimeout = function(inst)
            inst.sg.statemem.laughing = true
            inst.sg:GoToState(inst.sg.mem.laughsremaining ~= nil and "laugh_loop" or "laugh_pst")
        end,

        onexit = function(inst)
            if not inst.sg.statemem.laughing then
                ReduceLaughing(inst, 7)
            end
        end,
    },

    State{
        name = "laugh_pst",
        tags = { "busy" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("lol_pst")
        end,

        timeline =
        {
            TimeEvent(5 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },

    State{
        name = "transition",
        tags = { "transition", "busy", "nosleep", "nofreeze" },

        onenter = function(inst, transition)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("transform_pre")
            inst.sg.statemem.transition = inst.sg.mem.wantstotransition == "enrage" and "enrage" or transition
            if inst.sg.statemem.transition ~= nil then
                inst.sg:AddStateTag(inst.sg.statemem.transition)
                if inst.sg.statemem.transition == "callforhelp" and not inst:SummonHelpers() then
                    inst.sg.statemem.transition = nil
                    inst.sg:RemoveStateTag("callforhelp")
                end
            end
            inst.sg.mem.wantstotransition = nil
        end,

        timeline =
        {
            TimeEvent(6 * FRAMES, DoFoleySounds),
            TimeEvent(9 * FRAMES, DoFootstep),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("transition_loop", inst.sg.statemem.transition)
                end
            end),
        },
    },

    State{
        name = "transition_loop",
        tags = { "transition", "busy", "nosleep", "nofreeze" },

        onenter = function(inst, transition)
            if not inst.AnimState:IsCurrentAnimation("transform_loop") then
                inst.AnimState:PlayAnimation("transform_loop", true)
            end
            inst.sg.statemem.transition = inst.sg.mem.wantstotransition == "enrage" and "enrage" or transition
            if inst.sg.statemem.transition ~= nil then
                inst.sg:AddStateTag(inst.sg.statemem.transition)
            end
            inst.sg.mem.wantstotransition = nil
            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
        end,

        timeline =
        {
            TimeEvent(16 * FRAMES, DoFoleySounds),
            TimeEvent(29 * FRAMES, function(inst)
                DoFoleySounds(inst, .6)
            end),
        },

        ontimeout = function(inst)
            inst.sg:GoToState(
                (inst.sg.statemem.transition == "enrage" and "transition_enrage") or
                (inst.sg.statemem.transition == "callforhelp" and "transition_loop") or
                "transition_pst"
            )
        end,
    },

    State{
        name = "transition_pst",
        tags = { "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("transform_pst2")
            local x, y, z = inst.Transform:GetWorldPosition()
            for i, v in ipairs(TheSim:FindEntities(x, y, z, 20, { "deergemresistance", "_health" }, { "epic", "deer", "INLIMBO" })) do
                if not v.components.health:IsDead() then
                    StartLaughing(inst)
                    break
                end
            end
        end,

        timeline =
        {
            TimeEvent(0, function(inst)
                DoFoleySounds(inst, .4)
            end),
            CommonHandlers.OnNoSleepTimeEvent(7 * FRAMES, function(inst)
                DoFootstep(inst, .5)
                inst.sg:RemoveStateTag("busy")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("idle"),
        },
    },

    State{
        name = "transition_enrage",
        tags = { "enrage", "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("transform_pst")
            --Cancel all other transitions when enraged
            inst.sg.mem.wantstotransition = nil
        end,

        timeline =
        {
            TimeEvent(FRAMES, DoFoleySounds),
            TimeEvent(5 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/common/rebirth_amulet_poof")
                inst:Enrage()
                DoFootstep(inst, .5)
            end),
            TimeEvent(6 * FRAMES, DoFoleySounds),
            CommonHandlers.OnNoSleepTimeEvent(25 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("idle"),
        },

        onexit = function(inst)
            inst:Enrage()
        end,
    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        timeline = 
		{
			TimeEvent(7 * FRAMES, DoScratchSound),
			TimeEvent(14 * FRAMES, function(inst)
            DoFoleySounds(inst, .5)
            DoScratchSound(inst)
        end),
        TimeEvent(19 * FRAMES, DoScratchSound),
        TimeEvent(24 * FRAMES, DoScratchSound),
        TimeEvent(29 * FRAMES, DoScratchSound),
        TimeEvent(33 * FRAMES, function(inst)
            DoFoleySounds(inst, .5)
        end),
        TimeEvent(34 * FRAMES, DoScratchSound),
        TimeEvent(39 * FRAMES, DoScratchSound),
        TimeEvent(55 * FRAMES, function(inst)
            DoFootstep(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/breath_out", nil, .5)
        end),
        TimeEvent(57 * FRAMES, function(inst)
            inst.sg:RemoveStateTag("caninterrupt")
            DoFoleySounds(inst)
        end),
        TimeEvent(58 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/bodyfall")
        end),
        TimeEvent(60 * FRAMES, ShakeIfClose),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("sleep_loop")
		end,
			
		onexit = function(inst)

		end,
        
		timeline=
        {
			 TimeEvent(0, function(inst)
            DoFoleySounds(inst, .3)
        end),
        TimeEvent(7 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/breath_in")
        end),
        TimeEvent(29 * FRAMES, function(inst)
            DoFoleySounds(inst, .2)
        end),
        TimeEvent(32 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/breath_out")
			PlayablePets.SleepHeal(inst)
        end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        timeline = 
		{
			TimeEvent(0, function(inst)
            DoFoleySounds(inst, .3)
        end),
        TimeEvent(3 * FRAMES, DoBodyfall),
        CommonHandlers.OnNoSleepTimeEvent(23 * FRAMES, function(inst)
            inst.sg:RemoveStateTag("busy")
            inst.sg:RemoveStateTag("nosleep")
        end),
        TimeEvent(24 * FRAMES, DoFoleySounds),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
}

CommonStates.AddFrozenStates(states)
local moveanim = "walk"
local idleanim = "idle_loop"
local actionanim = "walk_pst"
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            PlayablePets.DoWork(inst, 8)
        end),
	}, 
	actionanim, nil, nil, "idle_loop", actionanim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(FRAMES, DoFoleySounds),
            TimeEvent(3 * FRAMES, function(inst)
				inst:RemoveTag("corpse")
                DoBodyfall(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/death")
            end),
            TimeEvent(23 * FRAMES, DoFoleySounds),
            TimeEvent(25 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/bodyfall")
                ShakeIfClose(inst)
            end),
            TimeEvent(27 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
            end),
            TimeEvent(3, function(inst)
                if not inst:IsUnchained() then
                    inst.sg.statemem.resurrecting = true
                    inst.sg:GoToState("resurrect")
                end
            end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0, function(inst)
                DoFoleySounds(inst, .3)
                DoScratchSound(inst)
            end),
            TimeEvent(4 * FRAMES, DoBodyfall),
            TimeEvent(10 * FRAMES, function(inst)
                DoFoleySounds(inst, .2)
            end),
            TimeEvent(23 * FRAMES, DoFoleySounds),
            TimeEvent(25 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/lock_break")
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/breath_out")
            end),
            TimeEvent(26 * FRAMES, DoFoleySounds),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "death_pst",
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, actionanim)
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
--PP_CommonStates.AddHomeState(states, nil, "eat", "taunt", true)
CommonStates.AddHopStates(states, false, {pre = moveanim.."_pre", loop = moveanim.."_loop", pst = moveanim.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = actionanim,
	plank_idle_loop = idleanim,
	plank_idle_pst = actionanim,
	
	plank_hop_pre = moveanim.."_pre",
	plank_hop = moveanim.."_loop",
	
	steer_pre = actionanim,
	steer_idle = idleanim,
	steer_turning = actionanim,
	stop_steering = actionanim,
	
	row = actionanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = actionanim,
	
	leap_pre = moveanim.."_pre",
	leap_loop = moveanim.."_loop",
	leap_pst = moveanim.."_pst",
	
	lunge_pre = moveanim.."_pre",
	lunge_loop = moveanim.."_loop",
	lunge_pst = moveanim.."_pst",
	
	superjump_pre = moveanim.."_pre",
	superjump_loop = moveanim.."_loop",
	superjump_pst = moveanim.."_pst",
	
	castspelltime = 10,
})

return StateGraph("klausp", states, events, "idle", actionhandlers)

