require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.REVIVE_CORPSE, "idle"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "notarget", "INLIMBO"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget"}
	end
end

local function OnBurrow(inst)
    inst:AddTag("invisible")
	inst:AddTag("notarget")
	inst.components.health:SetInvincible(true)
	inst.noactions = true
	inst:RemoveTag("scarytoprey")
    inst.components.locomotor:SetExternalSpeedMultiplier(inst, 3, 3)   
end

local function OnUnborrow(inst)
    inst.specialsleep = true
    inst.noactions = nil
	inst:RemoveTag("invisible")
	inst:RemoveTag("notarget")
	inst:AddTag("scarytoprey") 
    inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, 3)
	inst.components.health:SetInvincible(false)
end

local function DestroyStuff(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 4, nil, { "INLIMBO", "mushroomsprout", "NET_workable" })
    for i, v in ipairs(ents) do
        if v:IsValid() and
            v.components.workable ~= nil and
            v.components.workable:CanBeWorked() and
            v.components.workable.action ~= ACTIONS.NET then
            SpawnPrefab("collapse_small").Transform:SetPosition(v.Transform:GetWorldPosition())
            v.components.workable:Destroy(inst)
			--if v:IsValid() and not (v:HasTag("tree") or v:HasTag("boulder")) then
				--print("LOGGED: Toadstool player destoyed "..v.prefab) --useful for admins to indentify greifing, find out how to print names instead.
			--end
        end
    end
end

local function ClearRecentlyBounced(inst, other)
    inst.sg.mem.recentlybounced[other] = nil
end

local function SmallLaunch(inst, launcher, basespeed)
    local hp = inst:GetPosition()
    local pt = launcher:GetPosition()
    local vel = (hp - pt):GetNormalized()
    local speed = basespeed * 2 + math.random() * 2
    local angle = math.atan2(vel.z, vel.x) + (math.random() * 20 - 10) * DEGREES
    inst.Physics:Teleport(hp.x, .1, hp.z)
    inst.Physics:SetVel(math.cos(angle) * speed, 1.5 * speed + math.random(), math.sin(angle) * speed)

    launcher.sg.mem.recentlybounced[inst] = true
    launcher:DoTaskInTime(.6, ClearRecentlyBounced, inst)
end

local function BounceStuff(inst)
    if inst.sg.mem.recentlybounced == nil then
        inst.sg.mem.recentlybounced = {}
    end
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 6, { "_inventoryitem" }, { "locomotor", "INLIMBO" })
    for i, v in ipairs(ents) do
        if v:IsValid() and not (v.components.inventoryitem.nobounce or inst.sg.mem.recentlybounced[v]) and v.Physics ~= nil then
            local distsq = v:GetDistanceSqToPoint(x, y, z)
            local intensity = math.clamp((36 - distsq) / 27 --[[(36 - 9)]], 0, 1)
            SmallLaunch(v, inst, intensity)
        end
    end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .7, .02, .5, inst, 40)
    BounceStuff(inst)
end

local function DoFootstep(inst)
    inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/step_soft")
end

local function DoStompstep(inst)
    inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/step_stomp")
    ShakeAllCameras(CAMERASHAKE.FULL, .35, .02, .7, inst, 40)
    DestroyStuff(inst)
    BounceStuff(inst)
end

local function DoRoarShake(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .7, .02, .3, inst, 40)
    BounceStuff(inst)
end

local function DoChannelingShake(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, 6 * FRAMES, .02, 0.2, inst, 40)
    BounceStuff(inst)
end

local function DoSporeBombShake(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .35, .02, .3, inst, 40)
    BounceStuff(inst)
end

local function DoMushroomBombShake(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .35, .02, .3, inst, 40)
    BounceStuff(inst)
end

local function DoPoundShake(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .35, .02, 1, inst, 40)
    BounceStuff(inst)
end

--------------------------------------------------------------------------
--"nosleep" helpers

local function nosleep_gotosleep()
    return EventHandler("gotosleep", function(inst)
        inst.sg.statemem.sleeping = true
    end)
end

local function nosleep_onwakeup()
    return EventHandler("onwakeup", function(inst)
        inst.sg.statemem.sleeping = false
    end)
end

local function nosleep_animover(state)
    return EventHandler("animover", function(inst)
        if inst.AnimState:AnimDone() then
            inst.sg:GoToState(inst.sg.statemem.sleeping and "sleep" or state)
        end
    end)
end

--------------------------------------------------------------------------

local function ChooseAttack(inst)
	if inst.isdark ~= nil then
	if not inst.components.timer:TimerExists("sporebombp_cd") then
        local targets = inst:FindSporeBombTargets()
        if #targets > 0 then
            inst.sg:GoToState("sporebomb", targets)
            return true
        end
    end
	
	else
    if not inst.components.timer:TimerExists("sporebomb_cd") then
        local targets = inst:FindSporeBombTargets()
        if #targets > 0 then
            inst.sg:GoToState("sporebomb", targets)
            return true
        end
    end
	end
    if not inst.components.timer:TimerExists("mushroombomb_cd") then
        inst.sg:GoToState("mushroombomb")
        return true
    end
    return false
end

local function OnTickChannel(inst)
    inst:DoMushroomSprout(inst.sg.mem.mushroomsprout_angles)
    inst.components.epicscare:Scare(TUNING.TOADSTOOL_MUSHROOMSPROUT_TICK + 5)
    inst.components.timer:StartTimer("channeltick", TUNING.TOADSTOOL_MUSHROOMSPROUT_TICK)
end

local function OnStartChannel(inst)
    if inst.sg.mem.mushroomsprout_angles == nil then
        --Get new set of angles at start of new channel, or resuming after load
        inst.sg.mem.mushroomsprout_angles = inst:FindMushroomSproutAngles()
    end
    if inst.components.timer:TimerExists("channel") then
        inst.components.timer:ResumeTimer("channel")
        inst.components.timer:ResumeTimer("channeltick")
    else
        inst.components.timer:StartTimer("channel", TUNING.TOADSTOOL_MUSHROOMSPROUT_DURATION)
        OnTickChannel(inst)
    end
end

local function OnEndChannel(inst)
    inst.SoundEmitter:KillSound("channel")
    if inst.sg.mem.channelshaketask ~= nil then
        inst.sg.mem.channelshaketask:Cancel()
        inst.sg.mem.channelshaketask = nil
    end
    inst.components.health:SetAbsorptionAmount(0)
    if inst.components.timer:TimerExists("channel") then
        inst.components.timer:PauseTimer("channel")
        inst.components.timer:PauseTimer("channeltick")
    else
        inst.components.timer:StopTimer("channeltick")
        inst.components.timer:StartTimer("mushroomsprout_cd", inst.mushroomsprout_cd)
        inst.sg.mem.mushroomsprout_angles = nil
    end
end

--------------------------------------------------------------------------

local function StartDeath(inst)
    --inst.sg.statemem.fade = 1
	PlayablePets.DoDeath(inst)
end

local function StartFade(inst)
    inst.sg.statemem.fade = 1
end

local function UpdateFade(inst)
    if inst.sg.statemem.fade ~= nil then
        if inst.sg.statemem.fade <= .05 then
            inst.Light:Enable(false)
        else
            local k = inst.sg.statemem.fade - .05
            inst.sg.statemem.fade = k
            k = k * k
            inst.Light:SetRadius(2 * k)
            inst.Light:SetFalloff(1 - .5 * k)
            inst.Light:SetIntensity(.75 * k)
        end
    end
end

local function CancelFade(inst)
    if (inst.sg.statemem.fade or 1) < 1 then
        inst.Light:SetRadius(2)
        inst.Light:SetFalloff(.5)
        inst.Light:SetIntensity(.75)
        inst.Light:Enable(true)
    end
end

--------------------------------------------------------------------------

local events=
{
    EventHandler("attacked", function(inst)
        if not (inst.sg:HasStateTag("busy") or inst.components.health:IsDead()) and
            (   inst.sg.mem.last_hit_time == nil or
                inst.sg.mem.last_hit_time + inst.hit_recovery < GetTime()
            ) then
            inst.sg:GoToState("hit")
        end
    end),
	PP_CommonHandlers.AddCommonHandlers(),
    --EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
	EventHandler("doattack", function(inst)
        if not (inst.sg:HasStateTag("busy") or inst.components.health:IsDead()) then
            ChooseAttack(inst)
        end
    end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnDeath(),
	PP_CommonHandlers.OnLocomoteAdvanced(),
	EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}


 local states=
{

	 State{
        name = "idle",
        tags = { "idle" },

        onenter = function(inst)
            if inst.sg.mem.wantstoburrow then
                inst.sg:GoToState("burrow")
            elseif inst.sg.mem.wantstoroar then
                inst.sg:GoToState("roar")
            else
                inst.Physics:Stop()
                inst.AnimState:PlayAnimation("idle", true)
            end
        end,
    },
	
	State{
        name = "special_atk1",
        tags = { "roar", "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("phase_transition")
            inst.sg.mem.wantstoroar = nil
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/roar_phase")
                DoRoarShake(inst)
            end),
            TimeEvent(9 * FRAMES, function(inst)
                inst.components.epicscare:Scare(5)
            end),
            TimeEvent(21 * FRAMES, DoRoarShake),
            TimeEvent(22 * FRAMES, function(inst)
                inst.components.epicscare:Scare(5)
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },

    State{
        name = "walk_start",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("walk")
                end
            end),
        },
    },

    State{
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk")
        end,

        timeline =
        {
            TimeEvent(0 * FRAMES, DoFootstep),
            TimeEvent(10 * FRAMES, DoStompstep),
            TimeEvent(19 * FRAMES, DoFootstep),
            TimeEvent(30 * FRAMES, DoStompstep),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("walk")
                end
            end),
        },
    },

    State{
        name = "walk_stop",
        tags = { "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("walk_pst")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	 State{
        name = "attack",
        tags = {},

        onenter = function(inst)
            ChooseAttack(inst)
        end,

        events =
        {

        },
    },

    State{
        name = "surface",
        tags = { "busy", "nosleep", "nofreeze", "noattack" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.components.health:SetInvincible(true)
            inst.AnimState:PlayAnimation("spawn_appear_toad")
            inst.AnimState:SetLightOverride(0)
            inst.DynamicShadow:Enable(false)
            inst.Light:Enable(false)
        end,

        timeline =
        {
            TimeEvent(0 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/spawn_appear_pre")
            end),
            TimeEvent(10 * FRAMES, function(inst)
                ShakeAllCameras(CAMERASHAKE.VERTICAL, 40 * FRAMES , .03, 2, inst, 40)
            end),
            TimeEvent(12 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/spawn_appear")
                inst.AnimState:SetLightOverride(.3)
                inst.DynamicShadow:Enable(true)
                inst.Light:Enable(true)
            end),
            TimeEvent(31 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
            end),
            TimeEvent(32 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/dustpoof")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("special_atk1")
            end),
        },

        onexit = function(inst)
            inst.components.health:SetInvincible(false)
            inst.AnimState:SetLightOverride(.3)
            inst.DynamicShadow:Enable(true)
            inst.Light:Enable(true)
        end,
    },

    State{
        name = "burrow",
        tags = { "busy", "nosleep", "nofreeze", "noattack" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.components.health:SetInvincible(true)
            inst.AnimState:PlayAnimation("reset")
        end,

        timeline =
        {
            TimeEvent(11 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/roar")
            end),
            TimeEvent(19 * FRAMES, function(inst)
                inst.DynamicShadow:Enable(false)
            end),
            TimeEvent(20 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/spawn_appear")
            end),
            TimeEvent(21 * FRAMES, function(inst)
                ShakeAllCameras(CAMERASHAKE.VERTICAL, 20 * FRAMES , .03, 2, inst, 40)
            end),
            TimeEvent(40 * FRAMES, function(inst)
                ShakeAllCameras(CAMERASHAKE.VERTICAL, 30 * FRAMES , .03, .7, inst, 40)
            end),
            TimeEvent(48 * FRAMES, StartFade),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    --inst:OnEscaped()
                end
            end),
        },

        onupdate = UpdateFade,

        onexit = function(inst)
            --Should NOT happen!
            inst.components.health:SetInvincible(false)
            inst.DynamicShadow:Enable(true)
            CancelFade(inst)
        end,
    },

    State{
        name = "hit",
        tags = { "hit", "busy" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/hit")
            inst.sg.mem.last_hit_time = GetTime()
        end,

        timeline =
        {
            TimeEvent(12 * FRAMES, function(inst)
                if inst.sg.statemem.doattack then
                    if not inst.components.health:IsDead() and ChooseAttack(inst) then
                        return
                    end
                    inst.sg.statemem.doattack = nil
                end
                inst.sg:RemoveStateTag("busy")
            end),
        },

        events =
        {
            EventHandler("doattack", function(inst)
                inst.sg.statemem.doattack = true
            end),
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    if inst.sg.statemem.doattack and ChooseAttack(inst) then
                        return
                    end
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },

    State{
        name = "channel_pre",
        tags = { "busy", "channeling" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("attack_channeling_pre")
        end,

        timeline =
        {
            TimeEvent(14 * FRAMES, ShakeIfClose),
        },

        events =
        {
            EventHandler("attacked", function(inst)
                if not inst.components.health:IsDead() and
                    (   inst.sg.mem.last_hit_time == nil or
                        inst.sg.mem.last_hit_time + 1 < GetTime()
                    ) then
                    inst.sg:GoToState("channel_hit")
                end
            end),
            EventHandler("roar", function(inst)
                if not inst.components.health:IsDead() then
                    inst.sg:GoToState("channel_roar")
                end
            end),
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("channel")
                end
            end),
        },
    },

    State{
        name = "channel",
        tags = { "busy", "channeling" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("attack_channeling_loop", true)
            if not inst.SoundEmitter:PlayingSound("channel") then
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/channeling_LP", "channel")
                inst.SoundEmitter:SetParameter("channel", "intensity", 0)
            end
            if inst.sg.mem.channelshaketask ~= nil then
                inst.sg.mem.channelshaketask:Cancel()
            end
            inst.sg.mem.channelshaketask = inst:DoPeriodicTask(inst.AnimState:GetCurrentAnimationLength(), DoChannelingShake, 0)
            inst.components.health:SetAbsorptionAmount(TUNING.TOADSTOOL_VULNERABLE_MULT)
            OnStartChannel(inst)
        end,

        onupdate = function(inst)
            inst.SoundEmitter:SetParameter("channel", "intensity", 1)
        end,

        events =
        {
            EventHandler("attacked", function(inst)
                if not inst.components.health:IsDead() and
                    (   inst.sg.mem.last_hit_time == nil or
                        inst.sg.mem.last_hit_time + 1 < GetTime()
                    ) then
                    inst.sg.statemem.continuechannel = true
                    inst.sg:GoToState("channel_hit")
                end
            end),
            EventHandler("roar", function(inst)
                if not inst.components.health:IsDead() then
                    inst.sg.statemem.continuechannel = true
                    inst.sg:GoToState("channel_roar")
                end
            end),
            EventHandler("timerdone", function(inst, data)
                if not inst.components.health:IsDead() then
                    if data.name == "channeltick" then
                        OnTickChannel(inst)
                    elseif data.name == "channel" then
                        inst.sg:GoToState("channel_pst")
                    end
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.continuechannel then
                OnEndChannel(inst)
            end
        end,
    },

    State{
        name = "channel_hit",
        tags = { "hit", "busy", "channeling" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("hit_channeling")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/hit")
            inst.sg.mem.last_hit_time = GetTime()
        end,

        timeline =
        {
            TimeEvent(12 * FRAMES, function(inst)
                if not inst.components.health:IsDead() then
                    if inst.sg.statemem.doattack then
                        if ChooseAttack(inst) then
                            return
                        end
                        inst.sg.statemem.doattack = nil
                    end
                    if inst.sg.statemem.stopped then
                        inst.sg:GoToState("channel_pst")
                        return
                    end
                end
                inst.sg.statemem.endstun = true
            end),
        },

        events =
        {
            EventHandler("doattack", function(inst)
                if inst.sg.statemem.endstun and not inst.components.health:IsDead() then
                    ChooseAttack(inst)
                else
                    inst.sg.statemem.doattack = true
                end
            end),
            EventHandler("timerdone", function(inst, data)
                if data.name == "channeltick" then
                    if not inst.components.health:IsDead() then
                        OnTickChannel(inst)
                    end
                elseif data.name == "channel" then
                    if inst.sg.statemem.endstun and not inst.components.health:IsDead() then
                        inst.sg:GoToState("channel_pst")
                    else
                        inst.sg.statemem.stopped = true
                    end
                end
            end),
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    if inst.sg.statemem.doattack and ChooseAttack(inst) then
                        return
                    elseif inst.sg.statemem.stopped then
                        inst.sg:GoToState("channel_pst")
                    else
                        inst.sg.statemem.continuechannel = true
                        inst.sg:GoToState("channel")
                    end
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.continuechannel then
                OnEndChannel(inst)
            end
        end,
    },

    State{
        name = "channel_roar",
        tags = { "roar", "busy", "channeling", "nosleep", "nofreeze" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("phase_transition")
            if inst.sg.mem.channelshaketask ~= nil then
                inst.sg.mem.channelshaketask:Cancel()
                inst.sg.mem.channelshaketask = nil
            end
            inst.sg.mem.wantstoroar = nil
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/roar_phase")
                DoRoarShake(inst)
            end),
            TimeEvent(9 * FRAMES, function(inst)
                inst.components.epicscare:Scare(5)
            end),
            TimeEvent(21 * FRAMES, DoRoarShake),
            TimeEvent(22 * FRAMES, function(inst)
                inst.components.epicscare:Scare(5)
            end),
            TimeEvent(40 * FRAMES, function(inst)
                if not inst.components.health:IsDead() then
                    if inst.sg.statemem.stopped then
                        inst.sg:GoToState("channel_pst")
                    else
                        inst.sg.statemem.continuechannel = true
                        inst.sg:GoToState("channel")
                    end
                end
            end),
        },

        events =
        {
            EventHandler("timerdone", function(inst, data)
                if data.name == "channeltick" then
                    if not inst.components.health:IsDead() then
                        OnTickChannel(inst)
                    end
                elseif data.name == "channel" then
                    inst.sg.statemem.stopped = true
                end
            end),
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    if inst.sg.statemem.stopped then
                        inst.sg:GoToState("channel_pst")
                    else
                        inst.sg.statemem.continuechannel = true
                        inst.sg:GoToState("channel")
                    end
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.continuechannel then
                OnEndChannel(inst)
            end
        end,
    },

    State{
        name = "channel_pst",
        tags = { "busy" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("attack_channeling_pst")
        end,

        timeline =
        {
            TimeEvent(15 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },

    State{
        name = "sporebomb",
        tags = { "attack", "busy", "sporebombing", "canrotate" },

        onenter = function(inst, targets)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("attack_infection")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/channeling_LP", "channel")
            inst.SoundEmitter:SetParameter("channel", "intensity", 1)
            inst.components.combat:StartAttack()
            inst.sg.statemem.targets = targets
            if #targets > 0 then
                inst:ForceFacePoint(targets[1].Transform:GetWorldPosition())
            end
        end,

        timeline =
        {
            TimeEvent(15 * FRAMES, function(inst)
                inst.sg:AddStateTag("nosleep")
                inst.sg:AddStateTag("nofreeze")
            end),
            TimeEvent(18 * FRAMES, function(inst)
                inst.SoundEmitter:KillSound("channel")
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/infection_attack")
            end),
            TimeEvent(21 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/infection_attack_pre")
            end),
            TimeEvent(22 * FRAMES, function(inst)
                DoSporeBombShake(inst)
                inst:DoSporeBomb(inst.sg.statemem.targets)
                inst.components.timer:StartTimer("sporebomb_cd", inst.sporebomb_cd)
            end),
            TimeEvent(43 * FRAMES, function(inst)
                if inst.sg.statemem.sleeping and not inst.components.health:IsDead() then
                    inst.sg:GoToState("sleep")
                else
                    inst.sg:RemoveStateTag("busy")
                    inst.sg:RemoveStateTag("nosleep")
                    inst.sg:RemoveStateTag("nofreeze")
                end
            end),
        },

        events =
        {
            nosleep_gotosleep(),
            nosleep_onwakeup(),
            nosleep_animover("idle"),
        },

        onexit = function(inst)
            inst.SoundEmitter:KillSound("channel")
        end,
    },

    State{
        name = "mushroombomb",
        tags = { "attack", "busy", "mushroombombing", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("attack_basic")
            inst.components.combat:StartAttack()
        end,

        timeline =
        {
            TimeEvent(0 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/roar")
            end),
            TimeEvent(10 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/spore_shoot")
            end),
            TimeEvent(12 * FRAMES, function(inst)
                inst.sg:AddStateTag("nosleep")
                inst.sg:AddStateTag("nofreeze")
            end),
            TimeEvent(14 * FRAMES, function(inst)
                DoMushroomBombShake(inst)
                inst:DoMushroomBomb()
                inst.sg.mem.mushroombomb_chains = (inst.sg.mem.mushroombomb_chains or 0) + 1
                if inst.sg.mem.mushroombomb_chains >= inst.mushroombomb_maxchain then
                    inst.sg.mem.mushroombomb_chains = 0
                    inst.components.timer:StartTimer("mushroombomb_cd", inst.mushroombomb_cd)
                end
            end),
        },

        events =
        {
            nosleep_gotosleep(),
            nosleep_onwakeup(),
            nosleep_animover("idle"),
        },
    },

    State{
        name = "special_atk2",
        tags = { "attack", "busy", "pounding", "nosleep", "nofreeze" },

        onenter = function(inst)
			if inst.specialatk2 == true then
                inst.components.locomotor:StopMoving()
                inst.AnimState:PlayAnimation("attack_pound_pre")
			else
			    inst.sg:GoToState("idle")
			end            
        end,

        timeline =
        {
            TimeEvent(11 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/roar")
            end),
            TimeEvent(37 * FRAMES, function(inst)
                DoPoundShake(inst)
				PlayablePets.StartSpecialAttack(inst)
                inst.components.groundpounder:GroundPound()
				inst.components.combat:DoAreaAttack(inst, 8.5, nil, nil, "strong", GetExcludeTags(inst))
				local cooldown = inst.level * 2
				inst.specialatk2 = false
				inst:DoTaskInTime(TheNet:GetServerGameMode() == "forge" and PPNR_FORGE.TOADSTOOL.SPECIAL_CD or (10 - cooldown), function(inst) inst.specialatk2 = true end)
                inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
            end),
        },

        events =
        {
            nosleep_gotosleep(),
            nosleep_onwakeup(),
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("pound", inst.sg.statemem.sleeping)
                end
            end),
        },
    },

    State{
        name = "pound",
        tags = { "attack", "busy", "pounding", "nosleep", "nofreeze" },

        onenter = function(inst, sleeping)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("attack_pound_loop")
            inst.sg.statemem.sleeping = sleeping
			inst.components.combat:SetRange(7, 7)
        end,

        timeline =
        {
            TimeEvent(8 * FRAMES, function(inst)
                TheWorld:PushEvent("ms_miniquake", { rad = 20, num = 20, duration = 2.5, target = inst })
                inst.components.groundpounder:GroundPound()
				PlayablePets.StartSpecialAttack(inst)
                BounceStuff(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
				inst.components.combat:DoAreaAttack(inst, 8.5, nil, nil, "strong", GetExcludeTags(inst))
            end),
        },
		
		onexit = function(inst)
			inst.components.combat:SetRange(TUNING.TOADSTOOL_ATTACK_RANGE)

			
		end,	
		
        events =
        {
            nosleep_gotosleep(),
            nosleep_onwakeup(),
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("pound_pst", inst.sg.statemem.sleeping)
                end
            end),
        },
    },

    State{
        name = "pound_pst",
        tags = { "attack", "busy", "pounding", "nosleep", "nofreeze" },

        onenter = function(inst, sleeping)
            inst.AnimState:PlayAnimation("attack_pound_pst")
            inst.sg.statemem.sleeping = sleeping
        end,

        timeline =
        {
            TimeEvent(5 * FRAMES, function(inst)
                if inst.sg.statemem.sleeping and not inst.components.health:IsDead() then
                    inst.sg:GoToState("sleep")
                else
                    inst.sg:RemoveStateTag("busy")
                    inst.sg:RemoveStateTag("nosleep")
                    inst.sg:RemoveStateTag("nofreeze")
                end
            end),
        },

        events =
        {
            nosleep_gotosleep(),
            nosleep_onwakeup(),
            nosleep_animover("idle"),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("death")
            inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/dragonfly/death")
            inst.sg:SetTimeout((inst.components.health.destroytime or 4) - FRAMES)
		end,	

		timeline =
        {
            TimeEvent(4 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/channeling_LP", "channel")
            end),
            TimeEvent(23 * FRAMES, function(inst)
                inst.SoundEmitter:KillSound("channel")
            end),
            TimeEvent(24 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/roar")
            end),
            TimeEvent(35 * FRAMES, function(inst)
                ShakeIfClose(inst)
                inst.components.lootdropper:DropLoot(inst:GetPosition())
				inst.components.inventory:DropEverything(true)
            end),
            TimeEvent(36 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/death_fall")
            end),
            TimeEvent(52 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/death_roar")
            end),
        },	
			
		ontimeout = function(inst)
			PlayablePets.DoDeath(inst)
		end,


        onupdate = UpdateFade,

        onexit = function(inst)
            --Should NOT happen!
            inst.SoundEmitter:KillSound("channel")
            --CancelFade(inst)
        end,

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        timeline = 
		{
			TimeEvent(45 * FRAMES, function(inst)
            inst.sg:RemoveStateTag("caninterrupt")
        end),
        TimeEvent(46 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/step_stomp")
            ShakeIfClose(inst)
        end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				--if inst.components.sanity.current >= 50 then
				inst.AnimState:PlayAnimation("sleep_loop")
				PlayablePets.SleepHeal(inst)
			end,
			
		onexit = function(inst)
		--inst.components.sanity.dapperness = -0.5
		end,
        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
		
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) 
			inst.sg:GoToState("idle") end),
        },
    },	
}

local moveanim = "walk"
local idleanim = "idle"
local actionanim = "walk_pst"
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            PlayablePets.DoWork(inst, 3)
        end),
	}, 
	actionanim, nil, nil, "idle", actionanim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(4 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/channeling_LP", "channel")
            end),
            TimeEvent(23 * FRAMES, function(inst)
                inst.SoundEmitter:KillSound("channel")
            end),
            TimeEvent(24 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/roar")
            end),
            TimeEvent(36 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/death_fall")
            end),
            TimeEvent(52 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/death_roar")
            end),
		},
		
		corpse_taunt =
		{
			TimeEvent(8 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/roar_phase")
                DoRoarShake(inst)
            end),
            TimeEvent(21 * FRAMES, DoRoarShake),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "phase_transition",
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, actionanim)
PP_CommonStates.AddOpenGiftStates(states, idleanim)
--PP_CommonStates.AddHomeState(states, nil, "eat", "taunt", true)
CommonStates.AddHopStates(states, false, {pre = moveanim.."_pre", loop = moveanim, pst = moveanim.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = actionanim,
	plank_idle_loop = idleanim,
	plank_idle_pst = actionanim,
	
	plank_hop_pre = moveanim.."_pre",
	plank_hop = moveanim.."_loop",
	
	steer_pre = actionanim,
	steer_idle = idleanim,
	steer_turning = actionanim,
	stop_steering = actionanim,
	
	row = actionanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = actionanim,
	
	leap_pre = moveanim.."_pre",
	leap_loop = moveanim.."_loop",
	leap_pst = moveanim.."_pst",
	
	lunge_pre = moveanim.."_pre",
	lunge_loop = moveanim.."_loop",
	lunge_pst = moveanim.."_pst",
	
	superjump_pre = moveanim.."_pre",
	superjump_loop = moveanim.."_loop",
	superjump_pst = moveanim.."_pst",
	
	castspelltime = 10,
})

    
return StateGraph("toadstoolp", states, events, "surface", actionhandlers)

