require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action4"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		local buffaction = inst:GetBufferedAction()
		local target = buffaction ~= nil and buffaction.target or nil
		if target and inst:IsNear(target, 4) then
			return "attack_swipe"
		else
			return "attack"
		end
	end),
	ActionHandler(ACTIONS.PICKUP, "action2"),
	ActionHandler(ACTIONS.DROP, "action3"),
	ActionHandler(ACTIONS.GIVE, "action3"),
	ActionHandler(ACTIONS.GIVETOPLAYER, "action3"),
	ActionHandler(ACTIONS.GIVEALLTOPLAYER, "action3"),
	ActionHandler(ACTIONS.REVIVE_CORPSE, "idle"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end
------------------
local MAX_LOOPS = 3

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .5, .02, .15, inst, 30)
end

local function ShakeCasting(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .3, .02, 1, inst, 30)
end

local function ShakeRaising(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, 1, .02, 1, inst, 30)
end

--------------------------------------------------------------------------

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"playerghost", "notarget", "shadow", "antlion", "wall", "DECOR", "INLIMBO"}
	elseif PP_FORGE_ENABLED then
		return {"player", "companion", "playerghost", "antlion", "wall", "DECOR", "INLIMBO"}
	else	
		return {"player", "companion", "playerghost", "notarget", "antlion", "wall", "DECOR", "INLIMBO"}
	end
end

local function DoAoe(inst, range, multiplier)
	local posx, posy, posz = inst.Transform:GetWorldPosition()
	local ents = TheSim:FindEntities(posx, 0, posz, range or 5, {"locomotor"}, GetExcludeTags(inst))
	for _,ent in ipairs(ents) do
		if ent ~= inst and inst.components.combat:IsValidTarget(ent) and ent.components.health and ent ~= inst.components.combat.target then
			inst:PushEvent("onareaattackother", { target = ent--[[, weapon = self.inst, stimuli = self.stimuli]] })
			ent.components.combat:GetAttacked(inst, inst.components.combat:CalcDamage(ent) * (multiplier or 1))
		end
	end
end

local function ChooseAttack(inst)
    local target = inst.components.combat.target
    if target ~= nil and target:IsNear(inst, TUNING.ANTLION_CAST_RANGE) then
        if inst.altattack == false then
			if inst:IsNear(target, 4.5) then
				inst.sg:GoToState("attack_swipe", target)
			else
				inst.sg:GoToState("summonspikes", target)
			end
        else
            inst.sg:GoToState("summonwall")
        end
        return true
    end
    return false
end

--------------------------------------------------------------------------
--See sand_spike.lua
local SPIKE_SIZES =
{
    "short",
    "med",
    "tall",
}

local SPIKE_RADIUS =
{
    ["short"] = .2,
    ["med"] = .4,
    ["tall"] = .6,
    ["block"] = 1.1,
}

local function CanSpawnSpikeAt(pos, size)
    local radius = SPIKE_RADIUS[size]
    for i, v in ipairs(TheSim:FindEntities(pos.x, 0, pos.z, radius + 1.5, nil, { "antlion_sinkhole" }, { "groundspike", "antlion_sinkhole_blocker" })) do
        if v.Physics == nil then
            return false
        end
        local spacing = radius + v.Physics:GetRadius()
        if v:GetDistanceSqToPoint(pos) < spacing * spacing then
            return false
        end
    end
    return true
end

local function SpawnSpikes(inst, pos, count)
    for i = #SPIKE_SIZES, 1, -1 do
        local size = SPIKE_SIZES[i]
        if CanSpawnSpikeAt(pos, size) then
            local spike = SpawnPrefab("sandspike_"..size)
			spike.Transform:SetPosition(pos:Get())
            count = count - 1
            break
        end
    end
    if count > 0 then
        local dtheta = PI * 2 / count
        for theta = math.random() * dtheta, PI * 2, dtheta do
            local size = SPIKE_SIZES[math.random(#SPIKE_SIZES)]
            local offset = FindWalkableOffset(pos, theta, 2 + math.random() * 2, 3, false, true,
                function(pt)
                    return CanSpawnSpikeAt(pt, size)
                end)
            if offset ~= nil then
                SpawnPrefab("sandspike_"..size).Transform:SetPosition(pos.x + offset.x, 0, pos.z + offset.z)
            end
        end
    end
end

local function SpawnBlock(inst, x, z)
    SpawnPrefab("sandblock").Transform:SetPosition(x, 0, z)
end

local function SpawnBlocks(inst, pos, count)
    if count > 0 then
        local dtheta = PI * 2 / count
        local thetaoffset = math.random() * PI * 2
        for theta = math.random() * dtheta, PI * 2, dtheta do
            local offset = FindWalkableOffset(pos, theta + thetaoffset, 8 + math.random(), 3, false, true,
                function(pt)
                    return CanSpawnSpikeAt(pt, "block")
                end)
            if offset ~= nil then
                if theta < dtheta then
                    SpawnBlock(inst, pos.x + offset.x, pos.z + offset.z)
                else
                    inst:DoTaskInTime(math.random() * .5, SpawnBlock, pos.x + offset.x, pos.z + offset.z)
                end
            end
        end
    end
end

--------------------------------------------------------------------------

local events=
{
    EventHandler("attacked", function(inst)
        if not inst.components.health:IsDead() and
            (   not inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("caninterrupt")
            ) and
            (   inst.sg.mem.last_hit_time == nil or
                inst.sg.mem.last_hit_time + TUNING.ANTLION_HIT_RECOVERY < GetTime()
            ) then
            inst.sg:GoToState("hit")
        end
    end),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
    --EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    EventHandler("doattack", function(inst)
        if not (inst.sg:HasStateTag("busy") or inst.components.health:IsDead()) then
            ChooseAttack(inst)
        end
    end),
	CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},

        onenter = function(inst)
			if not inst.noactions then
				inst.components.locomotor:StopMoving()
				inst.components.locomotor.disable = true
				inst.AnimState:PlayAnimation("idle", true)
			else
				inst.components.locomotor.disable = false
			end
        end,
    },
	
	State
	{
		name = "rocktribute",
		tags = {"busy"},

		onenter = function(inst, data)
			inst.AnimState:PlayAnimation("eat")
			inst.sg.statemem.tributepercent = data ~= nil and data.tributepercent or 0
		end,
        
        timeline =
        {
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/eat") end),
			TimeEvent(36*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/eat") end),
			TimeEvent(71*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/swallow") end),
        },

		events = 
		{
            EventHandler("animover", function(inst) 
				if inst.AnimState:AnimDone() then 
					local level = 
					inst.sg:GoToState(inst:GetRageLevel() == 1 and "hightributeresponse" or "idle")
				end
			end),
		},
	},

	State
	{
		name = "lowtributeresponse",
		tags = {"busy"},

		onenter = function(inst, data)
			inst.AnimState:PlayAnimation("taunt")
		end,
        
        timeline =
        {
			TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/sfx/taunt") end),
        },

		events = 
		{
            EventHandler("animover", function(inst) if inst.AnimState:AnimDone() then inst.sg:GoToState("idle") end end),
		},
	},

	State
	{
		name = "hightributeresponse",
		tags = {"busy"},

		onenter = function(inst, data)
			inst.AnimState:PlayAnimation("full_pre")
			inst.AnimState:PushAnimation("full_loop", false)
			inst.AnimState:PushAnimation("full_pst", false)
		end,
        
        timeline =
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/purr") end),
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/rub") end),
			TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/rub") end),
			TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/rub") end),
			TimeEvent(46*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/rub") end),
        },

		events = 
		{
            EventHandler("animqueueover", function(inst) if inst.AnimState:AnimDone() then inst.sg:GoToState("idle") end end),
		},
	},

	State
	{
		name = "refusetribute",
		tags = {"busy"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("unimpressed")
		end,

        timeline =
        {
			TimeEvent(54*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/unimpressed") end),
        },

		events = 
		{
            EventHandler("animover", function(inst) if inst.AnimState:AnimDone() then inst.sg:GoToState("idle") end end),
		},
	},

	State
	{
		name = "awardtribute",
		tags = {"busy"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("spit")
		end,

        timeline =
        {
			TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/spit") end),
			TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/attack_pre") end),
            TimeEvent(26*FRAMES, function(inst) inst:PerformBufferedAction() end),
			TimeEvent(60*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/unimpressed") end),
        },

		events = 
		{
            EventHandler("animover", function(inst) if inst.AnimState:AnimDone() then inst.sg:GoToState("idle") end end),
		},
	},
	
	State
	{
		name = "trinketribute",
		tags = {"busy"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("eat_talisman")
			inst.AnimState:PushAnimation("spit_talisman", false)
		end,

        timeline =
        {
			TimeEvent(21*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/swallow") end),
			TimeEvent(44*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/unimpressed") end),
            TimeEvent(80*FRAMES, function(inst) inst:PerformBufferedAction() end),
			TimeEvent(80*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/spit") end),
        },

		events = 
		{
            EventHandler("animover", function(inst) if inst.AnimState:AnimDone() then inst.sg:GoToState("idle") end end),
		},
	},

	State{
        name = "special_atk1",
        tags = {"busy"},
        
        onenter = function(inst)
			if not inst.noactions then
				inst.AnimState:PlayAnimation("taunt")
			else
				inst.sg:GoToState("idle")
			end
        end,
        
        timeline = 
        {
            TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/taunt") end),
			TimeEvent(8*FRAMES, ShakeIfClose)
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "special_atk2",
        tags = {"busy", "move"},

        onenter = function(inst, pos)
			if not inst.noactions and inst.canunder == true then
				inst.components.health:SetInvincible(true)
				--inst.AnimState:PlayAnimation("taunt")
				inst.AnimState:PlayAnimation("out", false)
				inst.noactions = true
				inst.canunder = false
			else
				inst.sg:GoToState("idle")
			end
        end,

        timeline =
        {            
			TimeEvent(28*FRAMES, ShakeIfClose),
			
            TimeEvent(28 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/enter") 			
					inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/sfx/break_spike")
			end),
			TimeEvent(27 * FRAMES, function(inst) 
				DoAoe(inst, 4, 0.75)
				inst.components.combat:SetAreaDamage(6, 1)
			end),
	    },

        onexit = function(inst)
			if inst.noactions then
				inst.DynamicShadow:Enable(false)
				inst._isunder = true
				inst:Hide()
				inst:AddTag("notarget")
				
				if TheNet:GetServerGameMode() == "lavaarena" then
					inst.diguptask = inst:DoTaskInTime(6, function(inst) inst.sg:GoToState("special_exit") end)
				end
			end
        end,
		
		events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end)
        },
    },

	State{
        name = "attack",
        tags = { "attack", "busy" },

        onenter = function(inst, target)
			if inst.noactions then
				inst.sg:GoToState("idle")
			else	
				if inst.altattack == true then
					inst.sg:GoToState("summonwall")
				else
					inst.AnimState:PlayAnimation("cast_pre")
					inst.components.combat:StartAttack()
					local buffaction = inst:GetBufferedAction()
					local target = buffaction ~= nil and buffaction.target or nil
					if target ~= nil then
						if target:IsValid() then
							inst:FacePoint(target:GetPosition())
							inst.sg.statemem.attacktarget = target
							inst.sg.statemem.target = target
							inst.sg.statemem.targetpos = target:GetPosition()
							inst.sg.statemem.targetpos.y = 0
						end
					end
				end
			end
        end,

        onupdate = function(inst)
            if inst.sg.statemem.target ~= nil then
                if not inst.sg.statemem.target:IsValid() then
                    inst.sg.statemem.target = nil
                else
                    local x, y, z = inst.sg.statemem.target.Transform:GetWorldPosition()
                    local distsq = inst:GetDistanceSqToPoint(x, y, z)
                    if distsq < TUNING.ANTLION_CAST_MAX_RANGE * TUNING.ANTLION_CAST_MAX_RANGE then
                        inst.sg.statemem.targetpos.x, inst.sg.statemem.targetpos.z = x, z
                    end
                end
            end
        end,

        timeline =
        {
            TimeEvent(3 * FRAMES, function(inst)
                --NOTE: sandspike has 28 frames lead in time
                if inst.sg.statemem.targetpos ~= nil then
                    inst.sg.statemem.target = nil --not typo, this is to stop updating
                    SpawnSpikes(inst, inst.sg.statemem.targetpos, math.random(6, 7))
                end
            end),
            TimeEvent(6 * FRAMES, function(inst)
                inst.sg:AddStateTag("nosleep")
                inst.sg:AddStateTag("nofreeze")
            end),
            TimeEvent(8 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/cast_pre") end),
            TimeEvent(29 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/sfx/ground_break")
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/cast_pre")
            end),
            TimeEvent(32 * FRAMES, ShakeCasting),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("summonspikes_pst")
                end
            end),
        },
    },

    State{
        name = "summonspikes_pst",
        tags = { "attack", "busy", "nosleep", "nofreeze" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("cast_pst")
        end,

        timeline =
        {
            CommonHandlers.OnNoSleepTimeEvent(10 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("idle"),
        },
    },

    State{
        name = "summonwall",
        tags = { "attack", "busy" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("cast_sandcastle")
			inst.altattack = false
            --V2C: don't trigger attack cooldown
            --inst.components.combat:StartAttack()
        end,

        timeline =
        {
            TimeEvent(9 * FRAMES, function(inst)
                inst.sg:AddStateTag("nosleep")
                inst.sg:AddStateTag("nofreeze")
            end),
            TimeEvent(9 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/attack_pre") end),
            TimeEvent(14 * FRAMES, function(inst)
                --NOTE: sandblock has 10 frames lead in time
				inst:DoTaskInTime(25, function(inst) inst.altattack = true end)
                SpawnBlocks(inst, inst:GetPosition(), 19)
                --inst.components.timer:StartTimer("wall_cd", TUNING.ANTLION_WALL_CD)
            end),
            TimeEvent(25 * FRAMES, ShakeRaising),
            CommonHandlers.OnNoSleepTimeEvent(56 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
                inst.sg:RemoveStateTag("nosleep")
                inst.sg:RemoveStateTag("nofreeze")
            end),
        },

        events =
        {
            CommonHandlers.OnNoSleepAnimOver("idle"),
        },
    },
	
	State{
        name = "attack_swipe",
        tags = {"busy", "attack"},
        
        onenter = function(inst)
			if not inst.noactions then
				local buffaction = inst:GetBufferedAction()
				local target = buffaction ~= nil and buffaction.target or nil
				if target ~= nil then
					if target:IsValid() then
						inst:FacePoint(target:GetPosition())
						inst.sg.statemem.attacktarget = target
					end
				end
				inst.AnimState:PlayAnimation("attack_pre")
				inst.AnimState:PushAnimation("attack", false)
				inst.components.combat:StartAttack()
			else
				inst.sg:GoToState("idle")
			end
        end,
        
        timeline = 
        {
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/attack_pre") end),
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/attack") end),
			TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/leif/swipe") end),
            TimeEvent(50*FRAMES, function(inst) inst.components.combat:DoAreaAttack(inst, 6) end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

	State{
        name = "spawn",
        tags = {"busy"},

        onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.components.locomotor.disable = true
			inst.components.health:SetInvincible(false)
			inst:Show()
			inst:RemoveTag("notarget")
            inst.AnimState:PlayAnimation("enter")
        end,

        timeline =
        {
			TimeEvent(0 * FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/sfx/break_spike")
            end),
			TimeEvent(1*FRAMES, ShakeIfClose),
			TimeEvent(2 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/enter") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end)
        },
    },
	
    State{
        name = "special_exit",
        tags = {"busy"},

        onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.components.locomotor.disable = true
			inst.components.health:SetInvincible(false)
			inst:Show()
			inst:RemoveTag("notarget")
			inst.noactions = false
			inst.canunder = false
			inst._isunder = nil
            inst.AnimState:PlayAnimation("enter")
			if inst.diguptask then
				inst.diguptask:Cancel()
				inst.diguptask = nil
			end
        end,

        timeline =
        {
			TimeEvent(0 * FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/sfx/break_spike")
            end),
			TimeEvent(1*FRAMES, ShakeIfClose),
			TimeEvent(2 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/enter") end),
			--TimeEvent(50*FRAMES, ShakeIfClose)
        },

        events =
        {
            EventHandler("animover", function(inst) inst:DoTaskInTime(10, function(inst) inst.canunder = true end) inst.sg:GoToState("idle") end)
        },
    },

	State{
        name = "run_start",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			if inst.noactions then
				inst.components.locomotor:RunForward()
			else
				inst.sg:GoToState("idle")
			end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("run")
                end
            end),
        },
    },

    State{
        name = "run",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			if inst.noactions then
				inst.components.locomotor:RunForward()
			else
				inst.sg:GoToState("idle")
			end
        end,

        timeline =
        {

        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("run")
                end
            end),
        },
    },

    State{
        name = "run_stop",
        tags = { "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	 State{
        name = "walk_stop",
        tags = { "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
			inst.sg:GoToState("idle")
        end,
    },
	
	State{
        name = "action",
        tags = { "canrotate" },

        onenter = function(inst)
			if not inst.noactions then
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation("hit")
				inst:PerformBufferedAction()
			else
				inst.sg:GoToState("idle")
			end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
        name = "action2",
        tags = { "canrotate" },

        onenter = function(inst)
			if not inst.noactions then
				inst.components.locomotor:StopMoving()
				inst.sg:GoToState("eat")
			else
				inst.sg:GoToState("eat_pre")
			end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
        name = "action3",
        tags = { "canrotate" },

        onenter = function(inst)
			if not inst.noactions then
				inst.components.locomotor:StopMoving()
				inst.sg:GoToState("spit")
			else
				inst.sg:GoToState("spit_pre")
			end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
        name = "action4",
        tags = { "busy" },

        onenter = function(inst)
			if inst.noactions then
				inst.components.health:SetInvincible(false)
				inst:PerformBufferedAction()
				inst.components.health:SetInvincible(true)
				inst.sg:GoToState("idle")
			else
				inst.sg:GoToState("eat")
			end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	 State{
        name = "action_pst",
        tags = {"busy", "move"},

        onenter = function(inst, pos)
            inst.components.health:SetInvincible(true)
            inst.AnimState:PlayAnimation("out", false)
			inst.noactions = true --just incase
        end,

        timeline =
        {            
			TimeEvent(28*FRAMES, ShakeIfClose),
			
            TimeEvent(28 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/enter")  
				inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/sfx/break_spike")
			end),
	    },

        onexit = function(inst)
			inst.noactions = true
			inst:Hide()
			inst:AddTag("notarget")
        end,
		
		events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end)
        },
    },
	
	State{
        name = "eat_pre",
        tags = {"busy"},

        onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.components.locomotor.disable = true
			inst.components.health:SetInvincible(false)
			inst:Show()
			inst:RemoveTag("notarget")
			inst.noactions = true --Incase we get interrupted.
            inst.AnimState:PlayAnimation("enter")
        end,

        timeline =
        {
			TimeEvent(0 * FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/sfx/break_spike")
            end),
			TimeEvent(1*FRAMES, ShakeIfClose),
			TimeEvent(2 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/enter") end),
			--TimeEvent(50*FRAMES, ShakeIfClose)
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("eat") end)
        },
    },
	
	State
	{
		name = "eat",
		tags = {"busy"},

		onenter = function(inst, data)
			inst:PerformBufferedAction()
			inst.AnimState:PlayAnimation("eat")
		end,
        
        timeline =
        {
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/eat") end),
			TimeEvent(36*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/eat") end),
			TimeEvent(71*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/swallow") end),
        },

		events = 
		{
            EventHandler("animover", function(inst) 
				if inst.AnimState:AnimDone() then 
					if inst.noactions then
						inst.sg:GoToState("action_pst")
					else
						inst.sg:GoToState("idle")
					end	
				end
			end),
		},
	},
	
	State{
        name = "spit_pre",
        tags = {"busy"},

        onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.components.locomotor.disable = true
			inst.components.health:SetInvincible(false)
			inst:Show()
			inst:RemoveTag("notarget")
			inst.noactions = true --Incase we get interrupted.
            inst.AnimState:PlayAnimation("enter")
        end,

        timeline =
        {
			TimeEvent(0 * FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/sfx/break_spike")
            end),
			TimeEvent(1*FRAMES, ShakeIfClose),
			TimeEvent(2 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/enter") end),
			--TimeEvent(50*FRAMES, ShakeIfClose)
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("spit") end)
        },
    },
	
	State
	{
		name = "spit",
		tags = {"busy"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("spit")
		end,

        timeline =
        {
			TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/spit") end),
			TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/attack_pre") end),
            TimeEvent(26*FRAMES, function(inst) inst:PerformBufferedAction() end),
			TimeEvent(60*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/unimpressed") end),
        },

		events = 
		{
            EventHandler("animover", function(inst) if inst.AnimState:AnimDone() then inst.sg:GoToState("action_pst") end end),
		},
	},
	
	State{
        name = "migrate",
        tags = { "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("hit")
			inst:PerformBufferedAction()
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
        name = "hit",
        tags = { "hit", "busy" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving() --doesn't move but might as well check.
            inst.AnimState:PlayAnimation("hit")
			inst.sg.mem.last_hit_time = GetTime()
        end,

        timeline =
        {
            
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State
	{
		name = "sinkhole_pre",
		tags = {"busy", "attack"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("cast_pre")
		end,

        timeline =
        {
			TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/cast_pre") end),
			TimeEvent(29*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/sfx/ground_break") end),
			TimeEvent(30*FRAMES, ShakeIfClose),
			TimeEvent(29*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/cast_pre") end),
        },

		events = 
		{
            EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then 
					inst.sg:GoToState("sinkhole_loop", 0)
				end
			end),
		},
	},

	State
	{
		name = "sinkhole_loop",
		tags = {"busy", "attack"},

		onenter = function(inst, loopcount)
			inst.AnimState:PlayAnimation("cast_loop_active")
			if loopcount then
			loopcount = loopcount + 1
			
			else
			print("Loopcount came up nil in stategraph!")
			inst.sg:GoToState("sinkhole_pst")
			end
		end,

        timeline =
        {
			TimeEvent(28*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/cast_pre") end),
			TimeEvent(28*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/sfx/ground_break") end),
			TimeEvent(29*FRAMES, ShakeIfClose),
			TimeEvent(69*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/cast_pre") end),
			TimeEvent(69*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/sfx/ground_break") end),
			TimeEvent(70*FRAMES, ShakeIfClose),
        },

		events = 
		{
            EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then 
					if loopcount == MAX_LOOPS then
						inst.sg:GoToState("sinkhole_pst")
					else
						inst.sg:GoToState("sinkhole_loop", loopcount)
					end
				end
			end),
		},
	},

	State
	{
		name = "sinkhole_pst",
		tags = {"busy", "attack"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("cast_pst")
		end,

        timeline =
        {
        },

		events =
		{
            EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then 
					inst.sg:GoToState("idle")
				end
			end),
		},
	},
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst:Show() --incase death happens while underground.
			inst.canunder = true
			inst.noactions = false
            inst.AnimState:PlayAnimation("death")
			inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/death")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/attack_pre")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
        end,

		 timeline =
        {
			TimeEvent(0 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/death") end),			
            TimeEvent(40*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/bodyfall_death")
				inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
				inst.components.inventory:DropEverything(true)
				ShakeIfClose(inst)
            end),
        },
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },

	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
			if inst.noactions then
				if inst.components.locomotor ~= nil then
					inst.components.locomotor:StopMoving()
				end
			else
				inst.AnimState:PlayAnimation("sleep_pre")
				inst.sg:AddStateTag("caninterrupt")
			end
        end,
		
		timeline = 
		{
			TimeEvent(46 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/bodyfall_death") end),
			TimeEvent(48 * FRAMES, ShakeIfClose),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			PlayablePets.SleepHeal(inst)
			inst.AnimState:PlayAnimation("sleep_loop")
		end,

		timeline=
        {
			TimeEvent(7 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/sleep_in") end),
			TimeEvent(40 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/sleep_out") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,
		
		timeline = {
        CommonHandlers.OnNoSleepTimeEvent(24 * FRAMES, function(inst)
            inst.sg:RemoveStateTag("busy")
            inst.sg:RemoveStateTag("nosleep")
        end),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
}

CommonStates.AddFrozenStates(states)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/death") end),			
            TimeEvent(40*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/together/antlion/bodyfall_death")
				ShakeIfClose(inst)
            end),
		},
		
		corpse_taunt =
		{

		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "idle"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "idle")
PP_CommonStates.AddOpenGiftStates(states, "idle")
PP_CommonStates.AddSailStates(states, {}, "idle", "idle")
local simpleanim = "hit"
local simpleidle = "idle"
local simplemove = "idle"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simplemove, loop = simplemove, pst = simplemove}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove,
	plank_hop = simplemove,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "idle",
}
)
    
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "idle",
	
	castspelltime = 10,
})



    
return StateGraph("antlionp", states, events, "spawn", actionhandlers)

