require("stategraphs/commonstates")
require("stategraphs/ppstates")

--Note: This will be used for all deer related mobs.

local longaction = "action"
local shortaction = "action"
local workaction = "work"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	--ActionHandler(ACTIONS.CAST_NET, longaction),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

--------------------------------------------------------------------------

local function DoChainSound(inst, volume)
	if inst.gem ~= nil then
    inst:DoChainSound(volume)
	end
end

local function DoChainIdleSound(inst, volume)
	if inst.gem ~= nil then
    inst:DoChainIdleSound(volume)
	end
end

local function DoBellSound(inst, volume)
	if inst.gem ~= nil then
    inst:DoBellSound(volume)
	end
end

local function DoBellIdleSound(inst, volume)
	if inst.gem ~= nil then
    inst:DoBellIdleSound(volume)
	end
end

local function DoFootstep(inst, volume)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/footstep", nil, volume)
    PlayFootstep(inst, volume)
end

local function DoFootstepRun(inst, volume)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/footstep_run", nil, volume)
    PlayFootstep(inst, volume)
end

--------------------------------------------------------------------------

local events=
{
	CommonHandlers.OnAttacked(),
    PP_CommonHandlers.OnDeath(),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then 
		if inst.gem ~= nil and inst.canmagic == true and inst:GetDistanceSqToInst(target) > TUNING.DEER_ATTACK_RANGE then
		inst.sg:GoToState("magic_pre", data.target)
		else
		inst.sg:GoToState("attack", data.target) 
		end
	end 
	end),
    CommonHandlers.OnSleep(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.OnSink(),
	CommonHandlers.OnHop(), 
	PP_CommonHandlers.OpenGift(),
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("idle_loop")
        end,
		
		timeline =
        {
            TimeEvent(10 * FRAMES, DoBellIdleSound),
            TimeEvent(12 * FRAMES, DoChainIdleSound),
            TimeEvent(20 * FRAMES, function(inst)
                DoBellIdleSound(inst, .4)
            end),
            TimeEvent(23 * FRAMES, function(inst)
                DoChainIdleSound(inst, .4)
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

    },
	
	 State{
        name = "alert",
        tags = { "idle", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("alert_pre")
            inst.AnimState:PushAnimation("alert_idle", true)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/curious")
            --don't need gem deer chain sounds
        end,
    },
	
	State{
        name = "special_atk1",
        tags = { "idle", "canrotate", "notaunting" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("alert_pre")
            inst.AnimState:PushAnimation("alert_idle", true)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/curious")
            --don't need gem deer chain sounds
        end,
    },

    State{
        name = "idle_grazing",
        tags = { "idle", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("eat")
        end,

        timeline =
        {
            TimeEvent(0, DoChainSound),
            TimeEvent(6 * FRAMES, DoBellSound),
            TimeEvent(7 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/eat")
            end),
            TimeEvent(9 * FRAMES, function(inst)
                DoChainIdleSound(inst, .5)
            end),
            TimeEvent(19 * FRAMES, function(inst)
                DoBellIdleSound(inst, .3)
            end),
            TimeEvent(20 * FRAMES, function(inst)
                DoChainIdleSound(inst, .5)
            end),
            TimeEvent(32 * FRAMES, function(inst)
                DoBellIdleSound(inst, .7)
            end),
            TimeEvent(33 * FRAMES, function(inst)
                DoChainIdleSound(inst, .5)
            end),
            TimeEvent(47 * FRAMES, DoChainSound),
            TimeEvent(48 * FRAMES, DoBellSound),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
        name = "eat",
        tags = { "busy"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("eat")
        end,

        timeline =
        {
            TimeEvent(0, DoChainSound),
            TimeEvent(6 * FRAMES, DoBellSound),
            TimeEvent(7 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/eat")
				inst:PerformBufferedAction()
            end),
            TimeEvent(9 * FRAMES, function(inst)
                DoChainIdleSound(inst, .5)
            end),
            TimeEvent(19 * FRAMES, function(inst)
                DoBellIdleSound(inst, .3)
				inst.sg:RemoveStateTag("busy")
            end),
            TimeEvent(20 * FRAMES, function(inst)
                DoChainIdleSound(inst, .5)
            end),
            TimeEvent(32 * FRAMES, function(inst)
                DoBellIdleSound(inst, .7)
            end),
            TimeEvent(33 * FRAMES, function(inst)
                DoChainIdleSound(inst, .5)
            end),
            TimeEvent(47 * FRAMES, DoChainSound),
            TimeEvent(48 * FRAMES, DoBellSound),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },

    State{
        name = "growantler",
        tags = { "busy" },

        onenter = function(inst)
            inst.sg.mem.wantstogrowantler = nil
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("unshackle")
            --don't need gem deer chain sounds
        end,

        timeline =
        {
            TimeEvent(12 * FRAMES, DoFootstep),
            TimeEvent(13 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/common/deathpoof")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    local fx = SpawnPrefab("deer_growantler_fx")
                    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
                    fx.Transform:SetRotation(inst.Transform:GetRotation())

                    inst.sg:GoToState("unshackle_pst")
                end
            end),
        },

        onexit = function(inst)
            inst:ShowAntler()
        end,
    },
    
    State{
        name = "knockoffantler",
        tags = { "busy" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("hit_2")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/hit")
            --don't need gem deer chain sounds
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
        name = "work",
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
        local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
        inst.components.combat:SetTarget(target)
		if target ~= nil then
            if target:IsValid() then
                inst:FacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("atk")
	end,
	
	onexit = function(inst)
        inst.components.combat:SetTarget(nil)
    end,
	
    timeline=
    {

			TimeEvent(0, DoBellSound),
        TimeEvent(FRAMES, function(inst)
            DoChainSound(inst, .6)
        end),
        TimeEvent(3 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/swish")
        end),
        TimeEvent(5 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/taunt")
        end),
        TimeEvent(11 * FRAMES, DoBellSound),
        TimeEvent(12 * FRAMES, function(inst)
            inst:PerformBufferedAction()
            DoChainSound(inst)
        end),
        TimeEvent(23 * FRAMES, DoFootstep),
        TimeEvent(25 * FRAMES, DoFootstepRun),
        TimeEvent(26 * FRAMES, function(inst)
            DoBellSound(inst)
            DoChainSound(inst)
        end),
        TimeEvent(28 * FRAMES, function(inst)
            inst.sg:RemoveStateTag("busy")
        end)
    },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle")  end),
        },
	
    },
	
	State{
        name = "attack",
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		if inst.gem ~= nil and inst.canmagic == true then
			inst.sg:GoToState("magic_pre")
		else	
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			inst.components.combat:SetTarget(target)
			if target ~= nil then
				if target:IsValid() then
					inst:FacePoint(target:GetPosition())
					inst.sg.statemem.attacktarget = target
				end
			end
			inst.components.combat:StartAttack()
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("atk")
        
		end
	end,
	
	onexit = function(inst)
        inst.components.combat:SetTarget(nil)
    end,
	
    timeline=
    {

			TimeEvent(0, DoBellSound),
        TimeEvent(FRAMES, function(inst)
            DoChainSound(inst, .6)
        end),
        TimeEvent(3 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/swish")
        end),
        TimeEvent(5 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/taunt")
        end),
        TimeEvent(11 * FRAMES, DoBellSound),
        TimeEvent(12 * FRAMES, function(inst)
            inst.components.combat:DoAttack(inst.sg.statemem.target)
            DoChainSound(inst)
        end),
        TimeEvent(23 * FRAMES, DoFootstep),
        TimeEvent(25 * FRAMES, DoFootstepRun),
        TimeEvent(26 * FRAMES, function(inst)
            DoBellSound(inst)
            DoChainSound(inst)
        end),
        TimeEvent(28 * FRAMES, function(inst)
            inst.sg:RemoveStateTag("busy")
        end)
    },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle")  end),
        },
	
    },
	
    State{
        name = "magic_pre",
        tags = { "attack", "busy", "casting" },

        onenter = function(inst, target)
            --inst.components.combat:StartAttack()
			--local tar = inst:GetTarget() or nil
           -- local pos = tar or inst:GetPosition()
			--local pos2 = inst:GetPosition()
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			inst.components.combat:SetTarget(target)
			if target ~= nil then
				if target:IsValid() then
					inst:FacePoint(target:GetPosition())
					inst.sg.statemem.attacktarget = target
				end
			end
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("atk_magic_pre")

           -- inst.sg.statemem.targets = targets
            inst.sg.mem.wantstocast = nil

            inst.sg.statemem.fx = SpawnPrefab(inst.gem == "red" and "deer_fire_charge" or "deer_ice_charge")
            inst.sg.statemem.fx.entity:SetParent(inst.entity)
            inst.sg.statemem.fx.entity:AddFollower()
            inst.sg.statemem.fx.Follower:FollowSymbol(inst.GUID, "swap_antler_red", 0, 0, 0)
        end,

        timeline =
        {
            TimeEvent(0, DoChainIdleSound),
            TimeEvent(FRAMES, DoBellIdleSound),
            TimeEvent(3 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/huff")
            end),
            TimeEvent(14 * FRAMES, function(inst)
                DoChainSound(inst)
                DoBellSound(inst)
            end),
            TimeEvent(19.5 * FRAMES, function(inst)
                if inst.gem ~= "red" then
                    inst.sg.statemem.spells = inst:DoCast(inst.sg.statemem.target)
                    inst.sg.statemem.target = nil
                end
            end),
            TimeEvent(22 * FRAMES, DoBellSound),
            TimeEvent(23 * FRAMES, DoChainSound),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg.statemem.magic = true
                    if inst.sg.statemem.spells == nil and inst.components.combat.target == nil then
                        inst.sg:GoToState("magic_pst", { fx = inst.sg.statemem.fx })
                    else
                        inst.sg:GoToState("magic_loop", { fx = inst.sg.statemem.fx, spells = inst.sg.statemem.spells, targets = inst.components.combat.target })
                    end
                end
            end),
        },

        onexit = function(inst)
            if not inst.sg.statemem.magic then
                inst.sg.statemem.fx:KillFX()
            end
        end,
    },

    State{
        name = "magic_loop",
        tags = { "attack", "busy", "casting" },

        onenter = function(inst, data)
			--local tar = inst:GetTarget() or nil
           -- local pos = tar or inst:GetPosition()
			--local pos2 = inst:GetPosition()
            if inst.gem == "red" or inst.gem == "blue" then
                inst.sg.statemem.magic = true
                inst.sg:GoToState("magic_pst", data)
            else
                data.looped = (data.looped or 0) + 1
                inst.sg.statemem.data = data
                if not inst.AnimState:IsCurrentAnimation("atk_magic_loop") then
                    inst.AnimState:PlayAnimation("atk_magic_loop", true)
                end
                inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
            end
        end,

        timeline =
        {
            TimeEvent(0, DoChainIdleSound),
            TimeEvent(9 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/scratch")
            end),
            TimeEvent(14 * FRAMES, DoBellIdleSound),
        },

        ontimeout = function(inst)
            inst.sg.statemem.magic = true
            inst.sg:GoToState(inst.sg.statemem.data.looped < 3 and "magic_loop" or "magic_pst", inst.sg.statemem.data)
        end,

        onexit = function(inst)
            if not inst.sg.statemem.magic and inst.sg.statemem.data ~= nil and inst.sg.statemem.data.fx ~= nil then
                inst.sg.statemem.data.fx:KillFX()
            end
        end,
    },

    State{
        name = "magic_pst",
        tags = { "attack", "busy", "casting" },

        onenter = function(inst, data)
			local tar = inst.components.combat.target or nil
            local pos = tar or inst:GetPosition()
			local pos2 = inst:GetPosition()
			--local buffaction = inst:GetBufferedAction()
			--local target = buffaction ~= nil and buffaction.target or nil
			--inst.components.combat:SetTarget(target)
			--if target ~= nil then
				--if target:IsValid() then
					--inst:FacePoint(target:GetPosition())
					--inst.sg.statemem.attacktarget = target
				--end
			--end
			if data ~= nil then
                inst.sg.statemem.fx = data.fx
                inst.sg.statemem.spells = data.spells
                inst.sg.statemem.target = data.target
            end
            inst.AnimState:PlayAnimation("atk_magic_pst")
        end,

        timeline =
        {
            TimeEvent(2 * FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/grrr")
            end),
            TimeEvent(5 * FRAMES, DoBellSound),
            TimeEvent(11 * FRAMES, DoChainSound),
            TimeEvent(20 * FRAMES, DoBellSound),
            TimeEvent(21 * FRAMES, DoFootstepRun),
            TimeEvent(22 * FRAMES, DoChainSound),
            TimeEvent(25 * FRAMES, function(inst)
				local tar = inst.components.combat.target or nil
				local pos = tar or inst:GetPosition()
				local pos2 = inst:GetPosition()
				local success = true
				inst.SpawnSpell(inst, inst.components.combat.target) 
                if inst.sg.statemem.spells ~= nil then
                    for i, v in ipairs(inst.sg.statemem.spells) do
                        if v:IsValid() then
                            success = true
                            v:TriggerFX()
                        end
                    end
                elseif inst.gem == "red" then
                    local spells = inst:DoCast(inst.sg.statemem.target)
                    if spells ~= nil then
                        success = true
                            v:TriggerFX()
                    end
                end
                if inst.sg.statemem.fx ~= nil then
                    inst.sg.statemem.fx:KillFX(success and "blast" or nil)
                    inst.sg.statemem.fx = nil
                end
                inst.sg:RemoveStateTag("casting")
            end),
            TimeEvent(26 * FRAMES, function (inst)
                DoChainSound(inst)
                DoBellIdleSound(inst)
            end),
            TimeEvent(35 * FRAMES, DoBellSound),
            TimeEvent(36 * FRAMES, DoFootstep),
            TimeEvent(39 * FRAMES, DoChainSound),
            TimeEvent(41 * FRAMES, DoBellSound),
            TimeEvent(45 * FRAMES, DoFootstep),
            TimeEvent(46 * FRAMES, DoBellIdleSound),
            TimeEvent(47 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.fx ~= nil then
                inst.sg.statemem.fx:KillFX()
            end
			inst.canmagic = false
			inst.components.combat:SetRange(TUNING.DEER_ATTACK_RANGE)
			if inst.gem == "blue" then
			inst:DoTaskInTime(15, function(inst) inst.canmagic = true inst.components.combat:SetRange(TUNING.DEER_ATTACK_RANGE + 20, TUNING.DEER_ATTACK_RANGE) end)
			else
			inst:DoTaskInTime(10, function(inst) inst.canmagic = true inst.components.combat:SetRange(TUNING.DEER_ATTACK_RANGE + 20, TUNING.DEER_ATTACK_RANGE) end)
			end
        end,
    },

    State{
        name = "unshackle",
        tags = { "busy" },

        onenter = function(inst)
            inst.sg.mem.wantstounshackle = nil
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("unshackle")
        end,

        timeline =
        {
            TimeEvent(0, DoBellIdleSound),
            TimeEvent(2 * FRAMES, DoChainSound),
            TimeEvent(13 * FRAMES, DoFootstep),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    if inst.gem ~= nil then
                        local player--[[, rangesq]] = inst:GetNearestPlayer()
                        LaunchAt(SpawnPrefab(inst.gem.."gem"), inst, player, 1, 4, .5)
                    end
                    local x, y, z = inst.Transform:GetWorldPosition()
                    local rot = inst.Transform:GetRotation()
                    inst:Remove()

                    inst = SpawnPrefab("deer")
                    inst.Transform:SetPosition(x, y, z)
                    inst.Transform:SetRotation(rot)
                    inst.sg:GoToState("unshackle_pst")

                    inst = SpawnPrefab("deer_unshackle_fx")
                    inst.Transform:SetPosition(x, y, z)
                    inst.Transform:SetRotation(rot)
                end
            end),
        },
    },

    State{
        name = "unshackle_pst",
        tags = { "busy" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("unshackle_pst")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/huff")
        end,

        timeline =
        {
            TimeEvent(30 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
        end,

		timeline =
    {
        TimeEvent(0, DoChainSound),
        TimeEvent(FRAMES, DoBellSound),
        TimeEvent(12 * FRAMES, function(inst)
            if inst.gem == nil then
                DoFootstep(inst)
            end
        end),
        TimeEvent(13 * FRAMES, function(inst)
            if inst.gem == nil then
                inst.sg:RemoveStateTag("busy")
            end
        end),
        TimeEvent(14 * FRAMES, DoChainSound),
        TimeEvent(18 * FRAMES, DoBellSound),
        TimeEvent(22 * FRAMES, function(inst)
            if inst.gem ~= nil then
                inst.sg:RemoveStateTag("busy")
            end
        end),
    },
		
        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst)
			inst.AnimState:PlayAnimation("walk_pst")
            if inst.shouldwalk then
				inst.shouldwalk = false
			else
				inst.shouldwalk = true
			end
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline =
		{
        TimeEvent(0, DoChainIdleSound),
        TimeEvent(5 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/bodyfall_2")
            DoBellIdleSound(inst)
        end),
        TimeEvent(15 * FRAMES, DoChainSound),
        TimeEvent(20 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/hit")
        end),
        TimeEvent(23 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/bodyfall_2")
            DoBellSound(inst)
            if inst.gem ~= nil then
                local pt = inst:GetPosition()
                pt.y = 1
                inst.AnimState:Hide("swap_antler")
                inst.components.lootdropper:SpawnLootPrefab(inst.gem.."gem", pt)
            elseif inst.hasantler == true then
                local pt = inst:GetPosition()
                pt.y = 1
                inst.components.lootdropper:SpawnLootPrefab("deer_antler", pt)					
				end
			end),
			TimeEvent(24 * FRAMES, DoChainSound),
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        timeline =
		{
        TimeEvent(0, function(inst)
            DoChainSound(inst)
            DoBellSound(inst, .3)
        end),
        TimeEvent(9 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/bodyfall")
            DoBellSound(inst, .5)
        end),
        TimeEvent(16 * FRAMES, DoChainSound),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		--inst.components.sanity.dapperness = -0.5
		end,
        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
			TimeEvent(9 * FRAMES, function(inst)
				DoBellSound(inst, .2)
			end),
			TimeEvent(31 * FRAMES, function(inst)
				DoBellIdleSound(inst, .2)
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        timeline =
		{
			TimeEvent(2 * FRAMES, DoBellIdleSound),
			TimeEvent(22 * FRAMES, DoChainSound),
			TimeEvent(24 * FRAMES, DoBellSound),
		},
	
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
}

CommonStates.AddFrozenStates(states)
CommonStates.AddWalkStates(states,
{
    starttimeline =
    {
        TimeEvent(0, function(inst)
            DoChainIdleSound(inst, .5)
            DoBellIdleSound(inst, .5)
        end),
    },
    walktimeline =
    {
        TimeEvent(0, function(inst)
            DoFootstep(inst)
            DoChainIdleSound(inst)
            DoBellIdleSound(inst)
        end),
        TimeEvent(6 * FRAMES, DoBellIdleSound),
        TimeEvent(7 * FRAMES, DoFootstep),
        TimeEvent(8 * FRAMES, DoChainIdleSound),
        TimeEvent(9 * FRAMES, DoFootstep),
        TimeEvent(10 * FRAMES, DoChainIdleSound),
        TimeEvent(12 * FRAMES, DoBellIdleSound),
        TimeEvent(17 * FRAMES, function(inst)
            DoFootstep(inst)
            DoBellIdleSound(inst)
        end),
        TimeEvent(18 * FRAMES, DoChainIdleSound),
    },
    endtimeline =
    {
        TimeEvent(3 * FRAMES, function(inst)
            DoFootstep(inst, .5)
            DoBellIdleSound(inst, .5)
            DoChainIdleSound(inst, .5)
        end),
    },
})
CommonStates.AddRunStates(states,
{
    starttimeline =
    {
        TimeEvent(4 * FRAMES, DoBellSound),
        TimeEvent(5 * FRAMES, DoChainSound),
        TimeEvent(8 * FRAMES, DoFootstepRun),
    },
    runtimeline =
    {
        TimeEvent(0, DoFootstepRun),
        TimeEvent(3 * FRAMES, DoBellSound),
        TimeEvent(4 * FRAMES, DoChainSound),
        TimeEvent(14 * FRAMES, DoFootstepRun),
    },
    endtimeline =
    {
        TimeEvent(FRAMES, function(inst)
            DoChainSound(inst)
            DoBellSound(inst)
        end),
        TimeEvent(2 * FRAMES, DoFootstep),
        TimeEvent(4 * FRAMES, DoFootstep),
    },
})
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0, DoChainIdleSound),
			TimeEvent(5 * FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/bodyfall_2")
				DoBellIdleSound(inst)
			end),
			TimeEvent(15 * FRAMES, DoChainSound),
			TimeEvent(20 * FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/hit")
			end),
			TimeEvent(23 * FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/bodyfall_2")
				DoBellSound(inst)
            if inst.gem ~= nil then
                local pt = inst:GetPosition()
                pt.y = 1
                inst.AnimState:Hide("swap_antler")
                inst.components.lootdropper:SpawnLootPrefab(inst.gem.."gem", pt)
            elseif inst.hasantler == true then
                local pt = inst:GetPosition()
                pt.y = 1
                inst.components.lootdropper:SpawnLootPrefab("deer_antler", pt)					
				end
			end),
			TimeEvent(24 * FRAMES, DoChainSound),
		},
		
		corpse_taunt =
		{
			TimeEvent(2 * FRAMES, DoBellIdleSound),
			TimeEvent(22 * FRAMES, DoChainSound),
			TimeEvent(24 * FRAMES, DoBellSound),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "sleep_pst"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "idle_loop", "run_pst")
local simpleanim = "run_pst"
local simpleidle = "idle_loop"
local simplemove = "run"
CommonStates.AddHopStates(states, false, {pre = simplemove, loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = simpleanim,
}
)
    
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = simpleanim,
	
	castspelltime = 10,
})
    
return StateGraph("deerp", states, events, "idle", actionhandlers)

